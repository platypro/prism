Pri'sm: The Maze Game
=====================
_Alpha Release_

"Pri'sm: The Maze Game" is a multiplayer game of capture the flag set in a user-defined room.

Currently, the engine and the game are fairly interwoven within the same source tree. 
In future releases, seperation of game and engine will be a priority.

_for license, see LICENCE_

## Gameplay  
### Running the server  
To run the server, the 'prisms' command can be run with the following arguments:
`./prisms <script> <gamemode> <port>`

script   - The script to generate rooms with (see Room API)
gamemode - The gamemode to run (see Gamemodes)
port     - The port to run on (default 2133)

### Running the client  
The client can be started by running the 'prism' executable

### Gamemodes  
CHAR 0: None
  No win state.
  
CHAR 1: Debug
  Description:
    Randomly assigns scores then immediately ends the game.
  
CHAR 2: Timed
  Description:
    Game ends when time runs out. The team with the highest
    point score wins.
  Gamemode Data:
    Total time limit

CHAR 3: Threshold
  Description:
    Game ends when a team score threshold is reached. The 
    team with the highest point score wins.
  Gamemode Data:
    Score threshold

CHAR 4: Control
  Description:
    Game ends when no teams have flags inside of their bases
    except for one, which wins the game.

### Room API  
_see src/Media/gen for examples_

	Functions:
	pr_init <- Called when the room needs to be generated

	-> c is the Type, bg for background, fg for foreground

	prZone (  w,  h ) // Set Zone
	prZone ( -1,  h ) // Set Zone Height
	prZone (  w, -1 ) // Set Zone Width
	prZone ( )        // Get Zone

	prRect    ( c, x, y, w, h )
	prRound   ( c, x, y, w, h )

	prFlag    (    x, y, w, h ) //Set team flag area. Returns a team ID

# Building  
The instructions below appply to both the git version and the source package. The host system requires cmake, a working C99 Compiler, and any dependencies for GLFW3.
## Step 0: Clone Submodules  
If you are building from git, the project dependencies are managed as git submodules, which allows them to be switched out for system binaries. Most modules will default to system binaries if available, even if the submodule is cloned. Submodules can be loaded by running `git submodule update --init <dependency>`. Source releases include all submodules.

| (Dependency)  | (Version)  | (System Override) |
| ------------- |:----------:|:-----------------:|
| deps/GLFW     | 3.2        | Yes               |
| deps/Lua      | 5.3        | Yes               |
| deps/NanoVG   | N/A        | No                |

## Step 1: Downloading dependencies  
### Gnu/Linux  

Here is a short list of development libraries required on a gnu/linux system running Xorg:
 + xrandr
 + xinerama
 + xkb
 + xcursor

Any other libraries needed will show up as warnings in the next step.

### Windows  
For building on windows, I recommend using mingw-w64 as a toolchain. It can be installed using MSYS2 (http://www.msys2.org) by running 'Pacman -S mingw-w64-i686-gcc' for the 32bit compiler and 'pacman -S mingw-w64-x86_64-gcc' for the 64 bit one. Cmake can also be installed with 'pacman -S cmake'

## Step 2: Setting up cmake  
For building with cmake I recommend building in a new subfolder of this one. Create a new folder of any name and change directory into it. Then run the command "cmake .." to generate build files, any dependency errors will occur here.

### Cmake options  
To set any of the following options, append them to the cmake command followed by '-D' and ending with '=ON' or '=OFF'
These are case sensitive.

BuildClient - Build the client (default: ON)
BuildServer - Build the server (default: ON)
SysGLFW     - Use system GLFW lib where possible (default: ON)
SysLua      - Use system Lua lib where possible (default: ON)

### Hacking  
To enable debugging and to disable optimizations, just pass the argument "-DCMAKE_BUILD_TYPE=DEBUG" to cmake.

### Step 3: Building  
run 'make', and everything should show up in a subfolder named "Output"

### TCP Protocol  
	FORMAT:

	\255{c}{s}\254
	 |   |  |  |
	 |   |  |  +- Signal Footer
	 |   |  +---- Signal Body (Up to 512 Characters)
	 |   +------- Signal Identifier (1 Character)
	 +----------- Sync Bit

	The Signal identifier is split into two parts. 
	The first nibble is the class, and the second nibble is the signal.
	Below, signals are represented as 'CHAR (class, signal)'

	Signal bodies are parsed as a series of data elements, with
	underscores in between.

	In this document, each element is represented using the format:
	<(Data Type) (Brief Description of element)>

	Which might appear below as this:
	  <DEC X><DEC Y><STR Text><DEC Radius>

	To put this in perspective, the above format may
	result in a signal such as this:
	  90_231_Hello\t_54

	<DEC>  - Decimal Value
	<STR>  - String Value
	  (Terminated with tab character)
	  
	<RECT>  - Equivalent to "<DEC X><DEC Y><DEC W><DEC H>"
	<POINT> - Equivalent to "<DEC X><DEC Y>"

	CHAR (0,1): List Begin
	  Format: 
	    <DEC Data-ID>
	  Description:
	    Begin a new set of data.

	CHAR (0,2): List End
	  Format: 
	    <DEC Data-ID>
	  Description:
	    End a set of data.

	CHAR (0,3): Heartbeat
	  Format: 
	    ~~Undefined~~
	  Description:
	    Ping signal, ignore at your discretion.

	CHAR (0,4): Order
	  Format:
	    <DEC Order>
	  Description:
	    Send an order to the client.
	  See: ORDERS

	CHAR (0,5): Player ID
	  Format:
	    <DEC Player-ID>
	  Description:
	    Player ID sent by the before any other signals.

	CHAR (0,6): Player Death Signal
	  Format:
	    <DEC ID>
	  Description:
	    Notifies the client of a player's death. The player
	    with the specified ID must be removed from the
	    local player list.

	CHAR (0,7): Kill Request
	  Format: 
	    ~~Undefined~~
	  Description:
	    Request a KICK order.
	  See: ORDERS    

	CHAR (1,0): Room Data
	  Format:
	    <DEC Download-Size><DEC Room-W><DEC Room-H>
	  Description:
	    Metadata meant to be sent before the room download.
	    When recieved it can be assumed that all prior room
	    data which has been recieved is invalid. After this
	    is recieved, the server will distribute room data.

	CHAR (1,1): Gamemode
	  Format:
	    <DEC gamemode><DEC gamedata>
	  Description:
	    Sets the gamemode. See GAMEMODES.

	CHAR (1,2): Chat
	  Format: 
	    <STR Chat-Message>
	  Description:
	    A plantext message, intended to be displayed to the
	    user.

	CHAR (2,0): Player Name
	  Format:
	    <DEC Player-ID><STR Player-Name>
	  Description:
	    Sets the specified player's name. If given to the
	    server, the sender's name needs to be updated.

	CHAR (2,1): Team Signal
	  Format:
	    <DEC ID><DEC Team>
	  Description:
	    Sets a players team. If sent to the server, the
	    sender has requested a team change

	CHAR (2,2): Player Ready
	  Format: 
	    <DEC Player-ID><DEC Player-Ready-Status>
	  Description:
	    Sets the specified player's ready status on the
	    download screen. If given to the server, the 
	    sender's status needs to be updated.

	CHAR (2,3): Player Signal
	  Format:
	    <DEC ID><POINT Velocity><POINT Position>
	  Description:
	    Sets the specified player's positional data. This
	    is meant to be continually sent during the game. If
	    given to the server, the sender's position needs to
	    be updated.

	CHAR (4,0): Score Signal
	  Format:
	    <DEC PlayerID><DEC Score><DEC TeamScore>
	  Description:
	    Sets a player's score along with their team.

	CHAR (4,1): Stamina Signal
	  Format:
	    <DEC PlayerID><DEC Stamina>
	  Description:
	    Sets a player's stamina

	CHAR (5,1): Team Info Signal
	  Format:
	    <DEC ID><RECT Area>
	  Description:
	    Sets a team's flag area

	CHAR (5,2): Flag Signal
	  Format:
	    <DEC PlayerID><DEC HasFlag><DEC TeamMod>
	    <DEC TeamCount>
	  Description:
	    Sets a players flag status. The Team with id 
	    'TeamMod' must also set flag count to field
	    'TeamCount'

	CHAR (5,3): Transfer Signal
	  Format:
	    <DEC Player1><DEC Player2>
	  Description:
	    A flag transfer between two players.
	    It is given from player 1 to player 2

	CHAR (10,0): Rectangle Shape
	  Format:
	    <RECT Bounds>
	  Description:
	    Rectangle type for use in room downloads

	CHAR (10,1): Round Shape
	  Format:
	    <RECT Bounds>
	  Description:
	    Round type for use in room downloads. It is defined
	    by it's bounds, and not by it's radius

	MORE:

	 + If team count is greater than 2 or gamemode is 0, then all players without a team spectate.
	   They are not impacted by collisions
