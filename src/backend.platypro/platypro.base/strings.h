/*
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef H_PLM_BASE_STRINGS
#define H_PLM_BASE_STRINGS

/** \file strings.h
 
    This file is responsible for defining
    the String resource API.

 *  \defgroup platypro_net Logging API
 *  \ingroup platypro_base
 */
/** @{ */

#define STRINGS_H_VERSION 1

#include "platypro.base/common.h"
#include "platypro.base/list.h"

/*! \brief A simple key-value pair used to define a single string
 *  It is recommended to use the functions in this API to manipulate them
 */
typedef struct StringDefinition
{
	char* ID;
	char* content;
} STRINGDEFINITION;

/*! \brief the maximum error length */
#define ERR_MAX_LEN 255

#define ERR_FILE_UNLOADED "File %s not loaded or could not be found\n"
#define ERR_FILE_BADPARSE "File %s could not be parsed\n"

#define SETTINGSTR_TRUE "true"
#define SETTINGSTR_FALSE "false"

/*! \brief Loads a string file
 *  \param file      The file to load
 *  \param touch     If true, the file will be created if it does not exist yet.
 *
 *  \returns a new list with the loaded strings
 */
extern LIST Strings_Load(char* file, bool touch);

/*! \brief Gets a string by name from a string list, with the name as a fallback.
 *  \param strings   The string file to iterate through
 *  \param key       The ID of the string to get
 *
 *  \returns The first string it found with key. If not found, it returns the string ID requested
 */
extern char* Strings_Get(LIST strings, char* key);

/*! \brief Gets a string by name from a string list
 *  \param strings   The string file to iterate through
 *  \param key       The ID of the string to get
 *
 *  \returns The first string it found with key. If not found, the function returns NULL
 */
extern char* Strings_GetValue(LIST strings, char* key);

/*! \brief Sets a string value
 *  \param strings   The string file to set the value in
 *  \param key       The string name
 *  \param value     The string text
 *  \returns Success
 */
extern bool Strings_Set(LIST* strings, char* key, char* value);

/*! \brief Writes a string list to the filesystem
 *  \param strings   The string file to write
 *  \param file      The file to write
 *  \param header    Message to put at start of file
 */
extern bool Strings_Write(LIST strings, char* file, char* header);

/*! \brief Frees a list of strings. 
 *         All string lists must be cleared for the program to exit clean
 *  \param list        A pointer to the list of strings to clear
 *  \returns Success
 */
extern bool  Strings_Close(LIST* strings);

/** @} */
#endif
