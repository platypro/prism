/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include "platypro.base/common.h"
#include "platypro.base/net.h"

void putError(SERVERNODE* node)
{
  Log_ThrowFormat(node->errormanager, ERRSEV_USERWARN, ERRID_CONNECTION, gai_strerror(WSAGetLastError()));
  WSACleanup();
}

PRIVATE void enableBlocking(SOCKET sd)
{
  //Turn off blocking
  u_long blockmode=1;
  ioctlsocket(sd,FIONBIO,&blockmode);
}

bool Server_Host_P(SERVERNODE* node, char* port)
{
  int result = false;
  SOCKET sd;
  WSADATA data;

  if(!WSAStartup(MAKEWORD(2,2), &data))
  {
    struct addrinfo info;
    struct addrinfo* newinfo;

    memset(&info, 0, sizeof(struct addrinfo));

    //Load our static options
    info.ai_family = AF_INET;
    info.ai_socktype = SOCK_STREAM;
    info.ai_protocol = IPPROTO_TCP;
    info.ai_flags = AI_PASSIVE;

    //Load the address and port into new addrinfo
    if(getaddrinfo(NULL, port, &info, &newinfo) == 0)
    {
      //Make addrinfo copy for cleanup
      sd = socket(
        newinfo->ai_family, newinfo->ai_socktype,
        newinfo->ai_protocol);
      if(sd != INVALID_SOCKET)
      {
        if(bind(sd, newinfo->ai_addr, (int)newinfo->ai_addrlen) != SOCKET_ERROR)
        {
          if(listen(sd, SOMAXCONN) != SOCKET_ERROR)
          {
            enableBlocking(sd);
            node->w_sd = sd;
            node->w_data = data;
            node->w_name = NULL;
            result = true; 
          }
        }
      }

      if(newinfo)
        freeaddrinfo(newinfo);
    }
  }

  if(!result) putError(node);

  return result;
}

bool Server_Accept_P(SERVERNODE* node, SERVERNODE* server)
{
  bool result = false;

  struct sockaddr_in addr;
  socklen_t resultsize = sizeof(struct sockaddr_in);
  SOCKET sd = accept(server->w_sd, 
               (struct sockaddr *) &addr, 
               &resultsize);
  if(sd!= INVALID_SOCKET)
  {
    enableBlocking(sd);
    node->w_sd = sd;
    node->w_name = inet_ntoa(addr.sin_addr);
    Log_ThrowFormat(node->errormanager, ERRSEV_INFO, STR_CONNECTION_NEW, node->w_name);
    result = true;
  }

  return result;
}

bool Server_Connect_P(SERVERNODE* node, char* address, char* port)
{
  int result = false;
  SOCKET sd;
  WSADATA data;

  if(!WSAStartup(MAKEWORD(2,2), &data))
  {
    struct addrinfo info;
    struct addrinfo* newinfo, *originfo;

    memset(&info, 0, sizeof(struct addrinfo));

    //Load our static options
    info.ai_family = AF_UNSPEC;
    info.ai_socktype = SOCK_STREAM;
    info.ai_protocol = IPPROTO_TCP;

    //Load the address and port into new addrinfo
    if(getaddrinfo(address, port, &info, &newinfo) == 0)
    {
      //Make addrinfo copy for cleanup
      originfo = newinfo;
      //do
      //{
        sd = socket(
          newinfo->ai_family, newinfo->ai_socktype,
          newinfo->ai_protocol);
        if(sd != INVALID_SOCKET)
        {
          if(connect(sd,newinfo->ai_addr, (int)newinfo->ai_addrlen)
            != SOCKET_ERROR)
          {
            result = true;

            enableBlocking(sd);
            node->w_data = data;
            node->w_sd   = sd;
            node->w_name = address;
            //break;
          } 
          else
          {
            putError(node);
            closesocket(sd);
            sd = INVALID_SOCKET;
            //break;
          }
        }// else break;
      //} while(newinfo);
      if(originfo)
        freeaddrinfo(originfo);
    }
  }

  return result;
}
