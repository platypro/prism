/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include "platypro.base/common.h"
#include "platypro.base/strings.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

#define TYPE_POINTERSTRING 3
#define TYPE_ALLOCSTRING 4
#define TYPE_HEADER 2

PRIVATE char* eatfile(char* filename)
{
	char* result = NULL;
	//Load the file
	FILE* filestream = fopen(filename,"r");

	if (filestream)
	{
		int filesize;

		//Get the file size
		fseek(filestream, 0, SEEK_END);
		filesize = ftell(filestream);
		fseek(filestream, 0, SEEK_SET);

		//Allocate room for file
		result = calloc(1, filesize+1);
		if (result)
		{
			//Eat (Load) the file
			if(fread(result, sizeof(char), filesize, filestream) == 0)
      {
        free(result);
        result = NULL;
      }
		}
    fclose(filestream);
	}

	return result;
}

PRIVATE bool testEscape(char* chars, char test)
{
	bool result = false;
	char* c;

	for (c = chars; *c != '\0'; c++)
	{
		if (*c == test)
		{
			result = true;
			break;
		}
	}
	return result;
}

PRIVATE STRINGDEFINITION* genStrDef()
{
	STRINGDEFINITION* result = calloc(1, sizeof(STRINGDEFINITION));
	return result;
}

PRIVATE STRINGDEFINITION* addDef(LIST* addto, STRINGDEFINITION* build)
{
	if (build->ID && build->content)
	{
		List_Add(addto, (void*)build, TYPE_POINTERSTRING);
	} 
	else 
	{
		free(build);
		build = NULL;
	}
	return build;
}

PRIVATE char* seperators[] = {"= \t", "\n\0;", "#"};

PUBLIC LIST Strings_Load(char* file, bool touch)
{
	char* fileload = eatfile(file);
	LIST result = NULL;

	if(!fileload)
	{
    if(!touch)
    {
      printf(ERR_FILE_UNLOADED, file);
    }
    else
    {
      //Touch the file
      int fd = open (file, O_WRONLY | O_CREAT | O_TRUNC, S_IROTH | S_IWUSR | S_IRUSR | S_IRGRP);
      
      List_Add(&result, "", TYPE_HEADER);
      
      close(fd);
    }
	} else {
		STRINGDEFINITION* build = genStrDef();

		//Add the file to the list,
		//so it can be freed later
		List_Add(&result, fileload, TYPE_HEADER);

		int loadstate = 0;
		char* c;
		for (c = fileload; *c != '\0'; c++)
		{
			if (testEscape(seperators[2], *c))
			{
				loadstate = 5;
			}
			else if (testEscape(seperators[1], *c))
			{
				loadstate = 0;
				*c = '\0';
				build = addDef(&result, build);
				build = genStrDef();
				if(!build)
				{
					break;
				}
			}
			else if (loadstate == 0 && !testEscape(seperators[0], *c))
			{
				build->ID = c;
				loadstate++;
			}
			else if (loadstate == 1 && testEscape(seperators[0], *c))
			{
				*c = '\0';
				loadstate++;
			}
			else if (loadstate == 2 && !testEscape(seperators[0], *c))
			{
				build->content = c;
				loadstate++;
			}
		}

		build = addDef(&result, build);
	}

	return result;
}

PRIVATE char* findString(LIST strings, char* key)
{
  STRINGDEFINITION* string;

  if(key && strings && strings->sibling)
  {
    foreach (string, strings->sibling)
    {
      if (!strcmp(string->ID, key))
      {
        return string->content;
        break;
      }
    }
  }
  return NULL;
}

PUBLIC char* Strings_Get(LIST strings, char* key)
{
	char* result = findString(strings, key);
  if(!result) result = key;

	return result;
}

PUBLIC char* Strings_GetValue(LIST strings, char* key)
{
  return findString(strings, key);
}

PUBLIC bool Strings_Set(LIST* strings, char* key, char* value)
{
  bool result = true;
  STRINGDEFINITION* string;
  
  //Remove existing string
  if(*strings)
  {
    if((*strings)->sibling)
    {
      foreach(string, (*strings)->sibling)
      {
        if(!strcmp(string->ID, key))
        {
          if(element->objType == TYPE_ALLOCSTRING)
          {
            free(string->content); 
            free(string->ID);
          }
          List_Remove(strings, string);
          free(string);
          break;
        }
      }
    }
    
    //Add String
    string = genStrDef();
    string->ID = malloc(strlen(key)+2);
    strcpy(string->ID, key);
    string->content = malloc(strlen(value)+2);
    strcpy(string->content, value);
    
    List_Add(strings, string, TYPE_ALLOCSTRING);
  } else result = false;
  
  return result;
}

PUBLIC bool Strings_Write(LIST strings, char* file, char* header)
{
  bool result = true;
  FILE* f = fopen(file, "w");
  if(f)
  {
    STRINGDEFINITION* sd;
    
    //Put header message
    fprintf(f, "%c%s%c", seperators[2][0], header, seperators[1][0]);
    
    if(strings && strings->sibling)
    {
      foreach(sd, strings->sibling)
      {
        fprintf(f, "%s%c%s%c", sd->ID, seperators[0][0], sd->content, seperators[1][0]);
      }
    }
    
    fclose(f);
  } else printf(ERR_FILE_UNLOADED, file);
  
  return result;
}

PRIVATE bool closeString(void* object, unsigned int key, void* data)
{
  STRINGDEFINITION* sd;
  switch(key)
  {
    case TYPE_ALLOCSTRING:
      sd = (STRINGDEFINITION*)object;
      free(sd->content);
      free(sd->ID);
    case TYPE_HEADER: case TYPE_POINTERSTRING:
      if(object && *(char*)object != '\0') free(object);
      break;
  }
  
  return true;
}

PUBLIC bool Strings_Close(LIST* strings)
{
	bool result;  
  result = List_Purge(strings, closeString);
	return result;
}
