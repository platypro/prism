/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include "platypro.base/common.h"
#include "platypro.base/net.h"

#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#ifdef _WIN32
  #include "platypro.base/net.win.c"
#else
  #include "platypro.base/net.posix.c"
#endif

PRIVATE void throwError(LOGMANAGER* log, char* servername, char* errstring)
{
  char errorInfo[100];
  snprintf(errorInfo, 100, "%s:%s", servername, errstring);

  Log_ThrowFormat(log, ERRSEV_USERWARN, ERRID_CONNECTION, errorInfo);
}

SERVERNODE* Server_Connect(LOGMANAGER* errors, char* address, char* port)
{
  SERVERNODE* result = calloc(1, sizeof(SERVERNODE));

  if(result)
  {
    time(&result->dirtyTime);
    result->errormanager = errors;
    if(!Server_Connect_P(result, address, port))
    {
      free(result);
      result = NULL;
	  throwError(errors, address, "Failed to connect!");
    }
#ifdef SERVERDEBUG
    else
    {
      printf("Connected\n");
    }
#endif
  }

  return result;
}

PUBLIC bool Server_Close(SERVERNODE** connection)
{
  if(*connection)
  {
    #ifdef _WIN32
      closesocket((*connection)->w_sd);
    #else
      close((*connection)->fd);
    #endif

#ifdef SERVERDEBUG
    printf("Server closed\n");
#endif

    free(*connection);
    *connection = NULL;
    return true;
  }
  return false;
}

PUBLIC bool Server_CheckTimeout(SERVERNODE* server, char** errorset)
{
  //Check age of server node
  time_t currentTime;
  time(&currentTime);

  if(currentTime - server->dirtyTime >= MAX_TIMEOUT)
  {
    *errorset = "Connection Timed out!\n";
    return -1;
  }
  else return 1;
}

PUBLIC SERVERMESG Server_Get(SERVERNODE* server, SVRGET getFunc, void* data)
{
  int readTop;
  int result = MSG_NOTDONE;
  bool readDone = false; //< Flag to determine possible connection timeouts
  
#ifdef _WIN32
  readTop = recv(server->w_sd, server->readBuffer, sizeof(server->readBuffer), 0) - 1;
#else
  readTop = read(server->fd, server->readBuffer, sizeof(server->readBuffer)) - 1;
#endif
  if(readTop)
  {
    int i;
    readDone = true;

    for(i = 0; i<=readTop; i++)
    {
      if(server->bufferAt == 0)
      {
        if(server->readBuffer[i] == MESSAGE_HEADER)
          server->bufferAt++;
      }
      else
      {
        if(server->readBuffer[i] != MESSAGE_FOOTER)
        {
          server->messageBuffer[server->bufferAt - 1] = server->readBuffer[i];
          server->bufferAt++;
        }
        else
        {
          char header = *server->messageBuffer;
          char* msg   = server->messageBuffer + 1;
          int getResult = 0;
          
          server->messageBuffer[server->bufferAt - 1] = '\000';
          if(result != MSG_USER)
            result = MSG_SUCCESS;
          time(&server->dirtyTime);

          if(header == SIGNAL_BEGIN && !server->listRecv)
          {
            sscanf(msg, "%i", &server->listRecv);
          }
  
#ifdef SERVERDEBUG
  printf("Header:%d, Mesg:%s\n", header, msg);
#endif
  
          getResult = getFunc(header, msg, server->listRecv, data);
          
          switch(getResult)
          {
            case -1: 
              return MSG_FAIL;
            case 1:
              result = MSG_USER;
            default:
              if(header == SIGNAL_END && server->listRecv)
              {
                server->listRecv = 0;
              }

              server->bufferAt = 0;
              break;
          }
        }
      }
    }
  }

  if(!readDone)
  {
    //Check Errors
    char* err;
    int isAlive = Server_CheckTimeout(server, &err);
    if(isAlive == -1)
    {
      printf("%s\n", err);
      result = MSG_EXIT;
    }
  }

  return result;
}

PUBLIC bool Server_CheckBusy(SERVERNODE* server)
{
  return (
    server->writeAt == 0 
 && server->nextList == NULL
 && server->listSend == 0
 && server->sendBuffer[1] != SIGNAL_END
  );
}

//Sets the send buffer
PRIVATE void Server_SetMesg(SERVERNODE* server, char header, const char* message)
{
  #ifdef SERVERDEBUG
    printf("Header:%d, Mesg:%s\n", header, message);
  #endif
  snprintf(server->sendBuffer, MAX_MESSAGE_SIZE, "%c%c%s%c", MESSAGE_HEADER, header, message, MESSAGE_FOOTER);
}

#define NUM_MSG(num) char msg[10]; snprintf(msg, 10, "%i", num);

PRIVATE SERVERMESG doWrite(SERVERNODE** server, int resetCount);

PRIVATE void ResetFields(SERVERNODE* server)
{
  //Reset server fields
  *(server->sendBuffer) = '\0';
  server->userdata = NULL;
  server->listSend = 0;
  server->nextList = NULL;
  return;
}

//Processes the node->nextList Element
PRIVATE bool Server_SetList(SERVERNODE** server)
{
  LIST nlist = (*server)->nextList;
  if(nlist) // There is a new element available
  {
      char userbuffer[MAX_MESSAGE_SIZE - 3];
      //Call user format funtion to get message info
      char header = SIGNAL_NODATA;
      while(header == SIGNAL_NODATA && nlist)
      {
        header = (*server)->genfun(userbuffer,
             MAX_MESSAGE_SIZE - 3, nlist->object, nlist->objType, (*server)->userdata);
        nlist = nlist->sibling;
      }

      if(header != SIGNAL_NODATA)
      {
        //Set the user buffer
        Server_SetMesg((*server), header, userbuffer);

        //Prepare nextList for next time
        (*server)->nextList = nlist;

        return doWrite(server, false);
      }
      else if(!nlist)
      {
        (*server)->nextList = nlist;
        goto LISTISDONE;
      }
  }
  else //There is no list left, so we should set the list end message
  {
LISTISDONE:
    if((*server)->listSend && (*server)->listSend != LIST_NONE)
    {
      NUM_MSG((*server)->listSend);
      Server_SetMesg((*server), SIGNAL_END, msg);
      (*server)->listSend = 0;
      return MSG_NOTDONE;
    }
    else
    {
      ResetFields((*server));
      return MSG_SUCCESS;
    }
  }
  return MSG_FAIL;
}

#define DOCLOSE \
  fflush(stdout);\
  if(Server_Close(server)) return MSG_NOCONN;

PRIVATE SERVERMESG doWrite(SERVERNODE** server, int resetCount)
{
  bool result = MSG_NOTDONE;
  if(server)
  {
    int buffTop = strlen((*server)->sendBuffer);
    if(buffTop == 0)
    {
      result =  MSG_SUCCESS;
    }

    static int counter = 0;
    if(resetCount) counter = 0;
    if(counter > 25)
    {
      return MSG_NOTDONE;
    }
    counter ++;

#ifdef _WIN32
    int bytes = send((*server)->w_sd, (*server)->sendBuffer, buffTop - (*server)->writeAt, 0);
    if(bytes == SOCKET_ERROR)
    {
    LPSTR errString = NULL;

    FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, 
             0, WSAGetLastError(),0,
             (LPSTR)&errString, 0, NULL);
    throwError((*server)->errormanager, (*server)->w_name, errString);
    DOCLOSE;
    }
#else
    int bytes = send((*server)->fd, (*server)->sendBuffer, buffTop - (*server)->writeAt, MSG_NOSIGNAL);
    if(bytes == -1)
    {
      if((*server)->errormanager)
      {
        throwError((*server)->errormanager, inet_ntoa((*server)->addr), strerror(errno));
      }
      DOCLOSE;
    }
#endif

    (*server)->writeAt += bytes;
    if((*server)->writeAt == buffTop)
    {
      (*server)->sendBuffer[1] = 0;
      (*server)->writeAt = 0;
      result = Server_SetList(server);
    }
  }
  return result;
}

PUBLIC SERVERMESG Server_Heartbeat(SERVERNODE** server)
{
  if(Server_CheckBusy((*server)))
  {
    return Server_PutMesg(server, SIGNAL_HEARTBEAT, FORMAT_HEARTBEAT);
  }
  return MSG_FAIL;
}

PUBLIC SERVERMESG Server_Write(SERVERNODE** server)
{
  return doWrite(server, true);
}

PUBLIC SERVERMESG Server_PutMesg(SERVERNODE** server, char header, const char* mesg)
{
  int result = MSG_NOTDONE;
  if(Server_CheckBusy((*server)))
  {
    int msgLen = strlen(mesg) + 4;
    if(msgLen <= MAX_MESSAGE_SIZE)
    {
      Server_SetMesg((*server), header, mesg);
      return Server_Write(server);
    } else return MSG_FAIL;
  }
  return result;
}

PUBLIC SERVERMESG Server_PutList(SERVERNODE** server, LIST list, SVRGEN format, int listID, void* userdata)
{
  //Test if a list is specified already
  if(Server_CheckBusy(*server) && !(*server)->listSend && listID)
  {
    // Start to send the list by preparing the node.
    (*server)->listSend = listID;
    (*server)->nextList = list;
    (*server)->genfun = format;
    (*server)->userdata = userdata;

    // Set the message to the list begin signal
    // Server_Write will call the first element
    if((*server)->listSend != LIST_NONE)
    {
      NUM_MSG((*server)->listSend);
      Server_SetMesg((*server), SIGNAL_BEGIN, msg);
      //Write all it can to the server
      return doWrite(server, true);
    } else return Server_SetList(server);
  }

  return MSG_FAIL;
}

SERVERNODE* Server_Host(LOGMANAGER* errors, char* port)
{
  SERVERNODE* result = calloc(1, sizeof(SERVERNODE));

  if(result)
  {
    result->errormanager = errors;
    if(Server_Host_P(result, port))
    {
#ifdef SERVERDEBUG
printf("Server Hosted\n");
#endif
      time(&result->dirtyTime);
    }
    else
    {
      free(result);
      result = NULL;
    }
  }
  return result;
}

SERVERNODE* Server_Accept(LOGMANAGER* errors, SERVERNODE* node)
{
  SERVERNODE* result = calloc(1, sizeof(SERVERNODE));
  if(result)
  {
    result->errormanager = errors;
    if(Server_Accept_P(result, node))
    {
#ifdef SERVERDEBUG
printf("Client Accepted\n");
#endif
      time(&result->dirtyTime);
    }
    else     
    {
      free(result);
      result = NULL;
    }
  }
  return result;
}
