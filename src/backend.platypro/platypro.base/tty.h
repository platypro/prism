/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef H_PLM_BASE_TTY
#define H_PLM_BASE_TTY

/** \file tty.h
 *  \defgroup platypro_tty TTY Tools
 
    Various TTY helper functions.

 *  \ingroup platypro_base
 */
/** @{ */

//! Prepare terminal for character input
extern bool Term_Init();

//! Return the terminal to it's previous state
extern bool Term_Cleanup();

//! Get the next character of input stream
extern char Term_Getch();

/** @} */
#endif