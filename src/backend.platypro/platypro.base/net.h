/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef H_PLM_BASE_NET
#define H_PLM_BASE_NET

/** \file net.h

    Non-block network API. It sits on top of TCP. 
    To debug connections, make sure to uncomment
    SERVERDEBUG in the header. 

 *  \defgroup platypro_net Networking API
 *  \ingroup platypro_base
 */
/** @{ */

//Server Debug Traces
//Uncomment to see verbose output
//	#define SERVERDEBUG

//Include Network stuff
#ifdef _WIN32
 #include <winsock2.h>
 #include <ws2tcpip.h>
#else
 #include <sys/socket.h>
 #include <netinet/in.h>
 #include <netinet/ip.h>
 #include <arpa/inet.h>
 #include <netdb.h>
 #include <sys/types.h>
#endif

#include <time.h>

#include "platypro.base/list.h"
#include "platypro.base/log.h"

#define MESSAGE_HEADER '\255'
#define MESSAGE_FOOTER '\254'

#define ERRID_CONNECTION  "et_connect"
#define STR_CONNECTION_NEW "str_CONNECTION_NEW"

#define MAX_MESSAGE_SIZE 515
#define READ_BUFFER_SIZE 1024

#define MSG_USER    2   //!< User function reported successful
#define MSG_SUCCESS 1   //!< Function reported successful
#define MSG_NOTDONE 0   //!< Function needs to be called again
#define MSG_FAIL   -1   //!< Function failed
#define MSG_EXIT   -1   //!< Function should no longer be called
#define MSG_NOCONN -2   //!< Server lost connection

/*! Server message type.
 *  It takes the form of "MSG_*" defines. If the exact message is not
 *  neccisary, the value can be compared against zero. If it is
 *  above zero, the function succeeded. If it is below zero, then the
 *  function failed. If it is set to zero, the applicable function
 *  (either Server_Get or Server_Write) needs to be called again.
 */
typedef char SERVERMESG;

#define SIGNAL_NODATA -1

#define MAX_TIMEOUT 20

//! Defines a new signal. Anything unique can be picked for either value. Class 0 is reserved.
#define SIGNAL_(class, num) (class << 4) + num

#define SIGNAL_BEGIN     SIGNAL_(0,1) //!< List begins
#define SIGNAL_END       SIGNAL_(0,2) //!< List ends
#define SIGNAL_HEARTBEAT SIGNAL_(0,3) //!< Heartbeat signal

#define FORMAT_HEARTBEAT ""

#define LIST_NONE 3

typedef int (*SVRGET) (char header, char* request, int listID, void* connection);

/*! Generates a string for a server
	\returns The message header
*/
typedef char (*SVRGEN) (char* string, int maxlen, void* data, int objtype, void* user);

/*! A server node.
 *  Can represend either a hosted server, a connection to a server, or a client.
 */
typedef struct ServerNode
{
	#ifdef _WIN32
		WSADATA w_data;
		SOCKET w_sd;
		char* w_name;	
	#else
		int fd;
		struct in_addr  addr;
	#endif
	
	time_t dirtyTime;
	LOGMANAGER* errormanager;

	//Properties for recieving data

	char messageBuffer[MAX_MESSAGE_SIZE]; //!< Buffer for incomplete messages
	char readBuffer[READ_BUFFER_SIZE];    //!< Temporary cache for reads
	int listRecv; //!< Stores the ID of the current list which is being recieved
	int bufferAt;

	//Properties for sending data

	char sendBuffer[MAX_MESSAGE_SIZE]; //!< Buffer for incomplete writes
	int listSend;  //!< ID of list that is being sent
	LIST nextList; //!< Next element of the list that is being sent
	int writeAt;

	//int writeID;
	SVRGEN genfun;  //!< Function to convert list elements into strings
	void* userdata; //!< User data argument for genfun
} SERVERNODE;

/*! \brief Connects to a server
 *  \param log      The log store to write to when issues arise.
 *  \param address  The server address to connect to.
 *  \param address  The server port to connect to.
 *  \returns  A server connection
 */
extern SERVERNODE* Server_Connect(LOGMANAGER* log, char* address, char* port);

/*! \brief Closes a connection to a server
 *  \param node     Connection to close.
 *  \returns Success
 */
extern bool Server_Close(SERVERNODE** node);

/*! \brief Checks if the server is ready to deliver another message.
 *  \param node     Connection to check
 *  \returns Whether server is ready
 */
extern bool Server_CheckBusy(SERVERNODE* node);

/*! \brief Grabs pending messages.
 *  \param node     The node to recieve messages from.
 *  \param getFunc  The function to be called for each incoming message
 *  \param data     Additional data to pass to the callback
 *  \returns Current server status
 */
extern SERVERMESG Server_Get(SERVERNODE* node, SVRGET getFunc, void* data);

/*! \brief Writes everything possible to the server node.
 *  \param node     The node to write to
 *  \returns Current server status.
 */
extern SERVERMESG  Server_Write(SERVERNODE** node);

/*! \brief Sends a small heartbeat signal
 *  \param node     The node to write to
 *  \returns Current server status.
 */
extern SERVERMESG Server_Heartbeat(SERVERNODE** node); //! Send a heartbeat signal

/*! \brief Writes a list to the server node
 *  \param node     The node to write to
 *  \param list     The list to write
 *  \param format   The function to generate signals based off of the list items
 *  \param data     Additional data to pass to the callback
 *  \returns Current server status.
 */
extern SERVERMESG  Server_PutList(SERVERNODE** node, LIST list, SVRGEN format, int listID, void* data);

/*! \brief Writes a message to the server node
 *  \param node     The node to write to
 *  \param header   The message header
 *  \param mesg     The message to write
 *  \returns Current server status.
 */
extern SERVERMESG Server_PutMesg(SERVERNODE** node, char header, const char* mesg);

/*! \brief Hosts a server
 *  \param log      The log store to write to when issues arise.
 *  \param port     The port to listen on
 *  \returns A server connection representing the server
 */
extern SERVERNODE* Server_Host(LOGMANAGER* log, char* port);

/*! \brief Accepts a singular client. This is to be continually called until it returns NULL.
 *  \param log      The log store to write to when issues arise.
 *  \param port     A server connection representing the server, returned from Server_Host
 *  \returns A server connection representing the new client
 */
extern SERVERNODE* Server_Accept(LOGMANAGER* log, SERVERNODE* node);

/** @} */
#endif