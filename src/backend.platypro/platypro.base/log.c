/*
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "platypro.base/common.h"
#include "platypro.base/log.h"

#include <platypro.base/strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

LOGMANAGER* Log_Init(char* logPath, LIST stringFile)
{
  LOGMANAGER* result = calloc(1, sizeof(LOGMANAGER));
  if(result)
  {
    result->stringFile = stringFile;
    result->logFile = fopen(logPath, "a+");
    if(!result->logFile)
    {
      free(result);
      result=NULL;
    }
  }
  return result;
}

void Log_Cleanup(LOGMANAGER** errors)
{
  fclose((*errors)->logFile);
  free(*errors);
  *errors = NULL;
  return;
}

void Log_SetWarnCallback(LOGMANAGER* errors, LOGCALLBACK callback, void* user)
{
  errors->userWarnCallback = callback;
  errors->userWarnData = user;
  return;
}

void Log_Throw(LOGMANAGER* errors, char* severity, char* message)
{
  time_t currenttime;
  struct tm* timeinfo;
  char formattime[25];
  char fullmessage[ERR_MAX_LEN];

  time(&currenttime);
  timeinfo = localtime(&currenttime);

  strftime(formattime, 25, "[%F %T]", timeinfo);

  snprintf(fullmessage, 255, "%4s [%s] %s\n", formattime, severity, message);

  fprintf(errors->logFile, "%s", fullmessage);
   printf("%s", fullmessage);

  if(!strcmp(severity, ERRSEV_USERWARN) && errors->userWarnCallback)
  {
    errors->userWarnCallback(errors->userWarnData, severity, message);
  }

  return;
}

PRIVATE char* Log_Gen(LIST strings, char* ID, const char* info)
{
    char* result     = calloc(1, ERR_MAX_LEN);
    char* error      = Strings_Get(strings, ID);

    int len = snprintf(result, ERR_MAX_LEN, "%s", error);
    if(info)
    {
        len+=snprintf(result + len, ERR_MAX_LEN - len, ": %s", info);
    }

    return result;
}

void Log_ThrowFormat(LOGMANAGER* errors, char* severity,
    char* emesgID, const char* custommesg)
{
  char* error = Log_Gen(errors->stringFile, emesgID, custommesg);
  Log_Throw(errors, severity, error);
  free(error);
}
