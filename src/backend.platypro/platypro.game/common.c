/*
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <platypro.game/common.h>

PUBLIC IO_ROUND getRoundFromRect(IO_RECT rect)
{
  IO_ROUND result;
  result.rx = (rect.w / 2);
  result.ry = (rect.h / 2);
  result.x = rect.x + result.rx;
  result.y = rect.y + result.ry;
  return result;
}

PUBLIC bool testPointInRound(IO_ROUND round, uint32_t x, uint32_t y)
{
  IO_POINT normalized = {x - round.x,
                         y - round.y};

  return ((((double)(normalized.x * normalized.x)
           / (round.ry * round.rx)) + ((double)(normalized.y * normalized.y) / (round.rx * round.ry)))
      <= 1.0);
}

PUBLIC bool testPointInRect(IO_RECT rect, uint32_t x, uint32_t y)
{
  return (x > rect.x && x < rect.x + rect.w
       && y > rect.y && y < rect.y + rect.h);
}

PUBLIC bool testRectRoundCollision(IO_ROUND round, IO_RECT rect)
{
  //Test Round collision with Rect
  if( testPointInRect(rect, round.x - round.rx, round.y )  //Left Side
  ||  testPointInRect(rect, round.x + round.rx, round.y )  //Right Side
  ||  testPointInRect(rect, round.x , round.y - round.ry)  //Top Side
  ||  testPointInRect(rect, round.x , round.y + round.ry)) //Bottom Side
  {  return true; }

  //Test Rect Collision with Round
  if( testPointInRound(round, rect.x          , rect.y) //Top Left
  ||  testPointInRound(round, rect.x + rect.w , rect.y) //Top Right
  ||  testPointInRound(round, rect.x , rect.y + rect.h) //Bottom Left
  ||  testPointInRound(round, rect.x + rect.w , rect.y + rect.h))
  {  return true; }
  return false;
}

PUBLIC bool testRectCollision(IO_RECT* rect1, IO_RECT* rect2)
{
  return (rect1->x < rect2->x + rect2->w &&
     rect1->x + rect1->w > rect2->x &&
     rect1->y < rect2->y + rect2->h &&
     rect1->h + rect1->y > rect2->y);
}