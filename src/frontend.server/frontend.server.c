/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include "common.h"
#include "frontend.server.h"

#include "platypro.base/tty.h"
#include "platypro.base/tick.h"
#include "platypro.base/strings.h"
#include "platypro.game/multiplayer.h"
#include "server.h"

#include <stdint.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>

static SERVERSTATE* globalServerState = NULL;

PRIVATE bool getNewClients(SERVERSTATE* state)
{
    SERVERNODE* cnode = NULL;
    bool result = false;
    
    do
    {
      cnode = Server_Accept(state->log, state->server);
      if(cnode)
      {
        result = Game_AddPlayer(cnode, state);
      }
    }
    while(cnode);
    return result;
}

PRIVATE void cleanup(SERVERSTATE** state)
{
  SERVERSTATE* ss = *state;
  Log_ThrowFormat((*state)->log, ERRSEV_INFO, STR_SERVER_STOP, NULL);
  if(ss)
  {
    if(ss->script)
      Script_Close(ss->script);
    if(ss->players)
      Player_Clean(&ss->players);
    if(ss->levelData)
      Script_CleanData(&ss->levelData);
    if(ss->server)
      Server_Close(&ss->server);
    if(ss->collisionData)
      ClearCollisions(&ss->collisionData);
    if(ss->log)
      Log_Cleanup(&ss->log);
    if(ss->strings)
      Strings_Close(&ss->strings);
    
    Term_Cleanup();

    memset(ss, 0, sizeof(SERVERSTATE));
    free(ss);
    *state = NULL;
    globalServerState = NULL;
  }
}

PRIVATE SERVERSTATE* init(char* execpath, char* script, char* port, char gamemode)
{
  //Initialize terminal for getch
  Term_Init();
  
  //Init main struct
  SERVERSTATE* state = calloc(1, sizeof(SERVERSTATE));
  if(!state) goto INIT_ERROR;

  //General main struct stuff
  globalServerState = state;
  state->gs = GS_WAITFORPLAYERS;
  state->gm = gamemode;
    
  //Init Strings
  state->strings = Strings_Load(DEF_STRINGFILE, false);
  if(!state->strings) goto INIT_ERROR;
  
  //Init error handling
  char logname[255];
  snprintf(logname, 255, "%s.log", execpath);
  state->log  = Log_Init(logname, state->strings);
  if(!state->log) goto INIT_ERROR;

  Log_ThrowFormat(state->log, ERRSEV_INFO, STR_SERVER_START, port);

  //Init server
  state->server = Server_Host(state->log, port);
  if(!state->server) goto INIT_ERROR;

  Log_ThrowFormat(state->log, ERRSEV_INFO, STR_SCRIPT_INIT, script);

  //Init Script
  state->script = Script_Init(script);
  if(!state->script) goto INIT_ERROR;

  Log_ThrowFormat(state->log, ERRSEV_INFO, STR_ROOM_GEN, NULL);
  
  //Init Game Room
  state->levelData = Script_GenRoom(state->script);
  if(!state->levelData) goto INIT_ERROR;
    
  //Generate collision data for room
  state->collisionData = GenerateCollsions
    (
      state->levelData->fg,
      state->levelData->zonew, 
      state->levelData->zoneh
    );
  
  //Init player management
  state->players = Player_Init(sizeof(SERVERPLAYER), GAME_TYPETOP, Game_Put, Game_Get, Game_Update, state);
  if(!state->players) goto INIT_ERROR;
  
  //Set gamemode to GAMEMODE_NONE if there are not enough teams to play
  if(state->levelData->teamCount < 2)
  {
    Log_Throw(state->log, ERRSEV_INFO, Strings_Get(state->strings, STR_SERVER_NOTEAMS));
    state->gm = GAMEMODE_NONE;
  }
  
  if(state->gm == GAMEMODE_NONE)
  {
    state->gs = GS_INGAME; 
  }
 
  char buffer[MAX_MESSAGE_SIZE];
 
  switch(state->gm)
  {
    case GAMEMODE_TIMED:
      state->gameGoal = DEF_GM_TIMED_MAX;
      break;
    case GAMEMODE_THRESHOLD:
      state->gameGoal = DEF_GM_THRESHOLD_MAX;
      break;
    default: state->gameGoal = 0;
  }
  
  snprintf
  (
    buffer, MAX_MESSAGE_SIZE, FORMAT_GAMEMODE,
    state->gm,
    state->gameGoal
  );
  
  //Push gamemode data
  Player_PushMesg(state->players, SIGNAL_GAMEMODE, buffer);
  
  snprintf
    (
      buffer, MAX_MESSAGE_SIZE, FORMAT_DOWNLOADDATA,
      List_Count(&state->levelData->bg) + List_Count(&state->levelData->fg),
      state->levelData->zonew, 
      state->levelData->zoneh
    );
  
  //Push room metadata
  Player_PushMesg(state->players, SIGNAL_ROOMDATA, buffer);
  
  //Push room data
  Player_PushList(state->players, LIST_BG, state->levelData->bg, Game_PutStatic);
  Player_PushList(state->players, LIST_FG, state->levelData->fg, Game_PutStatic);
  
  //Push team data
  Player_PushList(state->players, LIST_FLAG, state->levelData->team, Game_PutStatic);
  
  return state;
  
INIT_ERROR:
  cleanup(&state);
  return NULL;
}

//This is called on program termination
void terminate()
{
  if(globalServerState)
    cleanup(&globalServerState);
  
  printf("Terminated\n");
  return;
}

void interrupt()
{
 exit(0); 
}

int main(int argc, char** argv)
{  
  SERVERSTATE * state;
  TICK_OBJ tickClock;
  
#ifdef _WIN32
  tickClock = (LARGE_INTEGER){0};
#else
  tickClock = (struct timespec){0,0};
  signal(SIGPIPE, SIG_IGN);
#endif
  signal(SIGINT, interrupt);
  atexit(terminate);

  
  //Argument usage: Prism-Server <script> <gamemode> <port>
  
  char* script = DEF_SCRIPT;
  char* port   = DEF_PORT;
  char gamemode = DEF_GAMEMODE;
  
  if(argc > 1)
    script = argv[1]; 

  if(argc > 2)
    gamemode = atoi(argv[2]);
  
  if(argc > 3)
    port = argv[3];

  //Print warranty/license info
  printf("%s (%s)\n%s\n%s\n%s\n", VERSION_NAME, VERSION, VERSION_AUTHOR, VERSION_LICENSE, VERSION_WARRANTY );
  
  if((state = init(argv[0], script, port, gamemode)))
  {    
    Tick_Get(&tickClock);
    Log_ThrowFormat(state->log, ERRSEV_INFO, STR_SERVER_READY, NULL);
    while(state)
    {
      getNewClients(state);
     
      //Game Ticks
      int ticks = Tick_Get(&tickClock);
      
      //Game timer detection
      if(state->gs == GS_INGAME) // Only tick if game running
      {
        state->secondTick += ticks;
        if(state->secondTick >= TICKS_PERSEC)
        {
          state->secondTick -= TICKS_PERSEC;
          state->gameTime++;
          
          if(state->gm == GAMEMODE_TIMED && state->gameTime > state->gameGoal)
          {
            //End the game
            Game_End(state);
          }
        }
      }
      
      if(state->players)
      {
        int oldpCount = PLAYER_GETCOUNT(state->players);
        Player_Update(state->players, ticks);
        
        if(PLAYER_GETCOUNT(state->players) == 0 && oldpCount > 0 && state->gm != GAMEMODE_NONE)
        {
          if(state->levelData->teamCount > 0)
            state->gs = GS_WAITFORPLAYERS;
          Log_ThrowFormat(state->log, ERRSEV_INFO, STR_SERVER_EMPTY, NULL);
          state->playerReadyCount = 0;
          Game_Reset(state);
        }
      }
      
      if(state->gs == GS_INGAME && gamemode == GAMEMODE_DEBUG)
      {
        //End game immediately
        state->gs = GS_WAITFORPLAYERS;
        Player_Order(state->players, ORDER_ENDGAME);
        Game_Reset(state);
      } 
      
      //Quit Detection
      if(Term_Getch() == 'q')
      {
        break;
      }

      //Make sure loop is not busy
#ifdef WINDOWS
      Sleep(1);
#else
      usleep(8000);
#endif
    } 
    //Clean up
    cleanup(&state);
  }

  return 0;
}
