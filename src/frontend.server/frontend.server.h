#ifndef INC_PRISM_SERVER
#define INC_PRISM_SERVER

/** \file Prism Server Header
 *  \addtogroup Prism-Server
 */
/** @{ */

#include <platypro.base/log.h>
#include <platypro.base/net.h>
#include <platypro.game/multiplayer.h>
#include "script.h"
#include "collision.h"

/** @} */

#endif
