/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include "common.h"

PRIVATE PLAYER* Team_Player_Last(PLAYER* p)
{
  while(p->nextTeam != NULL)  { p = p->nextTeam; }
  return p;
}

void Team_Player_Add(TEAM* t, PLAYER* p)
{
  if(!t->firstChild)
    t->firstChild = p;
  else 
  {
    PLAYER* last = Team_Player_Last(t->firstChild);
    last->nextTeam = p;
    p->prevTeam = last;
  }
  
  p->team = t;
  t->childCount++;
  
  return;
}

void Team_Player_Remove(PLAYER* p)
{
  if(p->prevTeam)
    p->prevTeam->nextTeam = p->nextTeam;
  else
    p->team->firstChild = p->nextTeam;
  
  if(p->nextTeam)
    p->nextTeam->prevTeam = p->prevTeam;
  
  p->team->childCount--;
  
  p->team = NULL;
  p->prevTeam = NULL;
  p->nextTeam = NULL;
  
  return;
}