/*
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_PRISM_CLIENT
#define H_PRISM_CLIENT

#include "platypro.base/net.h"
#include "platypro.base/list.h"
#include "platypro.base/tick.h"
#include "platypro.game/tools.h"
#include "collision.h"

#define CHAT_CACHE_SIZE MAX_MESSAGE_SIZE

typedef struct ServerConnection
{
  SERVERNODE* node;
  bool isWaitingForClose; //!< Flag for determining where connection is presumably shutting down

  TICK_OBJ tickStatus; //!< Stores tick time
  PLAYER oldPlayer;        //!< The last player data sent

  LOGMANAGER* log;
  
  GAMESTAGE gameStage;  //!< Bool determining whether connection is in-game

// Boolean values for determining what data needs to be sent to the server
  bool update_player;      //!< Determines whether the server needs new local player data
  bool update_ready;       //!< Determines whether the server needs new ready status
  bool update_pname;       //!< Determines whether the server needs new player name
  bool update_team;        //!< Determines whether the server needs new player team
  
// Room and team data
  COLLISIONLIST* fgCollisions;
  COLLISIONLIST* bgCollisions;

  LIST fg;      //!< List of foreground objects
  LIST bg;      //!< List of background objects
  
  LIST teams;   //!< List of team bases 
  LIST playerStore;
  
  GAMEMODE gamemode; //!< Current Game Mode
  int gamegoal;      //!< Current Game Goal
  int secondTick;    //!< Ticks since last time gametime incremented
  int gametime;      //!< Time in seconds which game has been running
  
  struct ClientPlayer* activePlayer; //!< Pointer to active player
  int playerID;                      //!< ID of active player
  
  bool hasName; //!< Flag for determining whether the name has been recieved (can we begin)
  
  char chatHistory[CHAT_CACHE_SIZE];
  int chatAt;
} SERVERCONNECTION;

typedef struct RoomDownload
{
  SERVERCONNECTION* connection;

  //Progress Fields
  int downloadTotal; //!< Total count of objects to be downloaded
  int downloadAt;    //!< Current number of objects downloaded

  //Room Dimensions
  // (Will eventually end up in collision data)
  int roomW;    //!< Room Width (Pixels)
  int roomH;    //!< Room Height (Pixels)

  char bgReady; //!< Flag to be set when background objects are downloaded
  char fgReady; //!< Flag to be set when foreground objects are downloaded
} ROOMDOWNLOAD;

typedef struct Buffer
{
	//Buffer Dimensions
	IO_RECT dimens;

	//Buffer Data
	LIST objects;
	IO_TEX cache;
} BUFFER;

typedef struct ClientPlayer
{
  IO_POINT intent;
  PLAYER player;
} CLIENTPLAYER;

#define CHATBUFFERSIZE 25

//
// Client update functions 
// These functions retrieve new data from the server
//

// Gets the active player.
extern CLIENTPLAYER* Server_GetActivePlayer(SERVERCONNECTION* connection);

extern CLIENTPLAYER* Server_FindPlayer(SERVERCONNECTION* connection, int id);

extern bool Server_HandlePlayers(char header, char* request, SERVERCONNECTION* connection);

extern bool Server_Update(SERVERCONNECTION** server);

extern SERVERCONNECTION* Client_Connection_Create(LOGMANAGER* log, char* addr, char* port);
extern bool Client_Connection_Destroy(SERVERCONNECTION** connection);
extern void Client_RequestDisconnect(SERVERCONNECTION** connection);

extern void Client_Chat(SERVERCONNECTION* conn, char* chat);

extern int Client_Tick(SERVERCONNECTION** conn);

extern bool Client_UpdateVelocities(SERVERCONNECTION* connection, int amount, IO_POINT intentVelocity);

extern ROOMDOWNLOAD* Client_InitDownload(SERVERCONNECTION* connection);
extern int Client_RoomDownload(ROOMDOWNLOAD** download);
extern void Client_AbortDownload(ROOMDOWNLOAD** download);

//
// Server modification functions
// These functions request changes to the master server data store
//

//! Requests that the client's player execute their action
extern bool Server_RequestAction(SERVERCONNECTION* server);

extern bool Client_ClearVelocities(SERVERCONNECTION* connection, bool dir);
extern bool Client_SetVelocity(SERVERCONNECTION* connection, DIRECTION dir);

#endif
