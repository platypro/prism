/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef H_PRISM_COMMON
#define H_PRISM_COMMON

#include "platypro.base/common.h"
#include "platypro.game/common.h"

//
// Magic Constants
//

#define BUTTON_PADDING 15
#define VERTICAL_SPACING 5
#define LABEL_HEIGHT 35
#define BAR_HEIGHT 50

#define COLOR_BG    (IO_COLOR){0xB6,0xB6,0xB6,0xFF}
#define COLOR_FG    (IO_COLOR){0x00,0x00,0x00,0xFF}

#define PLAYER_NAMEMAX 24
#define PLAYER_STAMINA_MAX 90 //< Maximum stamina value

#define SCORE_FLAG 5
#define SCORE_STEAL 1

#define EXT_STR ".str"

//Defaults
#define DEF_PORT         "22133"
#define DEF_SCRIPT       "gen/arena_simple.lua"
#define DEF_SERVER       "platypro.net"
#define DEF_GAMEMODE     GAMEMODE_TIMED

#define DEF_GM_TIMED_MAX     90
#define DEF_GM_THRESHOLD_MAX 25

#define DEF_FONT_NORMAL  "Fonts/Roboto.ttf"
#define DEF_FONT_MONO    "Fonts/RobotoMono.ttf"

#define DEF_STRINGFILE   "prism"EXT_STR
#define DEF_SETTINGSFILE "prismsettings"EXT_STR

//
// Types
//

typedef struct Team
{
  int id;          //!< Team ID
  
  int childCount;  //!< Total count of players
  int teamscore;   //!< Total sum of all team scores for this round
  int flagCount;   //!< Number of flags this team contains
  
  IO_RECT base;    //!< Player base size and position
  
  struct Player* firstChild; //!< First item in linked list of players
} TEAM;

typedef struct Player
{
  int id;
  char name[PLAYER_NAMEMAX];
  
  int score;
  
  bool ready;   //!< Is the player ready to play?
  struct Player* hasFlag; //!< Does the player have a flag? If so, who from?
  
  unsigned char stamina; //!< How much stamina does the player have?
  
  IO_POINT pos;
  IO_POINT velocity;
  
  struct Player* nextTeam; //!< Next teammate
  struct Player* prevTeam; //!< Previous teammate
  
  struct Team* team; //!< Link to team info
} PLAYER;

//Header keys (for identifying objects)
typedef enum list_Header
{
  HEADER_LISTEND,
  HEADER_PLAYER,
  HEADER_PLAYERREM,
  HEADER_GAMEOBJECT,
  HEADER_RECT,
  HEADER_ROUND,
  HEADER_TEAM,
  HEADER_INT,
  HEADER_FG,
  HEADER_BG
} LIST_HEADER;

typedef enum GameStage
{
  //! No state, in menu or initialization
  GS_NOSTATE,
  
  //! Players in lobby, accept players
  GS_WAITFORPLAYERS,
  
  //! Game in play, don't accept players
  GS_INGAME,
  
  //! Between games
  GS_INTERGAME,
} GAMESTAGE;

//Gamemodes
typedef enum GameMode
{
  GAMEMODE_NONE,
  GAMEMODE_DEBUG,
  GAMEMODE_TIMED,
  GAMEMODE_THRESHOLD,
  GAMEMODE_CONTROL
} GAMEMODE;

extern void Team_Player_Add(TEAM* t, PLAYER* p);
extern void Team_Player_Remove(PLAYER* p);

//
//String IDs
//

//Error Types
#define ERRID_MEM    "et_mem"
#define ERRID_INIT   "et_init"
#define ERRID_KICK        "et_kick"
#define ERRID_SERVERCLOSE "et_close"
#define ERRID_SERVERFULL  "et_full"

//Messages
#define STR_SERVER_NOTEAMS "str_SERVER_NOTEAMS"
#define STR_SERVER_CONNECT "str_SERVER_CONNECT"
#define STR_WINDOW_LOAD    "str_WINDOW_LOAD"
#define STR_CLIENT_STARTED "str_CLIENT_STARTED"
#define STR_DOWNLOAD_STARTED "str_DOWNLOAD_STARTED"
#define STR_DOWNLOAD_FINISHED "str_DOWNLOAD_FINISHED"

//Server Messages
#define STR_SERVER_START "str_SERVER_START"
#define STR_SERVER_STOP  "str_SERVER_STOP"
#define STR_SERVER_READY "str_SERVER_READY"
#define STR_SERVER_EMPTY "str_SERVER_EMPTY"
#define STR_SCRIPT_INIT  "str_SCRIPT_INIT"
#define STR_ROOM_GEN     "str_ROOM_GEN"

//Server Chat Messages
#define STR_CHAT_PJOIN   "str_CHAT_PJOIN"
#define STR_CHAT_PLEAVE   "str_CHAT_PLEAVE"

//User info/tooltip messages
#define STR_INFO_CHAT     "str_INFO_CHAT"

//Game modes
#define STR_GM_TIMED      "str_GM_TIMED"
#define STR_GM_THRESHOLD  "str_GM_THRESHOLD"
#define STR_GM_DEBUG      "str_GM_DEBUG"
#define STR_GM_CONTROL    "str_GM_CONTROL"
#define STR_GM_NONE       "str_GM_NONE"
#define STR_GM_SPECTATE   "str_GM_SPECTATE"

//Menu Item string IDs
#define STR_MENU_JOINGAME "str_MENU_JOIN"
#define STR_MENU_QUICKJOIN "str_MENU_QUICKJOIN"
#define STR_MENU_HOSTGAME "str_MENU_HOST"
#define STR_MENU_SERVERMANAGER "str_MENU_SERVERS"
#define STR_MENU_SETTINGS "str_MENU_SETTINGS"
#define STR_MENU_EXIT          "str_MENU_EXIT"
#define STR_MENU_SETADDR  "str_MENU_SETADDR"
#define STR_MENU_SETPORT  "str_MENU_SETPORT"
#define STR_MENU_GOBACK   "str_MENU_GOBACK"
#define STR_MENU_CONNECTING  "str_MENU_CONNECTING"
#define STR_MENU_OKAY        "str_MENU_OKAY"
#define STR_MENU_CLICKCONTINUE "str_MENU_CLICKCONTINUE"
#define STR_MENU_ESCAPEABORT    "str_MENU_ESCAPEABORT"
#define STR_MENU_READYSTATUS   "str_MENU_READYSTATUS"
#define STR_MENU_NAMEINPUT     "str_MENU_NAME"
#define STR_MENU_FULLSCREEN    "str_MENU_FULLSCREEN"
#define STR_MENU_WAITINGFORPLAYER "str_MENU_WAITFORPLAYER"
#define STR_MENU_CHAT "str_MENU_CHAT"
#define STR_MENU_DISCONNECT "str_MENU_DISCONNECT"
#define STR_USERNAME  "str_USERNAME"

#define STR_YES "str_YES"
#define STR_NO  "str_NO"
#define STR_MENU_LEAVECONFIRM "str_MENU_LEAVECONFIRM"

#define STR_PLAYER "str_PLAYER"

#define STR_TEAM "str_TEAM"
#define STR_TEAM_JOIN "str_TEAM_JOIN"
#define STR_TEAM_DEAD "str_TEAM_DEAD"

#define STR_YOU    "str_YOU"
#define STR_SCORE  "str_SCORE"
#define STR_GM_TIMELEFT "str_GM_TIMELEFT"
#define STR_GM_WINAT    "str_GM_WINAT"
#define STR_GAME_END  "str_GAME_END"

//Setting IDs
#define SETTING_FULLSCREEN "fullscreen"
#define SETTING_USERNAME   "username"

#define SETTING_SERVERADDR "serveraddr"
#define SETTING_SERVERPORT "serverport"

#define SETTINGS_HEADER    "Pri'sm Settings file"

//
// Signals
//

#define SIGNAL_ROOMDATA    SIGNAL_(1,0)  //!< General Download data, required sending before gameplay.
#define SIGNAL_GAMEMODE    SIGNAL_(1,1)  //!< Set the gamemode
#define SIGNAL_CHAT        SIGNAL_(1,2)  //!< Chat Message

#define SIGNAL_PNAME       SIGNAL_(2,0)  //!< Player name
#define SIGNAL_TEAM        SIGNAL_(2,1)  //!< Sets a player's team
#define SIGNAL_PREADY      SIGNAL_(2,2)  //!< Data useful for download screen
#define SIGNAL_PLAYER      SIGNAL_(2,3)  //!< Data useful during gameplay

#define SIGNAL_SCORE       SIGNAL_(4,0)  //!< Sets a player score
#define SIGNAL_STAMINA     SIGNAL_(4,1)  //!< Sets stamina

#define SIGNAL_TEAMFLAG    SIGNAL_(5,1)  //!< Sets a team flag area
#define SIGNAL_FLAG        SIGNAL_(5,2)  //!< Sets player flag data
#define SIGNAL_TRANSFER    SIGNAL_(5,3)  //!< Team flag transfer

#define SIGNAL_RECT        SIGNAL_(10,0) //!< Rectangle type
#define SIGNAL_ROUND       SIGNAL_(10,1) //!< Round Type

#define VERSION_NAME      "Pri'sm: The Maze Game"
#define VERSION_AUTHOR    "(c) 2016-2018 Aeden McClain"
#define VERSION_LICENSE   "This program is released under the GNU GPL v3. You are free to change and redistribute it."
#define VERSION_WARRANTY  "Please note that this program is provided WITHOUT ANY WARRANTY."
#define VERSION           "alpha_02"

//List Headers
#define LIST_FG 4
#define LIST_BG 5
#define LIST_FLAG 6

//Orders
#define ORDER_BEGINGAME ORDER_(0)
#define ORDER_DEADTEAM ORDER_(1)
#define ORDER_PLIST ORDER_(2)
#define ORDER_ENDGAME ORDER_(3)

//Signal Formats
#define FORMAT_DOWNLOADDATA "%d_%d_%d" //Download Size, Room (W, H), Gamemode

#define FORMAT_GAMEMODE "%d_%d"

#define FORMAT_TRANSFER "%d_%d"

//Used for score signal and flag symbol
#define FORMAT_SCORE "%d_%d_%d" //Player, Player Score, Team Score

//ID, Velocity (X,Y), Position (X,Y)
#define FORMAT_PLAYER "%d_%d_%d_%d_%d"

//PID, Player Flag, TeamID, Team Flags
#define FORMAT_FLAG "%d_%d_%d_%d"

//ID, Player Name
#define FORMAT_PNAMEPRINT  "%d_%s\t"
#define FORMAT_PNAME       "%d_%[^\t]"

#define FORMAT_CHAT        "%[^\t]"

//Ready status of player
//ID, Ready Status
#define FORMAT_INT "%d_%d"

#define FORMAT_RECT     "%d_%d_%d_%d" // X, Y, W, H

//Common values
#define MOVEMENT_SPEED 2
#define RUN_SPEED 3
#define PLAYER_RADIUS  10

#define MAX_CHAT_SIZE 100

#endif