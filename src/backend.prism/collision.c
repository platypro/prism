/*
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include "common.h"
#include "collision.h"

#include <math.h>
#include <stdlib.h>

#include "platypro.base/list.h"

PRIVATE int findTile(int vector, int blockSize)
{
  return floor((double)vector / (double)blockSize);
}

PRIVATE void findOverlap(int* cache, IO_RECT* rect, int blockSize, int blockW)
{
  int cacheAt = 0;
  //Find Where each point of the rect lies
  int startX, startY, endX, endY, loopX, loopY;
  startX = findTile(rect->x, blockSize);
  startY = findTile(rect->y, blockSize);
  endX   = findTile(rect->x + rect->w, blockSize);
  endY   = findTile(rect->y + rect->h, blockSize);

  //Populate cache
  for(loopX = startX; loopX <= endX; loopX++)
  {
    for(loopY = startY; loopY <= endY; loopY++)
    {
      cache[cacheAt] = (loopY * blockW) + loopX;
      cacheAt ++;
    }
  }

  cache[cacheAt] = OVERLAP_END;

  return;
}

PRIVATE bool processRoomData(void* object, unsigned int key, void* data)
{
  COLLISIONLIST* collisionData = (COLLISIONLIST*)data;
  IO_RECT* rect = (IO_RECT*)object;

  int overlapCache[100];
  int* i = overlapCache;

  findOverlap(overlapCache, rect, COLLISION_BLOCK_SIZE, collisionData->tileW);

  while(*i != OVERLAP_END)
  {
    if(*i >= 0 && *i < collisionData->tileW * collisionData->tileH)
      List_Add(&collisionData->tiles[*i], rect, key);
    i++;
  }

  return true;
}

PUBLIC COLLISIONLIST* GenerateCollsions(LIST roomData, int roomw, int roomh)
{
  COLLISIONLIST* result = calloc(1, sizeof(COLLISIONLIST));
  if(result)
  {
    result->tileW = findTile(roomw, COLLISION_BLOCK_SIZE) + 1;
    result->tileH = findTile(roomh, COLLISION_BLOCK_SIZE) + 1;

    result->roomW = roomw;
    result->roomH = roomh;

    //Allocate list list
    result->tiles = calloc(result->tileH * result->tileW, sizeof(LIST));
    if(!result->tiles)
    {
      free(result);
      result = NULL;
    }
    else
    {
      //Fill list list with roomData item clones
      List_Map(&roomData, processRoomData, result);
    }
  }

  return result;
}

PUBLIC bool ClearCollisions(COLLISIONLIST** collisions)
{
  if(*collisions)
  {
    int i;
    for(i = 0; i < (*collisions)->tileH * (*collisions)->tileW; i++)
    {
      List_Clear(&(*collisions)->tiles[i]);
    }

    free((*collisions)->tiles);
    free(*collisions);
    *collisions = NULL;
  }

  return true;
}

//Fine phase collision detection for players
PUBLIC bool Collision_Fine(void* user, IO_RECT co1, LIST_HEADER ct1, IO_RECT co2, LIST_HEADER ct2)
{
  //Do special fine colision operations (if needed)
  if(ct1 == HEADER_ROUND && ct2 == HEADER_RECT)
    return testRectRoundCollision(getRoundFromRect(co1), co2);
  else if(ct1 == HEADER_RECT  && ct2 == HEADER_ROUND)
    return testRectRoundCollision(getRoundFromRect(co2), co1);

  return true;
}

PUBLIC bool HandleCollisionData(
    IO_RECT zone, LIST_HEADER zonetype, COLLISIONLIST* collisions, COLLISIONHANDLE ch, void* user)
{
  bool result = false;
  int overlapCache[50];
  int i;

  //Find collision tiles to process
  findOverlap(overlapCache, &zone, COLLISION_BLOCK_SIZE, collisions->tileW);

  //Find Broad phase collision
  for(i = 0; overlapCache[i]!=OVERLAP_END && i < NUMELEMENTS(overlapCache); i++)
  {
    if(overlapCache[i] >= 0 && overlapCache[i] < collisions->tileW * collisions->tileH)
    {
      IO_RECT* rect;
      LIST list = collisions->tiles[overlapCache[i]];
      if(list)
      {
        //Tile found, look for collision
        foreach(rect, list)
        {
          if(testRectCollision(rect, &zone))
            if( ch(user, zone, zonetype, *rect, element->objType) )
              result = true;
        }
      }
    }
  }
  return result;
}

PUBLIC bool DetectCollision (IO_RECT p, LIST_HEADER pType, COLLISIONLIST* collisions)
{
  //Test if rect is in room area
  if(p.x < 0 || p.y < 0 || p.w + p.x > collisions->roomW || p.h + p.y > collisions->roomH)
  {
    return true;
  }

  return HandleCollisionData(p, pType, collisions, Collision_Fine, NULL);
}

PRIVATE int GetSign(int num)
{
  return (num > 0) - (num < 0);
}

#define VELDETECT(xoff, yoff) \
  !DetectCollision( \
      (IO_RECT){p.x + result.x + (xoff), p.y + result.y + (yoff), p.w, p.h}, \
      pType, collisions)

PUBLIC IO_POINT GetCollision(IO_RECT p, LIST_HEADER pType, COLLISIONLIST* collisions, IO_POINT velocity)
{
  IO_POINT result = {0, 0};
  int signx = GetSign(velocity.x);
  int signy = GetSign(velocity.y);

  while(((result.x != velocity.x) && velocity.x) || ((result.y != velocity.y) && velocity.y))
  {
    if ((result.x != velocity.x) && velocity.x)
    {
      if(VELDETECT(signx, 0))
      {
        result.x+=signx;
      } else velocity.x = 0;
    }

    if ((result.y != velocity.y) && velocity.y)
    {
      if(VELDETECT(0, signy))
      {
        result.y+=signy;
      } else velocity.y = 0;
    }
  }
  return result;
}

IO_RECT getPlayerArea(IO_POINT p)
{
  return (IO_RECT){p.x - PLAYER_RADIUS, p.y - PLAYER_RADIUS,
                   (PLAYER_RADIUS*2), (PLAYER_RADIUS*2)};
}

void updateStamina(PLAYER* p, int amount)
{
  if(p->stamina > 0 && (abs(p->velocity.x) > MOVEMENT_SPEED || abs(p->velocity.y) > MOVEMENT_SPEED))
  {
    p->stamina -= amount;
  } 
  else if(p->stamina < PLAYER_STAMINA_MAX && p->stamina > 0)
  {
    p->stamina += amount;
  }
  else if(p->stamina == 0 && p->velocity.x == 0 && p->velocity.y == 0)
  {
    p->stamina += amount;
  }
}

IO_POINT updateVelocity(PLAYER* p, int amount, COLLISIONLIST* collisionData)
{
  IO_POINT newV, collision;

  newV.x = (p->velocity.x * amount);
  newV.y = (p->velocity.y * amount);

  collision = GetCollision(getPlayerArea(p->pos),
      HEADER_ROUND, collisionData, newV);

  p->pos.x += collision.x;
  p->pos.y += collision.y;
  
  if(newV.x && !collision.x) 
    p->velocity.x = 0;
  if(newV.y && !collision.y)
    p->velocity.y = 0;
  
  updateStamina(p, amount);
  
  return collision;
}
