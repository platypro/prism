/*
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef H_PRISM_COLLISION
#define H_PRISM_COLLISION

#include "platypro.base/list.h"

#define COLLISION_BLOCK_SIZE 50
#define OVERLAP_END -67647857

typedef struct CollisionList
{
  LIST* tiles;
  int roomW;
  int roomH;

  int tileW;
  int tileH;
} COLLISIONLIST;

typedef bool (*COLLISIONHANDLE) (void* user, IO_RECT rect1, LIST_HEADER type1, IO_RECT rect2, LIST_HEADER type2);

extern bool HandleCollisionData(IO_RECT zone, LIST_HEADER zonetype, COLLISIONLIST* collisions, COLLISIONHANDLE ch, void* user);
extern COLLISIONLIST* GenerateCollsions(LIST roomData, int roomw, int roomh);
extern bool ClearCollisions(COLLISIONLIST** collisions);
extern IO_RECT getPlayerArea(IO_POINT p);
extern bool DetectCollision(IO_RECT p, LIST_HEADER collisionType, COLLISIONLIST* collisions);

extern void updateStamina(PLAYER* p, int amount);
extern IO_POINT updateVelocity(PLAYER* p, int amount, COLLISIONLIST* collisionData);

#endif