--Test Room

function Prism_Gen()
	--Zone test
	local oldw, oldh = prZone()
	prZone(50, 75)
	prZone(-1, 60)
	prZone(120, -1)
	
	--Foreground Object Test
	prRect (fg, 55, 15, 10, 10)
	prRound(fg, 55, 30, 10, 10)

	--Background Object Test
	prRect (bg, 70, 15, 10, 10)
	prRound(bg, 70, 30, 10, 10)

	--Oval test
	prRound(fg, 55, 45, 25, 10)

	--Flag Test
	prFlag(0, 100, 0, 20, 20)
	prFlag(1, 100, 20, 20, 20)
	prFlag(2, 100, 40, 20, 20)

	prFlag(1)
end
