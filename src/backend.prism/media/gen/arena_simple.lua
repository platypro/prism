function Prism_Gen()
  --Set room size
  prZone(500,300)
  
  --Set up both flags
  prFlag(0  , 125  , 50,  50)
  prFlag(450, 125  , 50,  50)
  
  --Put some walls in the middle
  prRect(fg, 100, 50 , 50,  50)
  prRect(fg, 100, 200, 50,  50)
  
  prRect(fg, 350, 50 , 50,  50)
  prRect(fg, 350, 200, 50,  50)
  
  prRect(fg, 225, 125, 50,  50)
end
