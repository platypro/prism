function Prism_Gen()
  --Set room size
  prZone(200,200)
  
  --Four test flags (each corner)
  prFlag(0  , 0  , 50, 50)
  prFlag(0  , 150, 50, 50)
  
  prFlag(150, 0  , 50, 50)
  prFlag(150, 150, 50, 50)
end
