--[[
		-- Maze Generation Script --
		-- (c) 2015 Aeden McClain --
	
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]

--[[
	Finds the Wall Value in table
	t	table to search
	x:	x search query
	y:	y search query
	c:	channel  query
--]]
local findValue = function(t, x, y, c)
	for i, item in pairs(t) do
		if item.x == x and item.y == y and item.c == c then
			return i
		end
	end
	return -1
end

--[[
	Checks if wall is adacent to cell {x,y}
	returns Boolean 
--]]
local isAdjacent = function(wall, x, y)
	return (wall.x == x     and wall.y == y)
		or (wall.x == x + 1 and wall.y == y and wall.c == 0)
		or (wall.x == x     and wall.y == y + 1 and wall.c == 1)
end

--[[
	Adds all walls adjacent to cell {x,y} to a contender list
	This is used post getEmptyCell() to mark all unused walls as contenders
	unproc:     pointer to unprocessed list
	contenders: pointer to contender list
--]]
local addToContend = function(x, y, unproc, contenders)
	local remWalls = {}
	--find all unprocessed walls which are adjacent to {x,y}
	for i, wall in pairs(unproc) do
		--check if wall is adjacent
		if isAdjacent(wall, x, y) then
			--Upgrade wall to contender list
			contenders[#contenders + 1] = unproc[i]
			remWalls[#remWalls + 1] = i
		end
	end
	--Remove upgraded walls from unproc
	while #remWalls > 0 do
		table.remove(unproc, remWalls[#remWalls])
		table.remove(remWalls,#remWalls)
	end
end

--[[
	Gets the first empty cell behind a wall
	This is used for testing if a contender is viable
	Returns: Table containing cell data. if no cell is found, -1 is returned
--]]
local getEmptyCell = function(wall,cellList)
	local x, y, nCheck, pCheck
	nCheck = false pCheck = false
	x=wall.x y=wall.y
	
	if findValue(cellList, x, y, 0) ~= -1 then nCheck = true end	
	if wall.c == 0 then		
		if findValue(cellList, x - 1, y, 0) == -1 then x = x - 1 else pCheck = true end
	elseif wall.c == 1 then
		if findValue(cellList, x, y - 1, 0) == -1 then y = y - 1 else pCheck = true end
	end

	if pCheck == true and nCheck == true then return -1 else return {x=x, y=y, c=0} end
end

--[[
	Executes maze digging, uses prim's algorithm for mazes
	Relies on:
	getEmptyCell()
	addToContend()
--]]
local doDigging = function(mazeData)
	local contend  = {} --Walls to be processed in random order
	local wallList = {} --Walls in maze
	local finishedCells = {{x=5,y=5,c=5}}
	addToContend(5,5, mazeData, contend)
	while (#mazeData > 0 or #contend > 0) do

		local wall = math.random(#contend)
		local cell = getEmptyCell(contend[wall], finishedCells)
		if cell == -1 then
			wallList[#wallList + 1] = contend[wall]
			table.remove(contend, wall)
		else
			finishedCells[#finishedCells + 1] = cell
			table.remove(contend, wall)
			addToContend(cell.x,cell.y, mazeData, contend)
		end
	end
	return wallList
end

--[[
	Generates unprocessed walls
	size:	size of maze(square)
	blackWalls: areas not to Generate Maze
--]]
local genMaze = function(size, blackWalls)
	local at = 1
	local skip = false
	local mazeGen = {}
	for row = 0, size, 1 do
		for col = 0, size, 1 do
			if blackWalls ~= nil then for i, wall in pairs(blackWalls) do
				if  row >= wall.x1 and row <= wall.x2 
				and col >= wall.y1 and col <= wall.y2 then
					skip = true
				end
			end end
			if skip == false then
				if row ~= 0 then mazeGen[at] = {x=row, y=col, c=0} at = at + 1 end
				if col ~= 0 then mazeGen[at] = {x=row, y=col, c=1} at = at + 1 end
			end
			skip = false
		end
	end
	return mazeGen
end

--Maze Building

local mazesize = 25
local wallsize = 35
function mazeDraw(maze)
	if maze ~= nil then
		for i, wall in pairs(maze) do
			local rectdefs = {X=(wall.x * wallsize),Y=(wall.y * wallsize),W=wallsize,H=5}
			if wall.c == 0 then
				rectdefs.W = 5
				rectdefs.H = wallsize
			end
			prRect(fg, rectdefs.X, rectdefs.Y, rectdefs.W, rectdefs.H)
		end
	end
end

--All inclusive gobal meta-function for generating mazes
function Prism_Gen()
	local size = (mazesize+1) * wallsize
	x, y = prZone(size, size)
  --local maze = genMaze(50,nil)
	local maze = doDigging(genMaze(mazesize, nil))
	mazeDraw(maze)
end