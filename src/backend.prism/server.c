/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include "common.h"
#include "server.h"

#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <platypro.base/strings.h>
#include <platypro.base/list.h>

bool Player_Chat(PLAYERINDEX* index, char* pname, char* message)
{
  char buffer[MAX_MESSAGE_SIZE];
  
  if(pname)
    snprintf(buffer, MAX_CHAT_SIZE + strlen(pname), "<%s> %s", pname, message);
  else snprintf(buffer, MAX_CHAT_SIZE, "%s", message);
  
  Player_PushOnce(index, SIGNAL_CHAT, buffer);

  return true;
}

//Finds the lowest populated team
PRIVATE TEAM* findLowTeam(LIST teams)
{
  TEAM *team, *result = LIST_GETOBJ(teams);
  int clow = 0;
  foreach(team, teams)
  {
    if(team->id > 0)
    {
      if(team->childCount == 0)
        return team;
      if(clow < team->childCount)
      {
        clow = team->childCount;
        result = team;
      }
    }
  }
  return result;
}

//Moves a player to their team area
PRIVATE void moveToTeam(PLAYER* player)
{
  if(player->team->id != 0)
  {
    TEAM* t = player->team;
    //Move player to team base
    player->pos.x = t->base.x + PLAYER_RADIUS + (rand() % (t->base.w - (2*PLAYER_RADIUS)));
    player->pos.y = t->base.y + PLAYER_RADIUS + (rand() % (t->base.h - (2*PLAYER_RADIUS)));
  } 
  else
  {
    player->pos.x = PLAYERPOS_DEFAULT;
    player->pos.y = PLAYERPOS_DEFAULT;
  }
  return;
}

PRIVATE int gResetPlayer(CLIENT* client, void* user)
{
  SERVERPLAYER* p = MP_CLIENT(client);
  //Reset player ready status
  p->player.ready = false;
  
  p->spectate = false;
  
  //Reset player position
  moveToTeam(&p->player);
  
  p->PositionOverride = true;
  return true;
}

PUBLIC void Game_Reset(SERVERSTATE* game)
{
  //Reset timing
  game->gameTime = 0;
  game->playerReadyCount = 0;
  
  //Reset Scores and Flag Counts
  TEAM* t;
  foreach(t, game->levelData->team)
  {
    if(t->id != 0)
    {
      t->teamscore =0;
      //TODO: Make flagCount more dynamic
      t->flagCount = TEAM_FLAGCOUNT;
    }
  }
  
  Player_Do(game->players, gResetPlayer);
}

PUBLIC bool Game_AddPlayer(SERVERNODE* cnode, SERVERSTATE* state)
{
    char order; int pid;
    if(state->gs == GS_INGAME)
      order = ORDER_BEGINGAME;
    else if(state->gs == GS_WAITFORPLAYERS)
      order = ORDER_PLIST;
    else order = ORDER_NONE;
    
    SERVERPLAYER* p = (SERVERPLAYER*)Player_Add(state->players, cnode, order, &pid);
    if(p)
    {
      TEAM* pteam;
      if(state->gs == GS_WAITFORPLAYERS)
      {
        pteam = findLowTeam(state->levelData->team);
      }
      else
      {
        pteam = LIST_GETOBJ(state->levelData->team);
        if(state->gm != GAMEMODE_NONE)
          p->spectate = true;
      }
      
      strncpy(p->player.name, Strings_Get(state->strings, STR_USERNAME), PLAYER_NAMEMAX);
      
      p->PositionOverride = true;
      p->teamFlag = pteam;
      
      p->player.id = pid;
      
      //Add player to least populated team
      Team_Player_Add(pteam, &p->player);
      
      moveToTeam(&p->player);

      cnode = NULL;
      return true;
    }
    return false;
}

PRIVATE char genRect(char* string, int maxlen, IO_RECT* obj)
{
  snprintf(string, maxlen, FORMAT_RECT,
    obj->x, obj->y,
    obj->w, obj->h );

  return 0;
}

PUBLIC char Game_PutStatic(char* string, int maxlen, void* data, int objtype, void* user)
{
  char result = SIGNAL_NODATA;

  switch(objtype)
  {
  case HEADER_RECT:
    genRect(string, maxlen, (IO_RECT*)data);
    result = SIGNAL_RECT;
    break;
  case HEADER_ROUND:
    genRect(string, maxlen, (IO_RECT*)data);
    result = SIGNAL_ROUND;
    break;
  case HEADER_TEAM:
    if(((TEAM*)data)->id > 0)
    {
      genRect(string, maxlen, &((TEAM*)data)->base);
      result = SIGNAL_TEAMFLAG;
    }
    break;
  }
  return result;
}

PUBLIC void Game_End(SERVERSTATE* game)
{
    Player_Order(game->players, ORDER_ENDGAME);
    Game_Reset(game);
}

#define Game_genFlag(playerID, p) \
          playerID,\
          ((p->player.hasFlag != NULL)),\
          p->teamFlag->id,\
          p->teamFlag->flagCount

char Game_Put(struct PlayerIndex* cx, char* buffer, int type)
{
  SERVERSTATE* state = (SERVERSTATE*)MP_EXTRA(cx);
  SERVERPLAYER* me = (SERVERPLAYER*)MP_CLIENT(MP_THIS(cx));
  CLIENT* notme = MP_OTHER(cx);
  
  //Player has left, do cleanup message
  if(!buffer || !notme)
  {
    char buffer[MAX_MESSAGE_SIZE];
    if(me->player.ready) state->playerReadyCount--;
    
    snprintf(buffer, 100, "%s %s", me->player.name, Strings_Get(state->strings, STR_CHAT_PLEAVE));
    Player_Chat(state->players, SERVER_PLAYERNAME, buffer);
    
    Log_Throw(state->log, ERRSEV_INFO, buffer);
    
    if(me->player.team) 
    {
      //Test if game has ended, and if a dead team order is needed
      if(state->gs == GS_INGAME 
        && me->player.team->childCount == 1 
        && state->gm != GAMEMODE_NONE
        && me->player.team->id != 0)
      {
        //End the game
        Player_Order(state->players, ORDER_DEADTEAM);
        Game_Reset(state);
        if(state->levelData->teamCount > 0 && state->gm != GAMEMODE_NONE)
          state->gs = GS_WAITFORPLAYERS;
      }
      Team_Player_Remove(&me->player);
      Player_SendAll(state->players, true);
    }
    
    if(me->player.hasFlag)
    {
      me->teamFlag->flagCount++;
      me->player.hasFlag = false;
      Player_PushOnce(state->players, SIGNAL_FLAG, FORMAT_FLAG, Game_genFlag(MP_ID(MP_THIS(cx)), me)); 
    }
    
    return 0;
  }
    
  if(me)
  {
	SERVERPLAYER* other = MP_CLIENT(notme);
	int playerID = MP_ID(notme);
    switch(type)
    {
      case TYPE_PLAYER:
        if((me != other || me->PositionOverride) && !me->spectate)
        {
          if(state->gs == GS_INGAME)
          {
              snprintf(buffer, MAX_MESSAGE_SIZE, FORMAT_PLAYER,
                playerID,
                other->player.velocity.x,
                other->player.velocity.y,
                other->player.pos.x,
                other->player.pos.y);
              
              if(me == other) me->PositionOverride = false;
              
              return SIGNAL_PLAYER;
          }
          else if(state->gs == GS_WAITFORPLAYERS)
          {
            snprintf(buffer, MAX_MESSAGE_SIZE, FORMAT_INT,
              playerID,
              other->player.ready
            );
            
            return SIGNAL_PREADY;
          }
        }
        break;
      case TYPE_PNAME:
        snprintf(buffer, MAX_MESSAGE_SIZE, FORMAT_PNAMEPRINT,
          playerID,
          other->player.name);
        return SIGNAL_PNAME;
        break;
      case TYPE_TEAM:
        snprintf(buffer, MAX_MESSAGE_SIZE, FORMAT_INT,
            playerID,
            other->player.team->id);
        return SIGNAL_TEAM;
        break;
      case TYPE_SCORE:
        snprintf(buffer, MAX_MESSAGE_SIZE, FORMAT_SCORE,
          playerID,
          other->player.score,
          other->player.team->teamscore
        );
        return SIGNAL_SCORE;
        break;
      case TYPE_FLAG:
        snprintf(buffer, MAX_MESSAGE_SIZE, FORMAT_FLAG,
          Game_genFlag(playerID, other)
        );
        return SIGNAL_FLAG;
        break;
      case TYPE_STAMINA:
        snprintf(buffer, MAX_MESSAGE_SIZE, FORMAT_INT,
          playerID,
          other->player.stamina
        );
        return SIGNAL_STAMINA;
        break;
    }
  }
  return SIGNAL_NODATA;
}

//Tests for invalid movement signals
PRIVATE bool testMovementSignal(PLAYER pold, PLAYER pnew)
{
  int cfudge = CHEAT_FUDGE;
  if( (abs(pnew.velocity.x) != MOVEMENT_SPEED && abs(pnew.velocity.x) != RUN_SPEED && pnew.velocity.x != 0)
  ||  (abs(pnew.velocity.y) != MOVEMENT_SPEED && abs(pnew.velocity.y) != RUN_SPEED && pnew.velocity.y != 0) )
    return false;

  if(abs(pnew.velocity.x) == RUN_SPEED || abs(pnew.velocity.y) == RUN_SPEED)
  {
    cfudge *= 2;
    if(pold.stamina == 0)
      return false;
  }
    
  if(pnew.pos.x > pold.pos.x + (cfudge*MOVEMENT_SPEED)
  || pnew.pos.x < pold.pos.x - (cfudge*MOVEMENT_SPEED)
  || pnew.pos.y > pold.pos.y + (cfudge*MOVEMENT_SPEED)
  || pnew.pos.y < pold.pos.y - (cfudge*MOVEMENT_SPEED))
    return false;
    
  return true;
}

#define INCSCORE(p, s) (p)->score += s; (p)->team->teamscore += s; scoredirty = true;

PUBLIC int Game_Update(PLAYERINDEX* index, int tick)
{
  SERVERPLAYER* p = (SERVERPLAYER*)MP_CLIENT(MP_THIS(index));
  SERVERSTATE* state = (SERVERSTATE*)MP_EXTRA(index);
  bool scoredirty = false; //Dirty flag for score
  
  p->recvd = false;

  if(p->spectate)
  {
    p->player.pos.x += p->player.velocity.x;
    p->player.pos.y += p->player.velocity.y;
  }
  else
  {
    updateVelocity(&p->player, tick, state->collisionData);
  
    IO_RECT pRect = getPlayerArea(p->player.pos);
    
    //Handle flags
    if(p->player.hasFlag)
    {
      TEAM* t;
      //Check if flag needs to be transferred
      foreach(t, state->levelData->team)
      {
        if(t->firstChild && t != p->player.team)
        {
          PLAYER* collision = t->firstChild;
          while(collision)
          {
            IO_RECT r = getPlayerArea(collision->pos);      
            if(collision->hasFlag == NULL)
            {
              int coll = testRectCollision(&pRect, &r);
              
              if(p->player.hasFlag == collision && 
                !coll)
              {
                p->player.hasFlag = &p->player;
              }
              
              if
              (
                p->player.hasFlag  != collision &&
                coll
              )
              {              
                if(testRectCollision(&r, &collision->team->base))
                {
                  INCSCORE(collision, SCORE_FLAG)
                  p->player.hasFlag = NULL;
                  p->teamCollision = collision->team;
                  collision->team->flagCount ++;
                  
                  Player_Distribute(index, TYPE_FLAG);
                }
                else 
                {
                  p->player.hasFlag = NULL;
                  collision->hasFlag = &p->player;
                  INCSCORE(collision, SCORE_STEAL)
                  
                  Player_PushOnce(
                    state->players, 
                    SIGNAL_TRANSFER, 
                    FORMAT_TRANSFER, 
                    p->player.id,
                    collision->id);
                }
              }
            }
            if(scoredirty)
            {
              Player_PushOnce(
                state->players, 
                SIGNAL_SCORE, 
                FORMAT_SCORE,
                collision->id,
                collision->score,
                collision->team->teamscore); 
            }
            
            collision = collision->nextTeam;
          }
        }
      }
      
      //Check if flag needs to be dropped
      if(testRectCollision(&pRect, &p->player.team->base))
      {
        p->teamFlag = p->player.team;
        p->player.hasFlag = false;
        p->player.team->flagCount++;
        INCSCORE(&p->player, SCORE_FLAG)
        
        //Test for control winstate
        if(state->gm == GAMEMODE_CONTROL 
          && p->player.team->flagCount >= (TEAM_FLAGCOUNT * state->levelData->teamCount))
        {
          Game_End(state); 
        }
        
        Player_Distribute(index, TYPE_SCORE);
        Player_Distribute(index, TYPE_FLAG);
      }
      
      //Test for threshold winstate
      if(state->gm == GAMEMODE_THRESHOLD && p->player.score >= state->gameGoal)
      {
        Game_End(state);
      }
    }
    else
    {
      //Check collisions for picking up flag
      TEAM* t;
      foreach(t, state->levelData->team)
      {
        if(testRectCollision(&pRect, &t->base))
        {
          if(t != p->player.team && t != p->teamCollision && t->flagCount > 0)
          {
            p->player.hasFlag = &p->player;
            p->teamFlag = t;
            t->flagCount --;
            
            Player_Distribute(index, TYPE_FLAG);
          }
        }
        else
        {
          if(t == p->teamCollision) p->teamCollision = NULL; 
        }
          
      }
    }
  }

  return true; 
}

//Makes sure game is ready to start
PRIVATE char checkTeams(SERVERSTATE* state)
{
  TEAM* team;
  foreach(team, state->levelData->team)
  {
   if(team->id != 0 && team->childCount == 0)
     return false;
  }
  
  //Make sure everybody is ready
  return state->playerReadyCount >= PLAYER_GETCOUNT(state->players);
}

PUBLIC int Game_Get(PLAYERINDEX* context, char header, char* request)
{
  //SERVERPLAYER* client = (SERVERPLAYER*)data;
  SERVERPLAYER* client = (SERVERPLAYER*)MP_CLIENT(MP_THIS(context));
  SERVERSTATE* state = (SERVERSTATE*)MP_EXTRA(context);
  
  PLAYER newP = client->player;
  int pid, team;
  TEAM* t;

  switch(header)
  {
  case SIGNAL_PLAYER:
    if(!client->recvd && !client->spectate)
    {
      sscanf(request, FORMAT_PLAYER,
        &pid,
        &newP.velocity.x,
        &newP.velocity.y,
        &newP.pos.x,
        &newP.pos.y);
      
      if(testMovementSignal(client->player, newP))
      {
        client->player = newP;
      }
      else
      {
        Player_Distribute(context, TYPE_STAMINA);
        client->player.velocity.x = 0;
        client->player.velocity.y = 0;
        client->PositionOverride = true;
      }
      
      Player_Distribute(context, TYPE_PLAYER);
      
      client->recvd = true;
    }

    break;
  case SIGNAL_PREADY:
    Player_Distribute(context, TYPE_PLAYER);
    bool newReady;
    sscanf(request, FORMAT_INT,
            &pid,
            &newReady
          );
    
    if (newReady && !client->player.ready) state->playerReadyCount++;
    if (!newReady && client->player.ready) state->playerReadyCount--;
    
    if(checkTeams(state))
    {
      Player_Order(state->players, ORDER_BEGINGAME);
      Player_SendAll(state->players, true);
      state->gs = GS_INGAME;
      Game_Reset(state);
    }
    
    client->player.ready = newReady;
    break;
  case SIGNAL_PNAME:
    Player_Distribute(context, TYPE_PNAME);
    sscanf(request, FORMAT_PNAME,
           &pid,
            client->player.name);
    
    if(pid == 0)
    {
      char joinMesg[MAX_MESSAGE_SIZE];
      snprintf(joinMesg, 100, "%s %s", client->player.name, Strings_Get(state->strings, STR_CHAT_PJOIN));
      Player_Chat(state->players, SERVER_PLAYERNAME, joinMesg);
    }
    
    break;
  case SIGNAL_CHAT:
    //Trim chat string
    request[strlen(request) - 1] = '\000';
    Player_Chat(state->players, client->player.name, request);
    break;
  case SIGNAL_TEAM:    
    sscanf(request, FORMAT_INT,
        &pid,
        &team);
    
    //Find New Team
    foreach(t, state->levelData->team)
    {
      if(t->id == team)
      {
        //Swap team
        Team_Player_Remove(&client->player);
        Team_Player_Add(t, &client->player);
        
        //Turn off player ready status
        if(client->player.ready)
        {
          client->player.ready = false;
          Player_Distribute(context, TYPE_PLAYER);
          state->playerReadyCount --;
        }
        
        moveToTeam(&client->player);
        
        //Inform that no error has occured
        Player_Distribute(context, TYPE_TEAM);
        break;
      }
    }
    
    break;
  }
  
  return true;
}
