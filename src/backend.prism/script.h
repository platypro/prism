/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/** \file List file Header
 *  \addtogroup RoomLoader
 */
/** @{ */
#ifndef H_PRISM_SCRIPT
#define H_PRISM_SCRIPT

//Lua library (for room loading)
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#include "platypro.base/list.h"
#include "platypro.game/common.h"

//! The script state as returned from Script_Init
typedef lua_State SCRIPTSTATE;

//! Level data returned from Script_GenRoom
typedef struct LevelData
{
	LIST fg; //!< Foreground room data
	LIST bg; //!< Background room data
	
	LIST team; //!< Team data
	
	int teamCount; //!< Total number of teams

	int zonew; //!< Room width
	int zoneh; //!< Room Height
} LEVELDATA;

#define KEY_OBJS "prism_RoomData"

#define KEY_FG  "fg"
#define KEY_BG  "bg"

#define ROOM_DEFAULT_WIDTH 640
#define ROOM_DEFAULT_HEIGHT 480

//Script function "building blocks"
#define FUNC_PREFIX  "pr"
#define UFUNC_PREFIX "Prism_"
//Script function names
#define FUNC_GEN   UFUNC_PREFIX "Gen"
#define FUNC_RECT   FUNC_PREFIX "Rect"
#define FUNC_FLAG   FUNC_PREFIX "Flag"
#define FUNC_ROUND  FUNC_PREFIX "Round"
#define FUNC_ZONE   FUNC_PREFIX "Zone"

//! Open a file for script writing
extern SCRIPTSTATE* Script_Init(char* file);
//! Cleanup a loaded script
extern void         Script_Close(SCRIPTSTATE* state);

//! Generate room from script file
extern LEVELDATA*   Script_GenRoom(SCRIPTSTATE* state);
//! Cleanup loaded level data
extern void         Script_CleanData(LEVELDATA** ld);

#endif /*INC_ROOMLOADER_H*/
/** @} */
