/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include "common.h"
#include "script.h"

#include <stdio.h>
#include <stdlib.h>

#include "platypro.base/list.h"

//Gets room data from a lua state
PRIVATE LEVELDATA* Get_RoomData(lua_State* state)
{
    LEVELDATA* result;
    //Ask lua to query our object
    lua_pushstring(state, KEY_OBJS);
    lua_gettable(state, LUA_REGISTRYINDEX);
    //Retrieve our object
    result = (LEVELDATA*)lua_topointer(state, -1);
    return result;
}

//Handles general argument options for draw functions
IO_RECT* makeDraw(lua_State* state, LIST_HEADER otype)
{
    LEVELDATA* room = Get_RoomData(state);
    //Get the rect type (FG or BG)
    int rectType = luaL_checknumber(state, 1);

    //Find the list to add the rect object to
    LIST* listAdd;
    if(rectType == HEADER_FG)
    {
      listAdd = &room->fg;
    }
    else
    {
      listAdd = &room->bg;
    }

    //Finally, add the new rect object
    IO_RECT* obj = List_Add_Alloc(listAdd,
            sizeof(IO_RECT), otype);

    if(obj)
    {
		obj->x = luaL_checknumber(state, 2);
		obj->y = luaL_checknumber(state, 3);
		obj->w = luaL_checknumber(state, 4);
		obj->h = luaL_checknumber(state, 5);
		return obj;
    }

    return NULL;
}

int pr_Rect(lua_State* state)
{
	makeDraw(state, HEADER_RECT);
    return 0;
}

int pr_Round(lua_State* state)
{
	makeDraw(state, HEADER_ROUND);
	return 0;
}

bool zoneSetNum(lua_State* s, int arg, int* i)
{
  int x = lua_tonumber(s, arg);
  if(x != -1)
  {
    *i = x;
    return true;
  }
  return false;
}

int pr_Zone(lua_State* state)
{
  LEVELDATA* room = Get_RoomData(state);

  //Set Zone (if specified)
  if(lua_gettop(state) == 3)
  {
    zoneSetNum(state, 1, &room->zonew);
    zoneSetNum(state, 2, &room->zoneh);
  }

  //Push most up to date zone
  lua_pushnumber(state, room->zonew);
  lua_pushnumber(state, room->zoneh);

  return 2;
}

int pr_Flag(lua_State* state)
{
  LEVELDATA* room = Get_RoomData(state);
  
  //Default return value
  lua_pushnil(state);

  if(lua_gettop(state) == 6)
  {
    //Create a team
    TEAM* team = (TEAM*)List_Add_Alloc(&room->team, sizeof(TEAM), HEADER_TEAM); 
    if(team)
    {
      team->base.x = luaL_checknumber(state, 1);
      team->base.y = luaL_checknumber(state, 2);
      team->base.w = luaL_checknumber(state, 3);
      team->base.h = luaL_checknumber(state, 4);
      
      room->teamCount++;
      
      team->id = room->teamCount;
      
      //New return value
      lua_pop(state, -1);
      lua_pushnumber(state, room->teamCount);
    }
  }
  
  return 1;
}

#define regf(alias, func) lua_register(result, (alias), (func));

lua_State* Script_Init(char* script)
{
    lua_State* result = luaL_newstate();
    if(result)
    {
        if(luaL_dofile(result, script) == 0)
        {
            luaL_openlibs(result);
            //Register functions for use in scripts
            regf(FUNC_RECT  , pr_Rect);
            regf(FUNC_ROUND , pr_Round);
            regf(FUNC_ZONE  , pr_Zone);
            regf(FUNC_FLAG  , pr_Flag);
        } 
        else
        {
            printf("Lua:%s\n", lua_tostring(result, -1));
            lua_close(result);
            result = NULL;
        }
    }
    return result;
}

//Initializes the room from a script
LEVELDATA* Script_GenRoom(lua_State* ls)
{
    //The registry of all objects spawned by the script
    LEVELDATA* lData = calloc(1, sizeof(LEVELDATA));

    if (ls)
    {
      lData->zoneh = ROOM_DEFAULT_HEIGHT;
      lData->zonew = ROOM_DEFAULT_WIDTH;
      
      //Create default team
      List_Add_Alloc(&lData->team, sizeof(TEAM), HEADER_TEAM); 

      //Set up the object registry in the lua state
      lua_pushstring(ls, KEY_OBJS);
      lua_pushlightuserdata(ls, lData);
      lua_settable(ls, LUA_REGISTRYINDEX);

      //Set up constants
      lua_pushnumber(ls, HEADER_FG);
      lua_setglobal(ls, KEY_FG);

      lua_pushnumber(ls, HEADER_BG);
      lua_setglobal(ls, KEY_BG);

      //Call the generate function in the script
      lua_getglobal(ls, FUNC_GEN);
      if(lua_pcall(ls, 0, 1, 0) != 0)
      {
        printf("Script Failed: %s\n", lua_tostring(ls, -1));
          Script_CleanData(&lData);
      }
    }
    return lData;
}

void Script_Close(SCRIPTSTATE* state)
{
    if(state)
    {
        lua_close(state);
    }
}

void Script_CleanData(LEVELDATA** ld)
{
    List_Purge(&(*ld)->bg, NULL);
    List_Purge(&(*ld)->fg, NULL);
    List_Purge(&(*ld)->team, NULL);
    free(*ld);
    *ld = NULL;
}
