/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef H_PRISM_SERVER
#define H_PRISM_SERVER

#define GAME_TYPETOP 7

#define TYPE_PLAYER  0 //Player vector data (or ready)
#define TYPE_PNAME   1 //Player Name Signal
#define TYPE_TEAM    2 //Team Signal
#define TYPE_SCORE   3 //Score Signal
#define TYPE_FLAG    4 //Flag Signal
#define TYPE_STAMINA 5 //Stamina Signal

#define TEAM_FLAGCOUNT 3

#define SERVER_PLAYERNAME "Server"

#define CHEAT_FUDGE 3

#define PLAYERPOS_DEFAULT 15

#include "platypro.base/common.h"
#include "platypro.base/net.h"
#include "platypro.game/multiplayer.h"
#include "script.h"
#include "collision.h"

// Everything stored in server state,
// this stuff is usually set only once
typedef struct ServerState
{
  //! Static level data to be sent to all players
  LEVELDATA* levelData;

  //! Collision Data for the level (saves per-move time)
  COLLISIONLIST* collisionData;

  //! Index of strings, as loaded at the beginning of the program
  LIST strings;

  //! Manager in charge of logging, and display of errors
  LOGMANAGER* log;
  
  //! ServerAPI object
  SERVERNODE* server;

  //! The loaded script data
  SCRIPTSTATE* script;

  //! Player index
  PLAYERINDEX* players;
  
  ///////////////////////////////////////////////////////
  
  //! Total count of players ready
  int playerReadyCount;
  
  //! Tick counter for determining seconds
  int secondTick; 

  //! Current game status
  GAMESTAGE gs;
  
  //! Current game mode (win state)
  GAMEMODE gm;
  
  //! Goal for current gamemode
  int gameGoal;
  
  //! time since game started
  int gameTime;

} SERVERSTATE;

typedef struct ServerPlayer
{
  bool PositionOverride;
  bool spectate; //!< Buffer value for whether player is spectating
  bool recvd; //! Whether data has been recieved on this tick
  TEAM* teamCollision; //! Whether player is on a team's base
  TEAM* teamFlag; //! Flag has had their flag stolen by this player. Everyone needs to know
  PLAYER player;
} SERVERPLAYER;

extern bool Game_AddPlayer(SERVERNODE* cnode, SERVERSTATE* state);
extern void Game_Reset(SERVERSTATE* game);
extern void Game_End(SERVERSTATE* game);

//Items to interface with Player Module
extern int  Game_Get(struct PlayerIndex* context, char header, char* request);
extern char Game_Put(struct PlayerIndex* context, char* buffer, int type);
extern int Game_Update(struct PlayerIndex* context, int tick);

extern char Game_PutStatic(char* string, int maxlen, void* data, int objtype, void* user);

#endif