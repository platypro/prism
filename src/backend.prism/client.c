/*
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "common.h"
#include "client.h"

#include <string.h>
#include <stdlib.h>

#include "platypro.game/multiplayer.h"

PUBLIC CLIENTPLAYER* Server_GetActivePlayer(SERVERCONNECTION* server)
{
	if(server->playerID && !server->activePlayer)
	{
		server->activePlayer = Server_FindPlayer(server, server->playerID);
	}

	return server->activePlayer;
}

PUBLIC CLIENTPLAYER* Server_FindPlayer(SERVERCONNECTION* connection, int id)
{
  //Find Player
  CLIENTPLAYER* player = NULL;
  
  foreach(player, connection->playerStore)
  {
    if(player->player.id == id)
      break;
  }
  
  return player;
}

PUBLIC TEAM* Server_FindTeam(SERVERCONNECTION* connection, int id)
{
  TEAM* team = NULL; 
  
  foreach(team, connection->teams)
  {
    if(team->id == id)
      break;
  }
  
  return team;
}

void Client_RequestDisconnect(SERVERCONNECTION** connection)
{
  Server_PutMesg(&(*connection)->node, SIGNAL_KILLREQUEST, "");
  (*connection)->isWaitingForClose = true;
  return;
}

bool Client_Connection_Destroy(SERVERCONNECTION** conn)
{
  //Destroy the server
  ClearCollisions(&(*conn)->bgCollisions);
  ClearCollisions(&(*conn)->fgCollisions);

  List_Purge(&(*conn)->fg, NULL);
  List_Purge(&(*conn)->bg, NULL);
  
  List_Purge(&(*conn)->teams, NULL);

  List_Purge(&(*conn)->playerStore, NULL);
  
  //Close the server
  Server_Close(&(*conn)->node);
  Log_ThrowFormat((*conn)->log, ERRSEV_INFO, STR_MENU_DISCONNECT, NULL);
  
  free(*conn);
  *conn = NULL;
  return true;
}

PRIVATE CLIENTPLAYER* pUpdate(SERVERCONNECTION* connection, int id, IO_POINT* pos)
{
  CLIENTPLAYER* player  = Server_FindPlayer(connection, id);
  //Add Player if not found
  if(!player)
  {
    player = List_Add_Alloc(&connection->playerStore, sizeof(CLIENTPLAYER), HEADER_PLAYER);
    
    player->player.id = id;
    //Add to default team
    Team_Player_Add(LIST_GETOBJ(connection->teams), &player->player);
  }
  
  if(pos)
  {
    if((player->player.pos.x == 0 && player->player.pos.y == 0)
    || connection->playerID == id)
      player->player.pos = *pos;
    player->intent = *pos;
  }

  if(!connection->activePlayer && connection->playerID == id)
  {
    connection->activePlayer = player;
  }
  
  return player;
}

int teamCompar (void* team1, void* team2)
{
  return ((TEAM*)team1)->teamscore - ((TEAM*)team2)->teamscore;
}

PUBLIC bool Server_HandlePlayers(char header, char* request, SERVERCONNECTION* connection)
{
  bool result = false;
  CLIENTPLAYER* player;

  if(header == SIGNAL_PLAYER)
  {
    PLAYER pbuff;
    sscanf
    (
      request, FORMAT_PLAYER,
        &pbuff.id,
        &pbuff.velocity.x,
        &pbuff.velocity.y,
        &pbuff.pos.x,
        &pbuff.pos.y
    );
    
    //Process Request
    CLIENTPLAYER* p = pUpdate(connection, pbuff.id, &pbuff.pos);
    p->player.velocity = pbuff.velocity;
  }
  else if(header == SIGNAL_PREADY)
  {
    PLAYER pbuff;
    sscanf
    (
      request, FORMAT_INT,
        &pbuff.id,
        &pbuff.ready
    );
    CLIENTPLAYER* p = pUpdate(connection, pbuff.id, NULL);
    p->player.ready = pbuff.ready;
  }
  else if(header == SIGNAL_PLAYERDEAD)
  {
    int pid = -1;
    sscanf(request, "%d", &pid);

    player = Server_FindPlayer(connection, pid);
    if(player)
    {
      Team_Player_Remove(&player->player);
      List_Remove(&connection->playerStore, player);
    }
  } 
  else if(header == SIGNAL_PNAME)
  {
    int pid = -1;
    char name[PLAYER_NAMEMAX];
    sscanf(request, FORMAT_PNAME, &pid, name);
    
    CLIENTPLAYER* p = pUpdate(connection, pid, NULL);
    
    if(p)
    {
      strcpy(p->player.name, name); 
      if(pid == connection->playerID) connection->hasName = true;
    } 
  } 
  else if(header == SIGNAL_TEAM)
  {
    int teamid, pid = -1;
    TEAM* team;
    
    sscanf(request, FORMAT_INT, &pid, &teamid);
    CLIENTPLAYER* p = pUpdate(connection, pid, NULL);

    team = Server_FindTeam(connection, teamid);
    if(p && team)
    {
      Team_Player_Remove(&p->player);
      Team_Player_Add(team, &p->player);
    }
  }
  else if(header == SIGNAL_SCORE)
  {
    int score, teamscore, pid = -1;
    sscanf(request, FORMAT_SCORE, &pid, &score, &teamscore);
    
    CLIENTPLAYER* p = pUpdate(connection, pid, NULL);
    if(p)
    {
      p->player.team->teamscore = teamscore;
      p->player.score = score;
      
      //Sort team score
      List_Sort(&connection->teams, teamCompar); 
      
      //Sort player score
      while(p->player.prevTeam && p->player.score > p->player.prevTeam->score)
      {
        PLAYER* base = &p->player;
        PLAYER* swap = p->player.prevTeam;
        if(base->nextTeam)
          base->nextTeam->prevTeam = swap;
        
        if(swap->prevTeam)
          swap->prevTeam->nextTeam = base;
        else
          base->team->firstChild = swap;
        
        swap->nextTeam = base->nextTeam;
        base->prevTeam = swap->prevTeam;
        
        base->nextTeam = swap;
        swap->prevTeam = base;
      }
      
      if(!p->player.prevTeam)
        p->player.team->firstChild = &p->player;
    }
  }
  else if(header == SIGNAL_FLAG)
  {
    int pflag, tid, tflag, pid = -1;
    
    sscanf(request, FORMAT_FLAG, &pid, &pflag, &tid, &tflag);
    
    CLIENTPLAYER* p = Server_FindPlayer(connection, pid);
    TEAM* team = Server_FindTeam(connection, tid);
    
    if(p) p->player.hasFlag = pflag ? &p->player : NULL;
    if(team) team->flagCount = tflag;
  } 
  else if(header == SIGNAL_STAMINA)
  {
    int pid, stamina;
    
    sscanf(request, FORMAT_INT, &pid, &stamina);
    
    CLIENTPLAYER* p = pUpdate(connection, pid, NULL);
    if(p)
    {
      p->player.stamina = stamina;
    }
  }
  else if(header == SIGNAL_TRANSFER)
  {
    int pid1, pid2;
    
    sscanf(request, FORMAT_TRANSFER,
           &pid1, &pid2);
    
    CLIENTPLAYER* p1 = Server_FindPlayer(connection, pid1);
    CLIENTPLAYER* p2 = Server_FindPlayer(connection, pid2);
    
    if(p1 && p2)
    {
      //Do Transfer
      p2->player.hasFlag = &p1->player;
      p1->player.hasFlag = NULL;
    }
  }
  return result;
}

PRIVATE bool chatalloc(SERVERCONNECTION* conn, char* request)
{
  int len = strlen(request);
  char* at = request + len - 1;
  
  //Test for string can fit.
  if(conn->chatAt - len - 1 < 0)
  {
    //Find length of last string
    int newchat = CHAT_CACHE_SIZE - len + conn->chatAt;
    while(*(conn->chatHistory + newchat))
      newchat--;
    
    //Shift old strings over
    memmove(conn->chatHistory + CHAT_CACHE_SIZE - newchat, conn->chatHistory, newchat);
    conn->chatAt += CHAT_CACHE_SIZE - newchat;
  }

  memset(conn->chatHistory, 0, conn->chatAt);
  
  while(at >= request)
  {
    //Copy the character
    *(conn->chatHistory + conn->chatAt) = *at;
    //Decrement character
    at--;
    //Decrement chat pointer
    conn->chatAt--;
  }
  
  *(conn->chatHistory + conn->chatAt) = 0;
  conn->chatAt--;
  
  return true;
}

PRIVATE bool Server_HandleChat(SERVERCONNECTION* conn, char header, char* request)
{
  if(header == SIGNAL_CHAT)
  {
    //Trim chat string
    request[strlen(request) - 1] = '\000';
    chatalloc(conn, request);
    return true;
  }
  return false;
}

PRIVATE int Server_HandleOrders(SERVERCONNECTION** conn, char header, char* request)
{
  int result = 0;
  CLIENTPLAYER* p;
  
  #define REQERR(order, string) \
   if(*request == (order))\
    Log_ThrowFormat(\
      (*conn)->log,\
      ERRSEV_USERWARN,\
      (string), NULL);
  
  REQERR(ORDER_SERVERCLOSE, ERRID_SERVERCLOSE);
  REQERR(ORDER_SERVERFULL,  ERRID_SERVERFULL);
  
  if(!(*conn)->isWaitingForClose)
  {
    REQERR(ORDER_KICK,        ERRID_KICK);
  }
  
  if(header == SIGNAL_ORDER)
  {
    switch(*request)
    {
      case ORDER_SERVERCLOSE: 
      case ORDER_SERVERFULL:
      case ORDER_KICK:
        result = -1;
        Client_Connection_Destroy(conn);
        break;
      case ORDER_BEGINGAME:
        (*conn)->gameStage = GS_INGAME;
        //Reset ready states
        foreach(p, (*conn)->playerStore)
        {
          p->player.ready = false; 
        }
        break;
      case ORDER_PLIST:
        (*conn)->gameStage = GS_WAITFORPLAYERS;
        break;
      case ORDER_DEADTEAM:
        if((*conn)->gameStage == GS_INGAME)
        {
          Log_ThrowFormat((*conn)->log, ERRSEV_USERWARN, STR_TEAM_DEAD, "");
          (*conn)->gameStage = GS_INTERGAME;
        }
        break;
      case ORDER_ENDGAME:
        (*conn)->gameStage = GS_INTERGAME;
        break;
    }
  }
  
  return result;
}

int Server_HandleGet(char header, char* request, int listID, void* data)
{
    int result = 0;
    SERVERCONNECTION** c = (SERVERCONNECTION**)data;   
    result = Server_HandleOrders(c, header, request);
    
    if(!result)
    {
      Server_HandleChat(*c, header, request);
      //Seperate list population tasks
      Server_HandlePlayers(header, request, *c);
    }

    return result;
}

PUBLIC SERVERCONNECTION* Client_Connection_Create(LOGMANAGER* log, char* addr, char* port)
{
  SERVERCONNECTION* result = calloc(1, sizeof(SERVERCONNECTION));

  //Create server state
  result->node = Server_Connect(log, addr, port);
  result->log = log;
  result->chatAt = CHAT_CACHE_SIZE;

  if(!result->node) {free(result); result = NULL; }

  return result;
}

PUBLIC bool Server_Update(SERVERCONNECTION** server)
{
  if(server && *server && (*server)->node)
  {
    if(Server_Get((*server)->node, Server_HandleGet, (void*)server) == MSG_FAIL)
    {
      return false;
    }

    if(*server)
    {
      static char sendBuffer[MAX_MESSAGE_SIZE];
      
      //Player Update
      if((*server)->update_player == true)
      {
        if(Server_CheckBusy((*server)->node))
        {
          PLAYER p = (*server)->activePlayer->player;
          //Velocity changed, tell the (*server)
          snprintf
          (
            sendBuffer,
              MAX_MESSAGE_SIZE, FORMAT_PLAYER,
              (*server)->playerID,
              p.velocity.x,
              p.velocity.y,
              p.pos.x,
              p.pos.y
          );

          Server_PutMesg(&(*server)->node, SIGNAL_PLAYER, sendBuffer);
          (*server)->oldPlayer = p;
          (*server)->update_player = false;
        }
      }

      if((*server)->update_ready)
      {
        if(Server_CheckBusy((*server)->node))
        {
          PLAYER p = (*server)->activePlayer->player;
          snprintf
          (
              sendBuffer,
                MAX_MESSAGE_SIZE, FORMAT_INT,
                p.id,
                p.ready
          );
          Server_PutMesg(&(*server)->node, SIGNAL_PREADY, sendBuffer);
          (*server)->update_ready = false;
        }
      }
      
      if((*server)->update_pname)
      {
        if(Server_CheckBusy((*server)->node))
        {
          CLIENTPLAYER* p = (*server)->activePlayer;
          snprintf
          (
              sendBuffer,
                MAX_MESSAGE_SIZE, FORMAT_PNAMEPRINT,
                p->player.id,
                p->player.name
          );

          Server_PutMesg(&(*server)->node, SIGNAL_PNAME, sendBuffer);
          (*server)->update_pname = false;
        }
      }
      
      if((*server)->update_team)
      {
        if(Server_CheckBusy((*server)->node))
        {
          CLIENTPLAYER* p = (*server)->activePlayer;
          snprintf(
            sendBuffer, 
            MAX_MESSAGE_SIZE, FORMAT_INT, 
            p->player.id,
            p->player.team->id);
          
          Server_PutMesg(&(*server)->node, SIGNAL_TEAM, sendBuffer);
          (*server)->update_team = false;
        }
      }
    }
    return true;
  } else return false;
}

int Client_Tick(SERVERCONNECTION** conn)
{
  //Test if game is executing a tick
  int tick = Tick_Get(&(*conn)->tickStatus);
  (*conn)->secondTick += tick;

  if((*conn)->secondTick >= TICKS_PERSEC)
  {
    (*conn)->secondTick -= TICKS_PERSEC;
    (*conn)->gametime   ++;
    //Do Heartbeat every five seconds
    if((*conn)->gametime % 5 == 0)
    {
      if(!(*conn)->node)
      {
        Client_Connection_Destroy(conn);
        tick = 0;
      } 
      else 
      {
        Server_Heartbeat(&(*conn)->node);
      }
    }
  }

  return tick;
}

PUBLIC bool Client_UpdateVelocities(SERVERCONNECTION* connection, int amount, IO_POINT intentVelocity)
{
  CLIENTPLAYER* player;
  if(connection->playerStore)
  {
    if(connection->activePlayer)
    {
      PLAYER* p = &connection->activePlayer->player;
      
      if(p->team->id == 0 && connection->gamemode != GAMEMODE_NONE)
      {
        p->pos.x += intentVelocity.x; 
        p->pos.y += intentVelocity.y;
      }
      else
      {
        IO_POINT oldV = p->velocity;
        p->velocity = intentVelocity;
        updateVelocity(p, amount, connection->fgCollisions);
        
        connection->update_player = (oldV.x != p->velocity.x || oldV.y != p->velocity.y);   
      }
    }

    foreach(player, connection->playerStore)
    {
      if(player != connection->activePlayer)
      {
		//Interpolate intent
		int xSpeed = player->player.velocity.x * amount;
		int ySpeed = player->player.velocity.y * amount;
		player->intent.x += xSpeed;
		player->intent.y += ySpeed;
    
    if(!xSpeed) xSpeed = MOVEMENT_SPEED;
    if(!ySpeed) ySpeed = MOVEMENT_SPEED;
    //This can probably be simpler
    
    if(player->player.pos.x < player->intent.x)
    {
      player->player.pos.x += abs(xSpeed);
      if(player->player.pos.x > player->intent.x) 
        player->player.pos.x = player->intent.x;
    }
      
    if(player->player.pos.x > player->intent.x)
    {
      player->player.pos.x -= abs(xSpeed);
      if(player->player.pos.x < player->intent.x) 
        player->player.pos.x = player->intent.x;
    }
        
    if(player->player.pos.y < player->intent.y)
    {
      player->player.pos.y += abs(ySpeed);
      if(player->player.pos.y > player->intent.y) 
        player->player.pos.y = player->intent.y;
    }
      
    if(player->player.pos.y > player->intent.y)
    {
      player->player.pos.y -= abs(ySpeed);
      if(player->player.pos.y < player->intent.y)
        player->player.pos.y = player->intent.y;
    }
		
        updateStamina(&player->player, amount);
      }
    }
  }
  return true;
}

ROOMDOWNLOAD* Client_InitDownload(SERVERCONNECTION* connection)
{
  ROOMDOWNLOAD* result = calloc(1, sizeof(ROOMDOWNLOAD));
  if(result)
  {
    Tick_Get(&connection->tickStatus);
    result->connection = connection;
    //Add default team
    List_Add_Alloc(&result->connection->teams, sizeof(TEAM), HEADER_TEAM);
  }
  return result;
}

bool HandleRoom(ROOMDOWNLOAD* view, char header, char* request, int listID)
{
  int addType = HEADER_RECT;
  IO_RECT* r = calloc(1, sizeof(IO_RECT));
  sscanf(request, FORMAT_RECT,
      &r->x, &r->y, &r->w, &r->h);

  if(header == SIGNAL_RECT)
      addType = HEADER_RECT;
  else if(header == SIGNAL_ROUND)
      addType = HEADER_ROUND;

  if(listID == LIST_FG)
      List_Add(&view->connection->fg, r, addType);
  else if(listID == LIST_BG)
      List_Add(&view->connection->bg, r, addType);

  return false;
}

bool getServer(char header, char* request, int listID, void* data)
{
    ROOMDOWNLOAD* viewData = (ROOMDOWNLOAD*)data;
    SERVERCONNECTION* server = viewData->connection;
    bool result = 0;

    if(listID == 0)
    {
      if(header == SIGNAL_GAMEMODE)
      {
          sscanf(request, FORMAT_GAMEMODE,
            (int*)&server->gamemode,
            &server->gamegoal);
      }
      
      if(header == SIGNAL_ROOMDATA)
      {
        sscanf(request, FORMAT_DOWNLOADDATA,
            &viewData->downloadTotal, 
            &viewData->roomW,
            &viewData->roomH);
      }
      
      if(header == SIGNAL_PLYRID)
      {
        int pid;
        sscanf(
          request, FORMAT_PLYRID,
          &pid);
        server->playerID = pid;
      }
    }

    if(listID == LIST_FG || listID == LIST_BG)
    {
      viewData->downloadAt ++;
    }

    if(header == SIGNAL_END)
    {
      if(listID == LIST_FG)
        viewData->fgReady = true;
      else if(listID == LIST_BG)
        viewData->bgReady = true;
    }
    else if(header != SIGNAL_BEGIN)
    {
      if(listID == LIST_FG || listID == LIST_BG)
          result = HandleRoom(viewData, header, request, listID);
      
      if(listID == LIST_FLAG)
      {
        TEAM* t = List_Add_Alloc(&server->teams, sizeof(TEAM), HEADER_TEAM);

        sscanf(request, FORMAT_RECT,
          &t->base.x, &t->base.y, &t->base.w, &t->base.h);
        
        t->id = List_Count(&server->teams) - 1;
      }
    } 
    
    result = Server_HandleGet(header, request, listID, (void*)&server);
    
    if(viewData->fgReady && viewData->bgReady)
      result =  1;

    return result;
}

int Client_RoomDownload(ROOMDOWNLOAD** download)
{
  int result = false;
  ROOMDOWNLOAD* view = (*download);
  SERVERCONNECTION* conn = (*download)->connection;

  result = Server_Get(conn->node, getServer, (void*)view);

  switch(result)
  {
    case MSG_USER:
      //Server Finished download!
      //Create Collision Index
      conn->fgCollisions = GenerateCollsions(view->connection->fg, view->roomW, view->roomH);
      conn->bgCollisions = GenerateCollsions(view->connection->bg, view->roomW, view->roomH);
    case MSG_FAIL:
      free(*download);
      *download = NULL;
      break;
    default: break;
  }
    
  return result;
}

void Client_AbortDownload(ROOMDOWNLOAD** download)
{  
  Client_Connection_Destroy(&(*download)->connection);
  free(*download);
  *download = NULL;
  return;
}

void Client_Chat(SERVERCONNECTION* conn, char* chat)
{
  char buffer[MAX_MESSAGE_SIZE];
  int sendStatus = MSG_NOTDONE;
  while(sendStatus == MSG_NOTDONE)
  {
    snprintf(buffer, MAX_MESSAGE_SIZE, FORMAT_SIGNAL, chat);
    sendStatus = Server_PutMesg(&conn->node, SIGNAL_CHAT, buffer);
  }
  return;
}