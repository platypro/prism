/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef INC_PRISM_H
#define INC_PRISM_H

//
// Library Import Headers
//

//Contains Helpful functions for dealing with
//GLFW and other libraries
#include "platypro.game/tools.h"
#include "platypro.game/ui.h"
#include "platypro.game/view.h"
#include "platypro.base/log.h"
#include "client.h"

//Use NanoVG for rendering.
#include <GLFW/glfw3.h>
#include <nanovg.h>

typedef struct GameState GAMESTATE;

#define SET_COLORTEAM(ls, id, count, intensity) \
  nvgFillColor(ls->draw, nvgHSL( (double)id / (double)count, intensity, 0.6f));

//
//Game Structures
//

typedef struct ViewManager VIEWMANAGER;

typedef struct GameState
{
  SERVERCONNECTION* connection;
  LOGMANAGER* log;
  LIST strings;
  LIST settingStrings;
  LIBSTATE* libs;
  IO_FONT font;
  IO_FONT fontMono;
  VIEWMANAGER* views;
} GAMESTATE;

//
// Defaults
//

#define GAME_SHORTNAME "Pri'sm"
#define GAME_INFO VERSION_NAME" ("VERSION") "VERSION_AUTHOR

//Default window scaling
#define CANVAS_WIDTH  1280
#define CANVAS_HEIGHT 720

#endif /*INC_PRISM_H*/
