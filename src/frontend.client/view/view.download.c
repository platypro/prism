/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
#include "common.h"
#include "view/view.h"

#include <stdlib.h>
#include <string.h>

#include "platypro.game/ui.h"
#include "platypro.base/strings.h"
#include "platypro.base/net.h"
#include "client.h"
#include "ui/chat.h"
#include "ui/playerlist.h"

#define UINAME_SPECNAME "SpecName"
#define UINAME_READYCHECK "ReadyCheck"

PRIVATE int ClickEvent_setready(UI_INDEX*,UI_ELEMENT*, void* viewdata);
PRIVATE int ClickEvent_returntomenu(UI_INDEX*,UI_ELEMENT*, void* viewdata);

PRIVATE int TextEvent_playerName(UI_INDEX*,UI_ELEMENT*, void* viewdata);

PRIVATE IO_COLOR backColor =
    (IO_COLOR){0xFF,0xE6,0xE6,0xFF};

PRIVATE UI_STYLE uiStyle = {
    (IO_COLOR){0x00,0x00,0xDD,0xFF},
    (IO_COLOR){0x45,0x45,0x45,0xFF},
    (IO_COLOR){0xFF,0xFF,0xFF,0x00},
    (IO_COLOR){0x00,0x00,0x99,0xFF},
    (IO_COLOR){0x00,0x00,0x66,0xFF},
    (IO_COLOR){0xCC,0xCC,0xCC,0xFF}
};

#define CHATBOUNDSX CANVAS_WIDTH / 2 + BUTTON_PADDING
#define CHATBOUNDSY BUTTON_PADDING
#define CHATBOUNDSW ((CANVAS_WIDTH / 2) - (BUTTON_PADDING * 2))
#define CHATBOUNDSH CANVAS_HEIGHT - BAR_HEIGHT - (3*BUTTON_PADDING)

#define READY_WIDTH 125

PRIVATE IO_RECT chatBounds = 
{
    CHATBOUNDSX,
    CHATBOUNDSY,
    CHATBOUNDSW,
    CHATBOUNDSH
};

static IO_RECT playerListBounds =
{
  BUTTON_PADDING,
  PLAYERLIST_TOP,
  ((CANVAS_WIDTH / 2) - (BUTTON_PADDING)),
  CHATBOUNDSH - (PLAYERLIST_TOP) + BUTTON_PADDING
}; 

PRIVATE UI_DEF downloadViewUI[] =
{
    {
      "",
      UITYPE_BTN,
      STR_MENU_GOBACK,
      {
        BUTTON_PADDING,
        CANVAS_HEIGHT - BAR_HEIGHT - BUTTON_PADDING,
        BUTTON_READY_WIDTH,
        BAR_HEIGHT
      },
      ClickEvent_returntomenu,
    },
    //Name entry box
    {
      UINAME_SPECNAME,
      UITYPE_TEXTBOX,
      "",
      {
        BUTTON_PADDING,
        BUTTON_PADDING + BAR_HEIGHT,
        (CANVAS_WIDTH / 2) - READY_WIDTH - BUTTON_PADDING,
        BAR_HEIGHT - VERTICAL_SPACING
      },
      TextEvent_playerName
    },
    //Ready button
    {
      UINAME_READYCHECK,
      UITYPE_CHECK,
      UI_FALSE,
      {
        (CANVAS_WIDTH / 2) - READY_WIDTH,
        BUTTON_PADDING + BAR_HEIGHT,
        READY_WIDTH,
        BAR_HEIGHT - VERTICAL_SPACING
      },
      ClickEvent_setready
    },
    //Ready Label
    {
      "",
      UITYPE_LABEL,
      STR_MENU_READYSTATUS,
      {
        (CANVAS_WIDTH / 2) - READY_WIDTH,
        BUTTON_PADDING,
        READY_WIDTH,
        BAR_HEIGHT
      },
      NULL
    },
    //Name Label
    {
     "",
     UITYPE_LABEL,
     STR_MENU_NAMEINPUT,
     {
       BUTTON_PADDING,
       BUTTON_PADDING,
       (CANVAS_WIDTH / 2) - READY_WIDTH - BUTTON_PADDING,
       BAR_HEIGHT
     }
    },
};

PRIVATE int ClickEvent_setready(UI_INDEX* index, UI_ELEMENT* object, void* state)
{
  GAMESTATE* s = (GAMESTATE*)state;
  CLIENTPLAYER* c = Server_GetActivePlayer(s->connection);
  c->player.ready = (*object->text == *UI_TRUE);
  s->connection->update_ready = true;
  return true;
}

PRIVATE int ClickEvent_returntomenu(UI_INDEX* index, UI_ELEMENT* object, void* state)
{
  GAMESTATE* s = (GAMESTATE*)state;
  Client_RequestDisconnect(&s->connection);
  return true;
}

PRIVATE int TextEvent_playerName(UI_INDEX* index, UI_ELEMENT* object, void* state)
{
  GAMESTATE* s = (GAMESTATE*)state;

  //Enforce player name limit
  object->text[PLAYER_NAMEMAX] = '\000';

  CLIENTPLAYER* p = Server_GetActivePlayer(s->connection);
  
  //Ensure name has data
  if(*object->text == '\000')
  {
    strcpy(p->player.name, " ");
  }
  else 
  {
    strncpy
    (
        p->player.name,
        object->text,
        PLAYER_NAMEMAX
    );
  }

  s->connection->update_pname = true;

  return true;
}

PUBLIC char* DownloadView_init(void* globalState, void* viewData)
{
    DOWNLOADVIEW* view = (DOWNLOADVIEW*)viewData;
    GAMESTATE*    glob = (GAMESTATE*)globalState;

    view->uiState = UI_Create(glob->libs, &uiStyle, glob->font);
    if(!view->uiState) return VIEW_BREAK;

    UI_Object_Auto(view->uiState, downloadViewUI, sizeof(downloadViewUI), glob->strings);
    
    ChatUI_Create(glob, view->uiState, chatBounds);

    strcpy(
        UI_Object_GetByName(view->uiState, UINAME_SPECNAME)->text,
        Server_GetActivePlayer(glob->connection)->player.name);

    Log_ThrowFormat(glob->log, ERRSEV_INFO, STR_DOWNLOAD_STARTED, NULL);
    if(glob->connection != NULL)
    {
          return VIEW_CONTINUE;
    }
    else
    {
          return VIEW_BREAK;
    }
}

PUBLIC char* DownloadView_update(void* globalData, void* viewData, EVENTSTATE* e)
{
  GAMESTATE* glob    = (GAMESTATE*)globalData;
  DOWNLOADVIEW* view = (DOWNLOADVIEW*)viewData;
  char* result = VIEW_CONTINUE;

  static int oldready = false;
  int newReady = oldready;

  if(glob->connection)
  {
    Client_Tick(&glob->connection);
    
    if(!glob->connection)
    {
      result = VIEW_MENU;
    }
    else
    {
      if(e)
        UI_Update(view->uiState, *e, glob);
    }
  }
  
  //Find player list hover
  if(e)
    view->e = *e;
  
  UPDATESERVER(&glob->connection);
  
  if(glob->connection)
  {
    newReady = Server_GetActivePlayer(glob->connection)->player.ready;
    
    //Test if ready status has changed and update UI
    if(oldready != newReady)
    {
      UI_ELEMENT* element = UI_Object_GetByName(view->uiState, UINAME_READYCHECK);
      
      oldready = newReady;
      *element->text = newReady ? *UI_TRUE : *UI_FALSE;
    }

    if(glob->connection->gameStage == GS_INGAME)
    {
      //Reset game time
      glob->connection->gametime = 0;
      return VIEW_ROOM;
    }
  }
  return result;
}

PUBLIC char* DownloadView_draw(void* globalData, void* viewData)
{
  GAMESTATE* glob    = (GAMESTATE*)globalData;
  DOWNLOADVIEW* view = (DOWNLOADVIEW*)viewData;
  LIBSTATE* ls = glob->libs;
  
  if(glob->connection && view->uiState)
  {
    Draw_Color(ls, backColor);
    Draw_Rect(ls, (IO_RECT){0,0,CANVAS_WIDTH, CANVAS_HEIGHT});

    //Set font type for drawing
    Draw_Color(ls, COLOR_BG);
    Draw_Rect(ls, chatBounds);
    nvgFontFace(ls->draw, glob->fontMono);
    ChatUI_Draw(glob, chatBounds);

    nvgFontFace(ls->draw, glob->font);
    PlayerListUI_Draw(glob, &view->e, playerListBounds,  
          PLUI_TEAM_SHOW | PLUI_TEAM_ALLOWCHANGE | PLUI_PLAYER_READY | PLUI_PLAYER_SHOW);

    UI_Draw(view->uiState);
  }

  return VIEW_CONTINUE;
}

PUBLIC char* DownloadView_clean(void* globalData, void* viewData)
{
  DOWNLOADVIEW* dv = (DOWNLOADVIEW*)viewData;
  if(dv->uiState)
  {
    UI_Destroy(&dv->uiState); 
  }
  return VIEW_CONTINUE;
}
