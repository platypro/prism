/*
 * Aeden McClain (c) 2017-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "common.h"
#include "view/view.h"

#include <stdlib.h>

#include "platypro.base/strings.h"
#include "platypro.game/tools.h"
#include "ui/playerlist.h"

#define LISTFLAGS_PLAYERS  PLUI_PLAYER_SCORE | PLUI_PLAYER_SHOW
#define LISTFLAGS_TEAMS    PLUI_TEAM_SHOW | PLUI_TEAM_SCORE

PRIVATE IO_COLOR backColor =
    (IO_COLOR){0xCC,0xFF,0xCC,0xFF};

PRIVATE int ClickEvent_closeintergame(UI_INDEX* index, UI_ELEMENT* element, void* state)
{
  GAMESTATE* glob = (GAMESTATE*)state;
  glob->connection->gameStage = GS_WAITFORPLAYERS;
  Views_Set(glob->views, VIEW_DOWNLOAD);
  return false;
}

#define TEAMLISTX CANVAS_WIDTH / 4
#define TEAMLISTY IGV_TEAMLIST_START
#define TEAMLISTW CANVAS_WIDTH / 2
#define TEAMLISTH CANVAS_HEIGHT - BAR_HEIGHT * 2 - BUTTON_PADDING - IGV_TEAMLIST_START

PRIVATE IO_RECT playerList =
{
  TEAMLISTX,
  TEAMLISTY,
  TEAMLISTW,
  TEAMLISTH,
};

PRIVATE UI_DEF gameviewui[] = 
{
  {
    "",
    UITYPE_BTN,
    STR_MENU_OKAY,
    {
      2 * (CANVAS_WIDTH / 5),
      CANVAS_HEIGHT - BAR_HEIGHT - BUTTON_PADDING,
      1 * (CANVAS_WIDTH / 5),
      BAR_HEIGHT
    },
    ClickEvent_closeintergame,
  },
};

PUBLIC char* IntergameView_init(void* globalData, void* viewData)
{
  INTERGAMEVIEW* view = (INTERGAMEVIEW*)viewData;
  GAMESTATE* glob = (GAMESTATE*)globalData;
  
  view->uistate = UI_Create(glob->libs, NULL, glob->font);
  view->listflags = LISTFLAGS_PLAYERS;
  
  Log_ThrowFormat(glob->log, ERRSEV_INFO, STR_GAME_END, NULL);
  
  UI_Object_Auto(view->uistate, gameviewui, sizeof(gameviewui), glob->strings);
  
  return VIEW_CONTINUE;
}

PUBLIC char* IntergameView_update(void* globalData, void* userdata, EVENTSTATE* e)
{
  INTERGAMEVIEW* view = (INTERGAMEVIEW*)userdata;
  GAMESTATE* glob = (GAMESTATE*)globalData;
  if(e)
    UI_Update(view->uistate, *e, glob);
  return VIEW_CONTINUE;
}

PUBLIC char* IntergameView_draw(void* globalData, void* userdata)
{
  GAMESTATE* glob = (GAMESTATE*)globalData;
  INTERGAMEVIEW* view = (INTERGAMEVIEW*)userdata;
  NVGcontext* nvg = glob->libs->draw;
  
  //Draw background
  Draw_Color(glob->libs, backColor);
  Draw_Rect(glob->libs, (IO_RECT){0,0,CANVAS_WIDTH, CANVAS_HEIGHT});
  
  Draw_Color(glob->libs, COLOR_RED);
  nvgTextAlign(nvg, NVG_ALIGN_BASELINE | NVG_ALIGN_CENTER);
  nvgFontSize(nvg, IGV_TEAMLIST_START);
  nvgBeginPath(nvg);
  nvgText(nvg, CANVAS_WIDTH / 2, IGV_TEAMLIST_START - 4, Strings_Get(glob->strings, STR_GAME_END), NULL);
  
  PlayerListUI_Draw(glob, NULL, playerList, PLUI_TEAM_SHOW | PLUI_PLAYER_SCORE | PLUI_PLAYER_SHOW | PLUI_TEAM_SCORE);
  
  UI_Draw(view->uistate);
  
  return VIEW_CONTINUE;
}

PUBLIC char* IntergameView_clean(void* globalData, void* viewData)
{
  INTERGAMEVIEW* view = (INTERGAMEVIEW*)viewData;
  free(view->sortedplayers);
  free(view->sortedteams);
  UI_Destroy(&view->uistate);
  return VIEW_CONTINUE;
}
