/*
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "common.h"
#include "view/view.h"

#include "platypro.base/strings.h"

#include <string.h>

int ClickEvent_closemenu(UI_INDEX* index, UI_ELEMENT* object, void* state);
int ClickEvent_returntomenu(UI_INDEX* index, UI_ELEMENT* object, void* state);

PRIVATE UI_DEF gameMenuViewUI[] = 
{
  {
    "",
    UITYPE_BTN,
    STR_NO,
    {
      (CANVAS_WIDTH / 2) - (CANVAS_WIDTH / 5) - 5,
      3*(CANVAS_HEIGHT / 5),
      CANVAS_WIDTH / 5,
      BAR_HEIGHT
    },
    ClickEvent_closemenu,
  },
  {
    "",
    UITYPE_BTN,
    STR_YES,
    {
      (CANVAS_WIDTH  / 2) + 5,
      3*(CANVAS_HEIGHT / 5),
      CANVAS_WIDTH / 5,
      BAR_HEIGHT
    },
    ClickEvent_returntomenu,
  },
  {
      "",
      UITYPE_LABEL,
      STR_MENU_LEAVECONFIRM,
      {
        (CANVAS_WIDTH / 2) - (CANVAS_WIDTH / 5) - 5,
        2*(CANVAS_HEIGHT / 5),
        2*(CANVAS_WIDTH / 5) + 10,
        CANVAS_HEIGHT / 5
      },
      NULL,
    },
};

int ClickEvent_closemenu(UI_INDEX* index, UI_ELEMENT* object, void* state)
{
  Views_CloseFloat(((GAMESTATE*)state)->views);
  return false;
}

int ClickEvent_returntomenu(UI_INDEX* index, UI_ELEMENT* object, void* state)
{
  Client_RequestDisconnect(&((GAMESTATE*)state)->connection);
  Views_CloseFloat(((GAMESTATE*)state)->views);
  return false;
}

char* GameMenuView_init(void* globalData, void* viewData)
{
  GAMEMENUVIEW* mv = (GAMEMENUVIEW*)viewData;
  GAMESTATE* glob = (GAMESTATE*)globalData;
  mv->ui = UI_Create(glob->libs, NULL, glob->font);
  UI_Object_Auto(mv->ui, gameMenuViewUI, sizeof(gameMenuViewUI), glob->strings);
  
  return VIEW_CONTINUE;
}

char* GameMenuView_update(void* globalData, void* viewData, EVENTSTATE* e)
{
  GAMEMENUVIEW* mv = (GAMEMENUVIEW*)viewData;
  GAMESTATE* glob = (GAMESTATE*)globalData;
  if(e)
    UI_Update(mv->ui, *e, glob);

  return VIEW_CONTINUE;
}

char* GameMenuView_draw(void* globalData, void* viewData)
{
  GAMEMENUVIEW* mv = (GAMEMENUVIEW*)viewData;
  UI_Draw(mv->ui);
  
  return VIEW_CONTINUE;
}

char* GameMenuView_clean(void* globalData, void* viewData)
{
  GAMEMENUVIEW* mv = (GAMEMENUVIEW*)viewData;
  UI_Destroy(&mv->ui);
  return VIEW_CONTINUE;
}

