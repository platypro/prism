/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include "common.h"
#include "view.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "platypro.base/list.h"
#include "platypro.base/strings.h"
#include "platypro.base/net.h"
#include "platypro.game/tools.h"
#include "client.h"
#include "frontend.client.h"
#include "ui/playerlist.h"
#include "ui/chat.h"

PRIVATE ROOMVIEW* globalRoomView;

PRIVATE IO_RECT playerList =
{
  CANVAS_WIDTH / 4,
  CANVAS_HEIGHT / 4,
  CANVAS_WIDTH / 2,
  CANVAS_HEIGHT / 2
};

PRIVATE IO_RECT chatBounds_focused =
{
  0,
  CANVAS_HEIGHT / 2 - 10,
  CANVAS_WIDTH,
  (CANVAS_HEIGHT / 2) - FONTSIZE_CHAT
};

PRIVATE IO_RECT chatBounds_unfocused = 
{
  0,
  CANVAS_HEIGHT - (3*FONTSIZE_CHAT) - 10,
  CANVAS_WIDTH,
  (2*FONTSIZE_CHAT)
};

IO_COLOR playerColor_in = {70,70,255,255};
IO_COLOR playerColor_out = {175,175,175,255};

bool drawCollisions(void* user, IO_RECT rect1, LIST_HEADER type1, IO_RECT rect2, LIST_HEADER type2)
{
  //Draw the object
  if(type2 == HEADER_RECT)
	Draw_Rect((LIBSTATE*)user, rect2);
  else if(type2 == HEADER_ROUND)
    Draw_Round((LIBSTATE*)user, rect2);

  return true;
}

PRIVATE void drawFlag(NVGcontext* c, int x, int y, int size)
{
  nvgFillColor(c, nvgRGB(0,0,0));
  nvgBeginPath(c);
  nvgMoveTo(c, x     , y);
  nvgLineTo(c, x + 1 , y);
  nvgLineTo(c, x + 1        , y - (size / 3));
  nvgLineTo(c, x + size - 2 , y - (size * 2 / 3));
  nvgLineTo(c, x            , y -  size);
  nvgClosePath(c);
  nvgFill(c);
  
  nvgFillColor(c, nvgRGB(255,255,255));
  nvgBeginPath(c);
  nvgMoveTo(c, x + 1        , y + 1 - size);
  nvgLineTo(c, x + size - 3 , y - (size * 2 / 3));
  nvgLineTo(c, x + 1        , y - (size * 1 / 3) - 0.5);
  nvgClosePath(c);
  nvgFill(c);
  
  return;
}

void drawPlayers(GAMESTATE* ls, LIST players, int teamcount, int gamemode)
{
  if(players)
  {
    CLIENTPLAYER* p;

    foreach(p, players)
    {
      if(p->player.team->id != 0 || gamemode == GAMEMODE_NONE)
      {
        static IO_CIRCLE area;
        area = (IO_CIRCLE){p->player.pos.x, p->player.pos.y, PLAYER_RADIUS};
        Draw_Color(ls->libs, playerColor_out);
        Draw_Circle(ls->libs, area);
        
        if(p->player.stamina == PLAYER_STAMINA_MAX)
          area.r = PLAYER_RADIUS;
        else 
          area.r = (((float)PLAYER_RADIUS / (float)PLAYER_STAMINA_MAX) * (float)p->player.stamina);
        
        area.r --;

        SET_COLORTEAM(ls->libs, p->player.team->id, teamcount, 0.4);
        Draw_Circle(ls->libs, area);
        
        if(p->player.hasFlag)
        {
          drawFlag(ls->libs->draw, p->player.pos.x - 8, p->player.pos.y, FLAG_SIZE);
        }
        
        Draw_Color(ls->libs, COLOR_BLACK);
        Draw_Text(ls->libs, ls->font, (IO_POINT){p->player.pos.x, p->player.pos.y}, 4.0f, p->player.name);
      }
    }
  }
  return;
}

PRIVATE void drawTeams(LIBSTATE* ls, LIST teams, int teamcount)
{
  TEAM* t;
  int i;
  
  foreach(t, teams)
  {
    int flagx = t->base.x + (t->base.w / 2) - ((t->flagCount / 2)*FLAG_SIZE);
    int flagy = t->base.y + (t->base.h / 2);
    SET_COLORTEAM(ls, t->id, teamcount, 0.5f);
    Draw_Rect(ls, t->base);

    Draw_Color(ls, COLOR_BLACK);
    for(i = 0; i< t->flagCount;i++)
    {
      drawFlag(ls->draw, flagx + (i * FLAG_SIZE), FLAG_SIZE + flagy, FLAG_SIZE);
    }
  }
}

void drawRoomBorder(SERVERCONNECTION* server, LIBSTATE* ls)
{
  IO_RECT zone = (IO_RECT)
      {0,0,server->fgCollisions->roomW, server->fgCollisions->roomH};
  Draw_Color(ls, (IO_COLOR){255,255,255,255});
  Draw_Rect(ls, zone);
}

PUBLIC char* RoomView_init (void* global, void* view)
{
  ROOMVIEW* v = (ROOMVIEW*)view;
  GAMESTATE* g = (GAMESTATE*)global;

  if (!v)
  {
    Log_ThrowFormat(g->log, ERRSEV_FATAL, ERRID_MEM, "GameState");
  }
  else
  {
    Log_ThrowFormat(g->log, ERRSEV_INFO, STR_CLIENT_STARTED, NULL);
    Client_Tick(&g->connection);

    return VIEW_CONTINUE;
  }

  return VIEW_BREAK;
}

#define VELKEY(key, dir) \
  if(glfwGetKey(state->libs->window, (key)) == GLFW_PRESS) \
  { if(Client_SetVelocity(state->server, (dir))) result=true;} \

PRIVATE IO_POINT handleKeys(LIBSTATE* ls)
{
    IO_POINT intentVelocity;
    int moveSpeed = (glfwGetKey(ls->window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) ?
      RUN_SPEED : MOVEMENT_SPEED;

    if(glfwGetKey(ls->window, GLFW_KEY_D) == GLFW_PRESS)
      intentVelocity.x = moveSpeed;
    else if(glfwGetKey(ls->window, GLFW_KEY_A) == GLFW_PRESS)
      intentVelocity.x = -moveSpeed;
    else intentVelocity.x = 0;

    if(glfwGetKey(ls->window, GLFW_KEY_S) == GLFW_PRESS)
      intentVelocity.y = moveSpeed;
    else if(glfwGetKey(ls->window, GLFW_KEY_W) == GLFW_PRESS)
      intentVelocity.y = -moveSpeed;
    else intentVelocity.y = 0;

    return intentVelocity;
}

PUBLIC char* RoomView_update (void* g, void* v, EVENTSTATE* e)
{
  ROOMVIEW*  view = (ROOMVIEW*)v;
  GAMESTATE* glob = (GAMESTATE*)g;

  PLAYER* p = &glob->connection->activePlayer->player;
  IO_POINT intentVelocity = {0,0};
  int tick;
  
  globalRoomView = view;

  if(!view->chatOpen)
  {
    //Test Key Presses
    intentVelocity = handleKeys(glob->libs);
  }
  
  if(e)
  {
    unsigned short* buffCursor = (*e).stringBuffer;
    while(*buffCursor != '\000')
    {
      if(view->chatOpen && view->chatat <= MAX_CHAT_SIZE)
      {
        switch(*buffCursor)
        {
          case STRBUF_BACKSPACE:
            if(view->chatat > 0)
            {
              *(view->chatbuffer + view->chatat - 1) = '\000';
              view->chatat--;
            }
            break;
          case STRBUF_ENTER:
            Client_Chat(glob->connection, view->chatbuffer);
          case STRBUF_ESCAPE:
            view->chatat = 0; 
            memset(view->chatbuffer, 0, MAX_CHAT_SIZE);
            view->chatOpen = false;
            
            //Get keys (to avoid GLFW_STICKY_KEYS)
            glfwGetKey(glob->libs->window, GLFW_KEY_W);
            glfwGetKey(glob->libs->window, GLFW_KEY_A);
            glfwGetKey(glob->libs->window, GLFW_KEY_S);
            glfwGetKey(glob->libs->window, GLFW_KEY_D);
            
            break;
        }
        
        if(*buffCursor < 255 && view->chatat < MAX_CHAT_SIZE)
        {
          *(view->chatbuffer + view->chatat) = *buffCursor;
          view->chatat++;
        }
      } 
      else
      {
        if(*buffCursor == STRBUF_ESCAPE)
        {
          //Stop player
          intentVelocity = (IO_POINT){0,0};
          //Display game menu
          Views_SetFloat(glob->views, VIEW_GAMEMENU);
        }
      }
    
      if(*buffCursor == STRBUF_TAB) view->playersOpen = true;
      if(*buffCursor == STRBUF_UNTAB) view->playersOpen = false;
      if(*buffCursor == 'c')        view->chatOpen = true;
      
      buffCursor ++;
    }
  }
  
  if(p->stamina == 0)
  {
    if(intentVelocity.x == RUN_SPEED)
      intentVelocity.x = MOVEMENT_SPEED;
    else if(intentVelocity.x == -RUN_SPEED)
      intentVelocity.x = -MOVEMENT_SPEED;
    
    if(intentVelocity.y == RUN_SPEED)
      intentVelocity.y = MOVEMENT_SPEED;
    else if(intentVelocity.y == -RUN_SPEED)
       intentVelocity.y = -MOVEMENT_SPEED;
  }
  
  if ((tick = Client_Tick(&glob->connection)))
  {
    Client_UpdateVelocities(glob->connection, tick, intentVelocity);
  }
  
  if(!glob->connection)
    Views_Set(glob->views, VIEW_MENU);
  else
  {
    //Target the view on the active player
    CLIENTPLAYER* activePlayer = Server_GetActivePlayer(glob->connection);
    if(activePlayer)
    {
        view->target.x = (GAME_SCALE * activePlayer->player.pos.x) - (CANVAS_WIDTH / 2);
        view->target.y = (GAME_SCALE * activePlayer->player.pos.y) - (CANVAS_HEIGHT / 2);
    }
  }
  
  UPDATESERVER(&glob->connection);

  return VIEW_CONTINUE;
}

PRIVATE void drawHUDObj(NVGcontext* draw, int obj, char* text, int val)
{
  int drawcenter = CANVAS_WIDTH - (HUDITEM_WIDTH * obj);
  static char numtext[3];
  static int textbuf = -1;
  
  if(textbuf != val)
  {
    textbuf = val;
    snprintf(numtext, 3, "%d", val);
  }
  
  nvgFontSize(draw, HUD_SIZE * 3 / 4);
  nvgBeginPath(draw);
  nvgTextAlign(draw, NVG_ALIGN_TOP|NVG_ALIGN_CENTER);
  nvgText(draw, drawcenter, 0, numtext, NULL);
  
  nvgFontSize(draw, HUD_SIZE * 2 / 5);
  nvgBeginPath(draw);
  nvgTextAlign(draw, NVG_ALIGN_BASELINE|NVG_ALIGN_CENTER);
  nvgText(draw, drawcenter, HUD_SIZE - 2, text, NULL);
}

PRIVATE void drawHUD(GAMESTATE* state, PLAYER* p)
{
  SERVERCONNECTION* server = state->connection;
  NVGcontext* nvg = state->libs->draw;
  
  char buffer[10];
  //Draw Game Header
  Draw_Color(state->libs, HUD_COLOR);
  Draw_Rect(state->libs, (IO_RECT){0,0,CANVAS_WIDTH, HUD_SIZE});
  
  char* modetext = "";
  
  if(p->team->id == 0 && server->gamemode != GAMEMODE_NONE)
  {
    modetext = Strings_Get(state->strings, STR_GM_SPECTATE);
  }
  else
  {  
    //Draw gamemode
    switch(server->gamemode)
    {
      case GAMEMODE_NONE:
        modetext = Strings_Get(state->strings, STR_GM_NONE);
        break;
      case GAMEMODE_THRESHOLD:
        modetext = Strings_Get(state->strings, STR_GM_THRESHOLD);
        break;
      case GAMEMODE_CONTROL:
        modetext = Strings_Get(state->strings, STR_GM_CONTROL);
        break;
      case GAMEMODE_TIMED:
        modetext = Strings_Get(state->strings, STR_GM_TIMED);
        break;
      case GAMEMODE_DEBUG:
        modetext = Strings_Get(state->strings, STR_GM_DEBUG);
        break;
    }
  }
  
  Draw_Color(state->libs, COLOR_RED);
  
  nvgFontSize(nvg, HUD_SIZE / 2);
  
  nvgBeginPath(nvg);
  nvgTextAlign(nvg, NVG_ALIGN_LEFT | NVG_ALIGN_MIDDLE);
  nvgText(nvg, BUTTON_PADDING, HUD_SIZE / 2, modetext, NULL);
  
  //Draw Score
  Draw_Color(state->libs, COLOR_BLACK);
  
  int displaytime = server->gametime;
  
  //Count down time if in timed gamemode
  if(server->gamemode == GAMEMODE_TIMED)
    displaytime = server->gamegoal - displaytime;   
  
  snprintf(buffer, 10, "%.2u:%.2u", (int)(displaytime / 60), displaytime % 60);
  
  Draw_Text(state->libs, state->font, (IO_POINT) { CANVAS_WIDTH / 2, HUD_SIZE / 2 }, HUD_SIZE, buffer);
  
  drawHUDObj(nvg, 1, Strings_Get(state->strings, STR_SCORE), p->score);
  drawHUDObj(nvg, 2, Strings_Get(state->strings, STR_TEAM), p->team->teamscore);
  
  //Draw gamemode specific elements
  if(server->gamemode == GAMEMODE_THRESHOLD)
    drawHUDObj(nvg, 5, Strings_Get(state->strings, STR_GM_WINAT), server->gamegoal);
}

PRIVATE void drawChat(GAMESTATE* state, ROOMVIEW* view)
{
  LIBSTATE* globalLibs = state->libs;
  IO_COLOR chatBoxOutline = view->chatOpen ? COLOR_GREEN : COLOR_RED; 
  nvgStrokeColor(globalLibs->draw, nvgRGB(chatBoxOutline.r, chatBoxOutline.g, chatBoxOutline.b));
  
  Draw_Color(globalLibs, COLOR_WHITE);
  
  nvgBeginPath(globalLibs->draw);
  nvgRect(
    globalLibs->draw, 
    1, 
    CANVAS_HEIGHT - FONTSIZE_CHAT + 1, 
    CANVAS_WIDTH - 2, 
    FONTSIZE_CHAT - 2);
  nvgStroke(globalLibs->draw);
  nvgFill(globalLibs->draw);
  
  nvgBeginPath(globalLibs->draw);
  nvgTextAlign(globalLibs->draw,NVG_ALIGN_LEFT|NVG_ALIGN_TOP);
  nvgFontSize(globalLibs->draw, FONTSIZE_CHAT);
  
  Draw_Color(globalLibs, COLOR_BLACK);
  
  if(view->chatOpen)
  {
    //Draw pending chat
    nvgText(
      globalLibs->draw, 
      2, 
      CANVAS_HEIGHT - FONTSIZE_CHAT + 1,
      view->chatbuffer, NULL);
    
    ChatUI_Draw(state, chatBounds_focused);
  } 
  else 
  {
    nvgText(
      globalLibs->draw, 
      2, 
      CANVAS_HEIGHT - FONTSIZE_CHAT, 
      Strings_Get(state->strings, STR_INFO_CHAT),
      NULL);
    
    ChatUI_Draw(state, chatBounds_unfocused);
  }
}

PUBLIC char* RoomView_draw  (void* g, void* v)
{
  ROOMVIEW*  view = (ROOMVIEW*)v;
  GAMESTATE* glob = (GAMESTATE*)g;
  LIBSTATE* globalLibs = glob->libs;

  Draw_Color(globalLibs, (IO_COLOR){0x00,0x00,0x00,0x99});
  Draw_Rect(globalLibs,(IO_RECT){0,0,CANVAS_WIDTH,HUD_SIZE});
  
  if(glob->connection)
  {
    //Focus the view on the player
    nvgTranslate(globalLibs->draw, -view->target.x, -view->target.y);
    nvgScale(globalLibs->draw, GAME_SCALE, GAME_SCALE);

    glClearColor(0.5,0.5,0.5,1);
    Window_Clear(globalLibs);

    IO_RECT gameZone = {view->target.x / GAME_SCALE, view->target.y / GAME_SCALE,
        CANVAS_WIDTH / GAME_SCALE, CANVAS_HEIGHT / GAME_SCALE};

    int teamcount = List_Count(&glob->connection->teams);
        
    //Now draw the game area based on the loaded level
    //Draw Border
    drawRoomBorder(glob->connection, globalLibs);
    //Draw team bases
    drawTeams(globalLibs, glob->connection->teams, teamcount);
    //Draw Foreground
    Draw_Color(globalLibs, COLOR_FG);
	
    HandleCollisionData(gameZone, HEADER_RECT, glob->connection->fgCollisions, drawCollisions, globalLibs);
    //Draw Background
    Draw_Color(globalLibs, COLOR_BG);
    HandleCollisionData(gameZone, HEADER_RECT, glob->connection->bgCollisions, drawCollisions, globalLibs);
	
    //Now draw players
    drawPlayers(glob, glob->connection->playerStore, teamcount, glob->connection->gamemode);
    
    nvgResetTransform(globalLibs->draw);
    drawHUD(glob, &Server_GetActivePlayer(glob->connection)->player);
    
    drawChat(glob, view);
    
    if(view->playersOpen)
    {
      Draw_Color(globalLibs, HUD_COLOR);
      Draw_Rect(globalLibs, playerList);
      
      PlayerListUI_Draw(glob, NULL, playerList,  
                  PLUI_TEAM_SHOW | PLUI_PLAYER_SCORE | PLUI_PLAYER_SHOW | PLUI_TEAM_SCORE);
    }
  }
  
  return VIEW_CONTINUE;
}

PUBLIC char* RoomView_clean (void* globalData, void* viewData)
{
  GAMESTATE* game = (GAMESTATE*)globalData;
  
  if(game->connection && game->connection->gameStage == GS_INGAME)
    Client_Connection_Destroy(&game->connection);
  
  return VIEW_CONTINUE;
}

