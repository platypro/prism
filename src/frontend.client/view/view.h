/*
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SRC_CLIENT_VIEWS_VIEWS_H_
#define SRC_CLIENT_VIEWS_VIEWS_H_

#include "frontend.client.h"
#include "platypro.base/list.h"
#include "client.h"

#define VIEW_MENU     "menuView"
#define VIEW_DOWNLOAD "downloadView"
#define VIEW_ROOM     "roomView"
#define VIEW_MESSAGE  "messageView"
#define VIEW_INTERGAME "intergameView"
#define VIEW_GAMEMENU  "gamemenuView"

//Server update macro
#define UPDATESERVER(connection) \
  if(!Server_Update(connection))    Views_Set(glob->views, VIEW_MENU); \
  else if((*connection)->gameStage == GS_INTERGAME) Views_Set(glob->views, VIEW_INTERGAME);

//////////////////////////////
//        MenuView          //
//////////////////////////////

#define MENU_BUTTON_WIDTH 400
#define MENU_BUTTON_HEIGHT 40
#define MENU_BUTTON_SPACING 15
#define MENU_GAMEICON_HEIGHT 50

typedef enum ConnectionState
{
  CS_MENU,
  CS_CONNECTING,
  CS_DOWNLOADING,
  CS_GETTINGPLAYER
} CONNECTIONSTATE;

typedef struct MenuView
{
  UI_INDEX* uiState;

  ROOMDOWNLOAD* roomDownload;

  CONNECTIONSTATE connectionStatus; //!< Determines whether to try connecting to server during update
  float backHue; //!< Background color of menu

  IO_RECT menuSize;
  int titleImageID;
  
  UI_DEF* nextMenu;
  size_t nextMenuSize;
} MENUVIEW;

#define MENUVIEW_PRISMWIDTH  60
#define MENUVIEW_PRISMHEIGHT 90
#define MENUVIEW_TITLEHEIGHT 130.0f

extern char* MenuView_init   (void* globalData, void* viewData);
extern char* MenuView_update (void* globalData, void* viewData, EVENTSTATE* e);
extern char* MenuView_draw   (void* globalData, void* viewData);
extern char* MenuView_clean  (void* globalData, void* viewData);

////////////////////////
//    DownloadView    //
////////////////////////

typedef struct DownloadView
{
  UI_INDEX* uiState;
  
  EVENTSTATE e; //!< Event state (for player list interaction)
} DOWNLOADVIEW;

#define BUTTON_READY_WIDTH 250

#define PLAYERLIST_TOP (1.5*BUTTON_PADDING) + (2*BAR_HEIGHT)

extern char* DownloadView_init   (void* globalData, void* viewData);
extern char* DownloadView_update (void* globalData, void* viewData, EVENTSTATE* e);
extern char* DownloadView_draw   (void* globalData, void* viewData);
extern char* DownloadView_clean  (void* globalData, void* viewData);

///////////////////////////
//        RoomView       //
///////////////////////////

typedef struct RoomView
{
  IO_POINT target;
  
  char chatbuffer[MAX_CHAT_SIZE];
  char chatat;
  
  bool chatOpen;
  bool playersOpen;
} ROOMVIEW;

#define PLAYER_DEFAULT_X   PLAYER_RADIUS
#define PLAYER_DEFAULT_Y   PLAYER_RADIUS

//Sizes and values
#define HUD_SIZE 64
#define HUD_SPACING 5
#define HUD_COLOR (IO_COLOR){0x99,0xFF,0x99,0xFF}
#define GAME_SCALE 5
#define HUDITEM_WIDTH 100
#define FLAG_SIZE 10

#define TICK_LENGTH 5000

extern char* RoomView_init   (void* global, void* view);
extern char* RoomView_update (void* global, void* view, EVENTSTATE* e);
extern char* RoomView_draw   (void* global, void* view);
extern char* RoomView_clean  (void* global, void* view);

/////////////////////////////
//       MessageView       //
/////////////////////////////

typedef struct MessageView
{
  bool xButtonHighlight;
  char message[255];
} MESSAGEVIEW;

extern char* MessageView_init   (void* global, void* view);
extern char* MessageView_update (void* global, void* view, EVENTSTATE* e);
extern char* MessageView_draw   (void* global, void* view);
extern char* MessageView_clean  (void* global, void* view);

/////////////////////////////
//      InterGameView      //
/////////////////////////////

typedef struct IntergameView
{
  UI_INDEX* uistate;
  PLAYER** sortedplayers;
  TEAM**   sortedteams;
  int listflags;
} INTERGAMEVIEW;

extern char* IntergameView_init   (void* global, void* view);
extern char* IntergameView_update (void* global, void* view, EVENTSTATE* e);
extern char* IntergameView_draw   (void* global, void* view);
extern char* IntergameView_clean  (void* global, void* view);

#define IGV_OPTION_WIDTH 125
#define IGV_TEAMLIST_START 50

extern GAMEVIEW gameViews[6];

/////////////////////////////
//       GameMenuView      //
/////////////////////////////
typedef struct GameMenuView
{
  UI_INDEX* ui;
} GAMEMENUVIEW;

extern char* GameMenuView_init   (void* global, void* view);
extern char* GameMenuView_update (void* global, void* view, EVENTSTATE* e);
extern char* GameMenuView_draw   (void* global, void* view);
extern char* GameMenuView_clean  (void* global, void* view);

#endif /* SRC_CLIENT_VIEWS_VIEWS_H_ */
