/*
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "common.h"
#include "view/view.h"

#include "platypro.base/strings.h"
#include "frontend.client.h"

#include "platypro.game/ui.h"

#include <string.h>
#include <stdlib.h>

#define UINAME_USERNAME "UIUser"
#define UINAME_ADDR "UIAddr"
#define UINAME_PORT "UIPort"
#define UINAME_CONNECTING "UIConnecting"

int ClickEvent_dojoin(UI_INDEX* index, UI_ELEMENT* object, void* viewdata);
int ClickEvent_autojoin(UI_INDEX* index, UI_ELEMENT* object, void* viewdata);
int ClickEvent_goback(UI_INDEX* index, UI_ELEMENT*  object, void* viewdata);
int ClickEvent_joingame(UI_INDEX* index, UI_ELEMENT* object, void* viewdata);
int ClickEvent_settings(UI_INDEX* index, UI_ELEMENT* object, void* viewdata);
int ClickEvent_fullscreen(UI_INDEX* index, UI_ELEMENT* object, void* viewdata);
int ClickEvent_leaveSettings(UI_INDEX* index, UI_ELEMENT* object, void* viewdata);
int ClickEvent_quitgame(UI_INDEX* index, UI_ELEMENT* object, void* viewdata);

PRIVATE UI_DEF joinGameItems[] =
{
    {"", UITYPE_LABEL, STR_MENU_SETADDR      , (IO_RECT){0,0,0,0}, NULL},
    {UINAME_ADDR, UITYPE_TEXTBOX, DEF_SERVER , (IO_RECT){0,0,0,0}, NULL},
    {"", UITYPE_LABEL, STR_MENU_SETPORT      , (IO_RECT){0,0,0,0}, NULL},
    {UINAME_PORT, UITYPE_TEXTBOX, DEF_PORT   , (IO_RECT){0,0,0,0}, NULL},
    {"", UITYPE_BTN, STR_MENU_JOINGAME       , (IO_RECT){0,0,0,0}, ClickEvent_dojoin},
    {"", UITYPE_BTN, STR_MENU_GOBACK         , (IO_RECT){0,0,0,0}, ClickEvent_goback}
};

PRIVATE UI_DEF settingsItems[] =
{
  {"", UITYPE_LABEL, STR_MENU_NAMEINPUT, (IO_RECT){0,0,0,0}, NULL},
  {UINAME_USERNAME, UITYPE_TEXTBOX, " ", (IO_RECT){0,0,0,0}, NULL},
  {"", UITYPE_BTN, STR_MENU_FULLSCREEN, (IO_RECT){0,0,0,0}, ClickEvent_fullscreen},
  {"", UITYPE_BTN, STR_MENU_GOBACK, (IO_RECT){0,0,0,0}, ClickEvent_leaveSettings}
};

PRIVATE UI_DEF menuItems[] =
{
  {"", UITYPE_BTN, STR_MENU_JOINGAME, (IO_RECT){0,0,0,0}, ClickEvent_joingame},
  {"", UITYPE_BTN, STR_MENU_QUICKJOIN, (IO_RECT){0,0,0,0}, ClickEvent_autojoin},
  {"", UITYPE_BTN, STR_MENU_SETTINGS, (IO_RECT){0,0,0,0}, ClickEvent_settings},
  {"", UITYPE_BTN, STR_MENU_EXIT,  (IO_RECT){0,0,0,0}, ClickEvent_quitgame}
};

#define setMenu(mv, items, itemsSize) \
  (mv)->nextMenu = items;\
  (mv)->nextMenuSize = itemsSize;

int ClickEvent_fullscreen(UI_INDEX* index, UI_ELEMENT* object, void* gameState)
{
  GAMESTATE* state = (GAMESTATE*)gameState;
  char* set = state->libs->isFullscreen ? SETTINGSTR_FALSE : SETTINGSTR_TRUE;
  
  Strings_Set(&state->settingStrings, SETTING_FULLSCREEN, set);
  Window_FullScreen(state->libs, set);
  
  return true;
}

int ClickEvent_leaveSettings(UI_INDEX* index, UI_ELEMENT* object, void* gameState)
{
  GAMESTATE* state = (GAMESTATE*)gameState;
  MENUVIEW* view   = (MENUVIEW*)Views_Get(state->views);
  UI_ELEMENT* nameentry = UI_Object_GetByName(view->uiState,UINAME_USERNAME);
  Strings_Set(&state->settingStrings, SETTING_USERNAME, nameentry->text);
  Strings_Write(state->settingStrings, DEF_SETTINGSFILE, SETTINGS_HEADER);
  return ClickEvent_goback(index, object, gameState);
}

int ClickEvent_dojoin(UI_INDEX* index, UI_ELEMENT* object, void* gameState)
{
  GAMESTATE* state = (GAMESTATE*)gameState;
  MENUVIEW* view   = (MENUVIEW*)Views_Get(state->views);
  glfwGetKey(state->libs->window, GLFW_KEY_ESCAPE);
  view->connectionStatus = CS_CONNECTING;
  return true;
}

int ClickEvent_autojoin(UI_INDEX* index, UI_ELEMENT* object, void* gameState)
{
  GAMESTATE* state = (GAMESTATE*)gameState;
  MENUVIEW* view   = (MENUVIEW*)Views_Get(state->views);
  setMenu(view, joinGameItems, sizeof(joinGameItems));
  return ClickEvent_dojoin(index, object, gameState);
}

int ClickEvent_settings(UI_INDEX* index, UI_ELEMENT* object, void* gameState)
{
  GAMESTATE* state = (GAMESTATE*)gameState;
  MENUVIEW* view   = (MENUVIEW*)Views_Get(state->views);
  char* nametext = Strings_GetValue(state->settingStrings, SETTING_USERNAME);
  
  settingsItems[1].text = nametext ? nametext : "";
  
  setMenu(view, settingsItems, sizeof(settingsItems));
  
  return true;
}

int ClickEvent_goback(UI_INDEX* index, UI_ELEMENT* object, void* gameState)
{ 
  GAMESTATE* state = (GAMESTATE*)gameState;
  MENUVIEW* view   = (MENUVIEW*)Views_Get(state->views);
  setMenu(view, menuItems, sizeof(menuItems)); 
  return true;
}

int ClickEvent_joingame(UI_INDEX* index, UI_ELEMENT* object, void* gameState)
{ 
  GAMESTATE* state = (GAMESTATE*)gameState;
  MENUVIEW* view   = (MENUVIEW*)Views_Get(state->views);
  setMenu(view, joinGameItems, sizeof(joinGameItems)); 
  return true;
}

int ClickEvent_quitgame(UI_INDEX* index, UI_ELEMENT* object, void* gameState)
{ 
  GAMESTATE* state = (GAMESTATE*)gameState;
  Views_Set(state->views, VIEW_BREAK);
  return true;
}

IO_RECT createButtons(GAMESTATE* state, UI_INDEX* ui, UI_DEF* buttons, int numButtons)
{
  int i;
  IO_RECT result = {0,0,0,0};
  int uix   = (CANVAS_WIDTH / 2) - (MENU_BUTTON_WIDTH / 2);
  int cy    = (CANVAS_HEIGHT / 2) -
      ((MENU_BUTTON_HEIGHT + MENU_BUTTON_SPACING) * (numButtons-1) / 2);

  result.x = uix - MENU_BUTTON_SPACING;
  result.y = cy  - MENU_BUTTON_SPACING;

  for(i=0;i<numButtons;i++)
  {    
    char* newtext = NULL;
    buttons[i].dimens = (IO_RECT)
        {uix, cy, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT};
        
    //Auto fill server name and port values from settings
    if(!strcmp(buttons[i].name, UINAME_ADDR))
    {
      newtext = Strings_GetValue(state->settingStrings, SETTING_SERVERADDR);
    } 
    else if(!strcmp(buttons[i].name, UINAME_PORT))
    {
      newtext = Strings_GetValue(state->settingStrings, SETTING_SERVERPORT);
    }
    
    if(newtext)
    {
      buttons[i].text=newtext; 
    }
        
    buttons[i].text = Strings_Get(state->strings, buttons[i].text);
    UI_Object_Create(ui, buttons[i]);

    cy+=MENU_BUTTON_HEIGHT + MENU_BUTTON_SPACING;
  }

  result.w = MENU_BUTTON_WIDTH + (2*MENU_BUTTON_SPACING);
  result.h = numButtons * (MENU_BUTTON_HEIGHT + MENU_BUTTON_SPACING) + MENU_BUTTON_SPACING;

  return result;
}

char* MenuView_init (void* globalData, void* viewData)
{
  GAMESTATE* glob = (GAMESTATE*)globalData;
  MENUVIEW* view = (MENUVIEW*)viewData;

  view->uiState = UI_Create(glob->libs, NULL, glob->font);

  view->menuSize = createButtons(glob, view->uiState, menuItems,
      sizeof(menuItems) / sizeof(UI_DEF));
  return VIEW_CONTINUE;
}

float cycleFloat(float* cycle, float* amount, float max)
{
  if(*cycle + *amount > max || *cycle + *amount < 0)
  {
    *amount = -(*amount);
  }
    *cycle += *amount;

  return *cycle;
}

char* MenuView_update (void* globalData, void* viewData, EVENTSTATE* e)
{
  MENUVIEW* mv = (MENUVIEW*)viewData;
  GAMESTATE* glob = (GAMESTATE*)globalData;
  UI_INDEX* ui = mv->uiState;
  SERVERCONNECTION* s;
  char *addr, *port;

  switch(mv->connectionStatus)
  {
  case CS_MENU:
    if(e)
    UI_Update(ui, *e, glob);    
    break;
  case CS_CONNECTING:
    addr = UI_Object_GetByName(ui, UINAME_ADDR)->text;
    port = UI_Object_GetByName(ui, UINAME_PORT)->text;
    s = Client_Connection_Create(
      glob->log, 
      addr, port );
    
    if(s)
    {      
      //Save server info (for next time)
      Strings_Set(&glob->settingStrings, SETTING_SERVERADDR, addr);
      Strings_Set(&glob->settingStrings, SETTING_SERVERPORT, port);
	  Strings_Write(glob->settingStrings, DEF_SETTINGSFILE, SETTINGS_HEADER);
      
      if(s->node)
      {
        char buffer[MAX_MESSAGE_SIZE];
        glob->connection = s;
        mv->roomDownload = Client_InitDownload(s);
        mv->connectionStatus = CS_DOWNLOADING;
        
        //Send our username
        snprintf(buffer, MAX_MESSAGE_SIZE, FORMAT_PNAMEPRINT, 
                 0, Strings_Get(glob->settingStrings, SETTING_USERNAME));
        
        Server_PutMesg(&glob->connection->node, SIGNAL_PNAME, buffer);
      }
      else
      {
        free(s);
        mv->connectionStatus = CS_MENU;
      }
    } else
    {
      //Log_ThrowFormat(glob->log, ERRSEV_USERWARN, ERRID_CONNECTION, NULL);
      //Views_Set(glob->views, VIEW_BREAK);
	  mv->connectionStatus = CS_MENU;
	  return VIEW_CONTINUE;
    }
    break;
  case CS_DOWNLOADING:
    switch(Client_RoomDownload(&mv->roomDownload))
    {
      case MSG_USER:
        mv->connectionStatus = CS_GETTINGPLAYER;
        break;
      case MSG_FAIL:
        mv->connectionStatus = CS_MENU;
        break;
    }
    break;
  case CS_GETTINGPLAYER:
    if(Server_GetActivePlayer(glob->connection) && glob->connection->hasName)
    {     
      switch(glob->connection->gameStage)
      {
        case GS_NOSTATE: break;
        case GS_INGAME:
          Views_Set(glob->views, VIEW_ROOM);
          break;
        case GS_WAITFORPLAYERS:
          Views_Set(glob->views, VIEW_DOWNLOAD);
          break;
        case GS_INTERGAME:
          Views_Set(glob->views, VIEW_INTERGAME);
      }
    } else Server_Update(&glob->connection);    
    break;
  }
  
  //Update menu
  if(mv->nextMenu)
  {
    UI_Object_DestroyAll(mv->uiState);
    mv->menuSize = createButtons(
      glob,
      mv->uiState,
      mv->nextMenu,
      mv->nextMenuSize / sizeof(UI_DEF)
    );

    mv->nextMenu = NULL;
  }
  
  if(mv->connectionStatus != CS_MENU && glob->connection && glfwGetKey(glob->libs->window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
  {
    if(mv->roomDownload)
      Client_AbortDownload(&mv->roomDownload);
    else Client_Connection_Destroy(&glob->connection);
    
    mv->roomDownload = NULL;
    mv->connectionStatus = CS_MENU;
  }

  return VIEW_CONTINUE;
}

void drawTitle(GAMESTATE* glob, IO_RECT* ms)
{
#define COLORSEGMENTS 12.0f //< Number of segments to split hue into
#define VISIBLESEGMENTS 9   //< Number of segments to show
  LIBSTATE* l = glob->libs;
  NVGcontext* n = l->draw;
  NVGpaint p;
  //Draw Rainbow
  int beamh = ((MENUVIEW_PRISMHEIGHT) / (VISIBLESEGMENTS));
  int i;

  for(i = 0; i < VISIBLESEGMENTS; i ++)
  {
    //Current y of beam
    int beamy = ms->y - ((VISIBLESEGMENTS - i) * beamh);
    nvgFillColor(n, nvgHSL(i / COLORSEGMENTS, 0.4f, 0.6f));
    nvgBeginPath(n);
    nvgRect(n, ms->x + MENUVIEW_PRISMWIDTH, beamy, ms->w - MENUVIEW_PRISMWIDTH, beamh * 2);
    nvgFill(n);
  }

  //Draw Triangle
  p = nvgLinearGradient(n,
      ms->x, ms->y - (MENUVIEW_PRISMHEIGHT / 2),
      ms->x + (2*MENUVIEW_PRISMWIDTH), ms->y,
      nvgRGB(160,160,160),
      nvgRGB(50,50,50));

  nvgBeginPath(n);
  nvgMoveTo(n, ms->x, ms->y);

  nvgLineTo(n,
      ms->x + (MENUVIEW_PRISMWIDTH),
      ms->y - MENUVIEW_PRISMHEIGHT);

  nvgLineTo(n,
      ms->x + (2*MENUVIEW_PRISMWIDTH),
      ms->y);

  nvgClosePath(n);

  nvgFillPaint(n, p);
  nvgFill(n);

  nvgBeginPath(n);

  nvgFillColor(n, nvgRGB(0.0f, 0.0f, 0.0f));
  nvgTextAlign(n, NVG_ALIGN_CENTER|NVG_ALIGN_BASELINE);
  nvgFontSize(n,  MENUVIEW_TITLEHEIGHT);
  nvgFontFace(n,  glob->font);
  nvgText(n,
      ((ms->w - (2*MENUVIEW_PRISMWIDTH)) / 2) + (ms->x) + (2* MENUVIEW_PRISMWIDTH),
      ms->y, GAME_SHORTNAME, NULL);
}

char* MenuView_draw (void* globalData, void* viewData)
{
  MENUVIEW* mv = (MENUVIEW*)viewData;
  GAMESTATE* glob = (GAMESTATE*)globalData;
  LIBSTATE* ls = glob->libs;

  nvgFillColor(ls->draw, nvgHSL(mv->backHue, 0.5, 0.3));
  Draw_Rect(ls, (IO_RECT){0, 0, CANVAS_WIDTH, CANVAS_HEIGHT});

  mv->backHue+=0.001f;

  if(mv->connectionStatus == CS_MENU)
  {
    //Draw the title
    drawTitle(glob, &mv->menuSize);

    //Draw a box around the menu
    Draw_Color(ls, COLOR_WHITE);
    Draw_Rect(ls, mv->menuSize);

    Draw_Color(ls, (IO_COLOR){0xFF,0xFF,0xFF,0x66});
    Draw_Text(ls, glob->font,
        (IO_POINT){CANVAS_WIDTH/2, CANVAS_HEIGHT - 25},
        25.0f, GAME_INFO);

    UI_Draw(mv->uiState);
  }
  else
  {
    char messageBuffer[50];
    char* messageString = messageBuffer;
    switch(mv->connectionStatus)
    {
    case CS_MENU:
      break;
    case CS_CONNECTING:
      messageString = Strings_Get(glob->strings, STR_MENU_CONNECTING);
      break;
    case CS_DOWNLOADING:
      snprintf(messageBuffer, 50, "Downloading Room:%d/%d",
          mv->roomDownload->downloadAt, mv->roomDownload->downloadTotal);
      break;
    case CS_GETTINGPLAYER:
      messageString = Strings_Get(glob->strings, STR_MENU_WAITINGFORPLAYER);
      break;
    }

    Draw_Color(glob->libs, COLOR_WHITE);
    Draw_Text(glob->libs, glob->font,
      (IO_POINT){CANVAS_WIDTH / 2, CANVAS_HEIGHT / 2},
      56.0f,
      messageString);
    
    Draw_Text(glob->libs, glob->font,
        (IO_POINT){CANVAS_WIDTH / 2, 2 * CANVAS_HEIGHT / 3}
      , 24.0f, Strings_Get(glob->strings, STR_MENU_ESCAPEABORT));
  }

  return VIEW_CONTINUE;
}

char* MenuView_clean (void* globalData, void* viewData)
{
  MENUVIEW* mv = (MENUVIEW*)viewData;
  UI_Destroy(&mv->uiState);

  if(mv->connectionStatus == CS_DOWNLOADING)
  {
    Client_AbortDownload(&mv->roomDownload);
  }
  
  return VIEW_CONTINUE;
}
