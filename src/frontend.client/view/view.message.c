/*
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "common.h"
#include "view/view.h"
#include "platypro.base/strings.h"

#include <string.h>

#define OKAYWIDTH 240
#define OKAYHEIGHT 65

#define SIZE_XBUTTON   20

static IO_CIRCLE xButton = (IO_CIRCLE)
  {
    CANVAS_WIDTH / 4 * 3 - (2*SIZE_XBUTTON),
    CANVAS_HEIGHT / 4     + (2*SIZE_XBUTTON), 
    SIZE_XBUTTON
  };

char* MessageView_init(void* global, void* viewData)
{
  return VIEW_CONTINUE;
}

char* MessageView_update(void* globalData, void* viewData, EVENTSTATE* e)
{
  if(e)
  {
	GAMESTATE* glob = globalData;
    MESSAGEVIEW* mv = (MESSAGEVIEW*)viewData;
    mv->xButtonHighlight = testPointInRound(
      (IO_ROUND){
        xButton.x, xButton.y,
        SIZE_XBUTTON, SIZE_XBUTTON
      }
      , e->x, e->y);
    
    if(!e->pressed && e->lastpressed)
      Views_CloseFloat(glob->views);
  }

  return VIEW_CONTINUE;
}

char* MessageView_draw(void* globalData, void* viewData)
{
  MESSAGEVIEW* mv = (MESSAGEVIEW*)viewData;
  GAMESTATE* glob = (GAMESTATE*)globalData;
  static IO_RECT r = (IO_RECT){CANVAS_WIDTH / 4, CANVAS_HEIGHT / 4, 2 * CANVAS_WIDTH / 4, 2 * CANVAS_HEIGHT / 4};

  Draw_Color(glob->libs, COLOR_WHITE);
  Draw_Rect(glob->libs, r);

  switch(mv->xButtonHighlight)
  {
    case 0:
      Draw_Color(glob->libs, COLOR_FG);  
      break;
    case 1:
      Draw_Color(glob->libs, COLOR_BG);
      break;
  }
  
  //Draw 'x' Button
  Draw_Circle(glob->libs, 
        xButton);
  Draw_Color(glob->libs, COLOR_WHITE);
  Draw_Text(glob->libs, glob->font, (IO_POINT){xButton.x, xButton.y},SIZE_XBUTTON,"X");
  
  Draw_Color(glob->libs, COLOR_BLACK);
  Draw_Text(glob->libs, glob->font,
      (IO_POINT){CANVAS_WIDTH / 2, CANVAS_HEIGHT / 2}
    , 24.0f, "");

  nvgBeginPath(glob->libs->draw);
  nvgFontSize(glob->libs->draw, 24.0f);
  nvgTextBox(glob->libs->draw, CANVAS_WIDTH / 4, CANVAS_HEIGHT / 2, CANVAS_WIDTH / 2, mv->message, NULL);

  Draw_Text(glob->libs, glob->font,
      (IO_POINT){CANVAS_WIDTH / 2, 2 * CANVAS_HEIGHT / 3}
    , 20.0f, Strings_Get(glob->strings, STR_MENU_CLICKCONTINUE));
  return VIEW_CONTINUE;
}

char* MessageView_clean(void* globalData, void* viewData)
{
  return VIEW_CONTINUE;
}
