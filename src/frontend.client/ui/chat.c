/*
 * Aeden McClain (c) 2017-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "common.h"
#include "ui/chat.h"

#include "platypro.game/ui.h"
#include "platypro.game/tools.h"
#include "frontend.client.h"
#include "client.h"

#define COLOR_CHATBOX    (IO_COLOR){0xB6,0xB6,0xB6,0x99}

int ClickEvent_chat(UI_INDEX* index, UI_ELEMENT* object, void* state)
{
  GAMESTATE* s = (GAMESTATE*)state;
  UI_ELEMENT* chatbox = UI_Object_GetByName(index, UINAME_CHATBOX);
  if(*chatbox->text != '\000')
  {
    Client_Chat(s->connection, chatbox->text);
    UI_Focus(index, chatbox);
    *chatbox->text = '\000';
  }
  
  return true;
}

void ChatUI_Create(GAMESTATE* state, UI_INDEX* ui, IO_RECT bounds)
{
  static UI_DEF chatElements[] =
  {
      //Chat UI
      {
        UINAME_CHATBOX,
        UITYPE_TEXTBOX,
        "",
        {0,0,0,0},
        NULL
      },
      {
        UINAME_CHATBTN,
        UITYPE_BTN,
        STR_MENU_CHAT,
        {0,0,0,0},
        ClickEvent_chat
      }
  };
  
  chatElements[0].dimens = (IO_RECT){
    bounds.x, 
    bounds.y + bounds.h,
    2 * (bounds.w / 3),
    CHATBAR_SIZE
  };
  
  chatElements[1].dimens = (IO_RECT){
    bounds.x + (2*(bounds.w / 3)),
    bounds.y + bounds.h,
    bounds.w / 3,
    CHATBAR_SIZE
  };
    
  UI_Object_Auto(ui, chatElements, sizeof(chatElements), state->strings);
}

void ChatUI_Draw(GAMESTATE* state, IO_RECT bounds)
{
  LIBSTATE* libs = state->libs;
  SERVERCONNECTION* connection = state->connection;
  
  char* chatat = connection->chatHistory;
  int count = 1;
  
  //Configure Chat window
  nvgScissor(libs->draw, bounds.x, bounds.y, bounds.w, bounds.h); 

  nvgTextAlign(libs->draw, NVG_ALIGN_TOP|NVG_ALIGN_LEFT);
  nvgFontSize(libs->draw, FONTSIZE_CHAT);
  
  char type = 0;
  float chatbounds[4];
  int y = bounds.y + bounds.h;
  
  //Draw chat items
  do
  {
    switch(type)
    {
      case 0:
        if(*chatat)
        {
          int x = bounds.x + BUTTON_PADDING;
          
          nvgTextBoxBounds(libs->draw, x, y, bounds.w, chatat, NULL, chatbounds);
          
          chatbounds[3] -= chatbounds[1];
          chatbounds[2] -= chatbounds[0];
          
          y -= chatbounds[3];
          
          count++;
          
          Draw_Color(libs, COLOR_CHATBOX);
          
          nvgBeginPath(libs->draw);
          nvgRect(libs->draw, 
                  chatbounds[0], 
                  y, 
                  chatbounds[2], 
                  chatbounds[3]);
          nvgFill(libs->draw);
          
          Draw_Color(libs, COLOR_BLACK);
          nvgBeginPath(libs->draw);          
          nvgTextBox(libs->draw, x, y, bounds.w, chatat, NULL);
          type = 1;
        }
        break;
      case 1:
        if(!*chatat)
          type = 0;
    }
    chatat++;
  }
  while(chatat != (connection->chatHistory + CHAT_CACHE_SIZE));
  
  //Cleanup
  nvgResetScissor(libs->draw);
}
