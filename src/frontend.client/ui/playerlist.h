/*
 * Aeden McClain (c) 2017-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "platypro.game/tools.h"
#include "frontend.client.h"
#include "client.h"

#define LIST_PADDING 15
#define PLAYERLIST_DIVISIONS 10
#define PLAYERLIST_MAXSIZE 15

#define PLUI_TEAM_SHOW        (1 << 0)
#define PLUI_TEAM_ALLOWCHANGE (1 << 1)
#define PLUI_PLAYER_SCORE     (1 << 3)
#define PLUI_PLAYER_READY     (1 << 4)
#define PLUI_PLAYER_SHOW      (1 << 5)
#define PLUI_TEAM_SCORE       (1 << 6)

extern void PlayerListUI_Draw(GAMESTATE* state, EVENTSTATE* e, IO_RECT draw, int flags);
