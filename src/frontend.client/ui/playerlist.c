/*
 * Aeden McClain (c) 2017-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "common.h"
#include "ui/playerlist.h"

#include "frontend.client.h"
#include "platypro.game/tools.h"
#include "platypro.base/strings.h"

PRIVATE void drawPlayerName(LIBSTATE* ls, char* name, IO_RECT draw, int flags)
{
  int rowheight = draw.h / PLAYERLIST_DIVISIONS;
  IO_POINT pname =
  {
    draw.x,
    draw.y + (rowheight / 2)
  };
  
  if(flags & PLUI_PLAYER_READY)
    pname.x += rowheight;
  
  if(pname.x == draw.x) pname.x+=LIST_PADDING;

  
  Draw_Color(ls, COLOR_BLACK);
  
  nvgBeginPath(ls->draw);
  nvgTextAlign(ls->draw, NVG_ALIGN_LEFT|NVG_ALIGN_MIDDLE);
  nvgText(ls->draw, pname.x, pname.y, name, NULL);
}

PUBLIC void PlayerListUI_Draw(GAMESTATE* state, EVENTSTATE* e, IO_RECT draw, int flags)
{
  SERVERCONNECTION* connection = state->connection;
  NVGcontext* nvg = state->libs->draw;
  
  PLAYER* localPlayer = &Server_GetActivePlayer(connection)->player;
  char buffer[15]; // Buffer for team label/player numbering
  int rowheight = draw.h / PLAYERLIST_DIVISIONS;
  int count = 0; //Prevent lock-ups
  
  PLAYER* p;
  TEAM* t;
  foreach(t, connection->teams)
  {
    count ++;
    if(t->id > 0)
    {      
      if(flags & PLUI_TEAM_SHOW) // Team name is shown
      {
        nvgFontSize(nvg, rowheight - VERTICAL_SPACING);
        if(e && flags & PLUI_TEAM_ALLOWCHANGE)
        {
        if(localPlayer->team != t) // Team changes allowed
        {
          IO_RECT item = {draw.x, draw.y, draw.w, rowheight};
          //Sense clicking
          if(testPointInRect(item, e->x, e->y))
          {
            Draw_Color(state->libs, COLOR_WHITE);
            Draw_Rect(state->libs, item);

            if(!e->pressed && e->lastpressed)
            {
              //Change team
              Team_Player_Remove(localPlayer);
              Team_Player_Add(t, localPlayer);
              connection->update_team = true;
            }
          }
          
          IO_COLOR joincolor = COLOR_GREEN;
          
          int arrowbackx = draw.x + 3*(rowheight / 5);
          int frontx     = draw.x + rowheight - 4;
          int arrowpointy = draw.y + (rowheight / 2);
          
          //Draw join button
          Draw_Color(state->libs, joincolor);
          nvgBeginPath(nvg);
          nvgMoveTo(nvg, draw.x + 2, arrowpointy);
          nvgLineTo(nvg, frontx - 5, arrowpointy);
          nvgLineTo(nvg, arrowbackx, arrowpointy - 10);
          nvgLineTo(nvg, arrowbackx, arrowpointy + 10);
          nvgLineTo(nvg, frontx - 5, arrowpointy);
          nvgFill(nvg);
          
          nvgStrokeColor(nvg, nvgRGB(joincolor.r,joincolor.g,joincolor.b));
          nvgBeginPath(nvg);
          nvgMoveTo(nvg, arrowbackx, draw.y + 4);
          nvgLineTo(nvg, frontx, draw.y + 4);
          nvgLineTo(nvg, frontx, draw.y + rowheight - 4);
          nvgLineTo(nvg, arrowbackx, draw.y + rowheight - 4);
          nvgStroke(nvg);
        }
        else // Player already part of team
        {
          int arrowpointx = draw.x + (rowheight / 2);
          int arrowbacky = draw.y + 3*(rowheight / 5);
          int fronty     = draw.y + rowheight - 4;
          
          Draw_Color(state->libs, COLOR_BLACK);
          nvgBeginPath(nvg);
          nvgMoveTo(nvg, arrowpointx, draw.y + 2);
          nvgLineTo(nvg, arrowpointx, fronty - 5);
          nvgLineTo(nvg, arrowpointx - 10, arrowbacky);
          nvgLineTo(nvg, arrowpointx + 10, arrowbacky);
          nvgLineTo(nvg, arrowpointx, fronty - 5);
          nvgFill(nvg);
        }
        }
        else if(localPlayer->team == t)
        {
          Draw_Color(state->libs, COLOR_BLACK);
          nvgBeginPath(nvg);
          nvgMoveTo(nvg, draw.x, draw.y);
          nvgLineTo(nvg, draw.x, draw.y + rowheight);
          nvgLineTo(nvg, draw.x + rowheight - 5, draw.y + (rowheight / 2));
          nvgFill(nvg);
        }
        
        Draw_Color(state->libs, COLOR_BLACK);
               
        if(flags & PLUI_TEAM_SCORE)
        {
          snprintf(buffer, 15, "%d", t->teamscore);
          nvgBeginPath(nvg);
          nvgTextAlign(nvg, NVG_ALIGN_CENTER|NVG_ALIGN_TOP);
          nvgText(nvg, draw.x + draw.w - rowheight, draw.y, buffer, NULL);
        }
        
        //Draw Team label
        snprintf(buffer, 15, "%s %d", Strings_Get(state->strings, STR_TEAM), t->id);
        
        nvgBeginPath(nvg);
        nvgTextAlign(nvg, NVG_ALIGN_LEFT|NVG_ALIGN_TOP);
        nvgText(nvg, draw.x + rowheight, draw.y, buffer, NULL);
        
        draw.y+=rowheight;
      }
    }
      
    nvgFontSize(nvg, BAR_HEIGHT / 2);
    
    //Draw Player entries
    if(flags & PLUI_PLAYER_SHOW)
    for(p = t->firstChild; p!=NULL && count < PLAYERLIST_DIVISIONS; p=p->nextTeam)
    {  
      count++;
      IO_RECT pready =
      {
        draw.x + rowheight / 4,
        draw.y + rowheight / 4,
        rowheight / 2,
        rowheight / 2
      };
      SET_COLORTEAM(state->libs, t->id, List_Count(&connection->teams), 0.7f);
      Draw_Rect(state->libs, (IO_RECT){draw.x, draw.y + 5, draw.w, rowheight- 10} );
      
      if(flags & PLUI_PLAYER_READY)
      {          
        Draw_Color(state->libs, COLOR_BLACK);
        Draw_Round(state->libs, pready);
      }
      
      if(flags & PLUI_PLAYER_SCORE)
      {
        Draw_Color(state->libs, COLOR_BLACK);
        snprintf(buffer, 15, "%d", p->score);
        nvgBeginPath(nvg);
        nvgTextAlign(nvg, NVG_ALIGN_CENTER|NVG_ALIGN_MIDDLE);
        nvgText(nvg, draw.x + draw.w - rowheight, draw.y + rowheight / 2, buffer, NULL);
      }
      
      if(p != localPlayer)
      {
        drawPlayerName(state->libs, p->name, draw, flags);
        
        if(flags & PLUI_PLAYER_READY)
        {
          if(p->ready)
          {
            Draw_Color(state->libs, COLOR_GREEN);
          } else Draw_Color(state->libs, COLOR_RED);
          Draw_Round(state->libs, (IO_RECT){pready.x + 2, pready.y + 2, pready.w - 4, pready.h - 4});
        }
      } 
      else 
      {
        //Draw entry for "you"
        drawPlayerName(state->libs, Strings_Get(state->strings, STR_YOU), draw, flags);
      }
      
      draw.y+=rowheight;
    }
  }
}
