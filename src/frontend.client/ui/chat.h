/*
 * Aeden McClain (c) 2017-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "platypro.game/ui.h"
#include "platypro.game/tools.h"
#include "client.h"
#include "frontend.client.h"

#define FONTSIZE_CHAT 24.0f
#define CHATBAR_SIZE  50

#define UINAME_CHATBOX    "ChatBox"
#define UINAME_CHATBTN    "DoChat"

/** Initializes chat UI elements
 */
extern void ChatUI_Create(GAMESTATE* connection, UI_INDEX* ui, IO_RECT bounds);

/** Draws non-UI chat elements
 */
extern void ChatUI_Draw(GAMESTATE* state, IO_RECT bounds);
