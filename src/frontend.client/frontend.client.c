/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
#include "common.h"
#include "frontend.client.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "platypro.base/strings.h"
#include "platypro.game/tools.h"
#include "platypro.game/view.h"
#include "client.h"
#include "view/view.h"

bool cleanup(GAMESTATE** state)
{
  GAMESTATE* gs = *state;

  if(gs->views)
    Views_Cleanup(&gs->views);
  if(gs->log)
    Log_Cleanup(&gs->log);
  if(gs->libs)
    Window_Close(&gs->libs);
  if(gs->strings)
    Strings_Close(&gs->strings);
  if(gs->settingStrings)
    Strings_Close(&gs->settingStrings);

  free(gs);
  *state = NULL;

  return true;
}

VIEWMANAGER* createViews(GAMESTATE* state, LIST strings)
{
  VIEWMANAGER* result = Views_Init(state);
  if(!Views_Create(result,
      VIEW_MENU, sizeof(MENUVIEW),
      MenuView_init,
      MenuView_update,
      MenuView_draw,
      MenuView_clean))
    Views_Cleanup(&result);

  if(!Views_Create(result,
      VIEW_DOWNLOAD, sizeof(DOWNLOADVIEW),
      DownloadView_init,
      DownloadView_update,
      DownloadView_draw,
      DownloadView_clean))
    Views_Cleanup(&result);

  if(!Views_Create(result,
      VIEW_ROOM, sizeof(ROOMVIEW),
      RoomView_init,
      RoomView_update,
      RoomView_draw,
      RoomView_clean))
    Views_Cleanup(&result);

  if(!Views_Create(result,
      VIEW_MESSAGE, sizeof(MESSAGEVIEW),
      MessageView_init,
      MessageView_update,
      MessageView_draw,
      MessageView_clean))
    Views_Cleanup(&result);
  
  if(!Views_Create(result,
      VIEW_INTERGAME, sizeof(INTERGAMEVIEW),
      IntergameView_init,
      IntergameView_update,
      IntergameView_draw,
      IntergameView_clean))
    Views_Cleanup(&result);
  
  if(!Views_Create(result,
      VIEW_GAMEMENU, sizeof(GAMEMENUVIEW),
      GameMenuView_init,
      GameMenuView_update,
      GameMenuView_draw,
      GameMenuView_clean))
    Views_Cleanup(&result);

  Views_Set(result, VIEW_MENU);
  return result;
}

void onUserError(void* user, char* severity, char* message)
{
  GAMESTATE* state = (GAMESTATE*)user;
  MESSAGEVIEW* mv = (MESSAGEVIEW*)Views_SetFloat(state->views, VIEW_MESSAGE);
  strncpy(mv->message, message, 255);
  return;
}

GAMESTATE* init(char* stringfile)
{
    //Initialize the global game state.
    //This holds any rendering contexts and the
    //current loaded GAMEVIEW
    GAMESTATE* gs = calloc(1, sizeof(GAMESTATE));;
    LIST strings = Strings_Load(DEF_STRINGFILE, false);
    if(!strings) goto INIT_ERROR;

    if (!gs) 
    {
      Log_ThrowFormat(gs->log, ERRSEV_FATAL, ERRID_MEM, "GameState");
      goto INIT_ERROR;
    }

    char logname[255];
    snprintf(logname, 255, "%s.log", stringfile);
    gs->log  = Log_Init(logname, strings);
    if(!gs->log) goto INIT_ERROR;
    
    Log_SetWarnCallback(gs->log, onUserError, gs);

    gs->strings = strings;
    gs->settingStrings = Strings_Load(DEF_SETTINGSFILE, true);
    if(!Strings_GetValue(gs->settingStrings,SETTING_USERNAME))
    {
      Strings_Set(
        &gs->settingStrings,
        SETTING_USERNAME,
        Strings_Get(strings, STR_USERNAME));
    }

    //This sets the default room, 
    //and loads it
    gs->views = createViews(gs, gs->strings);
    if (!gs->views) goto INIT_ERROR;
    
    Log_ThrowFormat(gs->log, ERRSEV_INFO, STR_WINDOW_LOAD, NULL);

    gs->libs = Window_New(CANVAS_WIDTH, CANVAS_HEIGHT);
    
    Window_FullScreen(gs->libs, Strings_Get(gs->settingStrings,SETTING_FULLSCREEN));
    if(!gs->libs)
    {
      Log_ThrowFormat(gs->log, ERRSEV_FATAL, ERRID_INIT, NULL);
      goto INIT_ERROR;
    }
    
    Window_Title(gs->libs, VERSION_NAME);
    gs->font     = Draw_LoadFont(gs->libs, DEF_FONT_NORMAL);
    gs->fontMono = Draw_LoadFont(gs->libs, DEF_FONT_MONO);

    return gs;
    
INIT_ERROR:
    cleanup(&gs);
    return NULL;
}

int main(int argc, char** argv)
{
  GAMESTATE* state = init(argv[0]);
  if(state)
  {
    Window_Loop(state->libs, Views_Update, Views_Draw, state->views);
    cleanup(&state);
  }
  printf("Program Terminated\n");
  return 0;
}
